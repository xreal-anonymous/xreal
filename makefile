SHELL := /bin/bash # Use bash syntax

COMPILERSUFFIX=-11

CXX=clang++$(COMPILERSUFFIX)
CXXFLAGS=-std=c++17 -O2
CXXFLAGSFASTER=-std=c++17
# CXXFLAGS=-std=c++17

LD=clang++$(COMPILERSUFFIX)
OPT=opt$(COMPILERSUFFIX)

PASSFLAGS=-I/usr/include/eigen3 -fPIC

LDFLAGS=-lm -lgmp -lmpfr

LLVMCFG=llvm-config$(COMPILERSUFFIX)
LLVMCXXFLAGS=$(shell $(LLVMCFG) --cxxflags)
LLVMCXXFLAGS+=-fPIC -fno-rtti
LLVMLDFLAGS=$(shell $(LLVMCFG) --ldflags --libs --system-libs)
# If cannot find -lclang-cpp, `apt install libclang-cpp11-dev`
CLANGLIBS=-lclang -lclang-cpp

##### Utilities #####
dir_guard=@mkdir -p $(@D)

GREEN=$(shell tput setaf 2)
NOCOLOR=$(shell tput sgr0)
define MSG
	@tput setaf 2
	@echo [Done] $1
	@tput sgr0
endef
##### Utilities #####

.PHONY: all clean source-layer source-layer-temp runtime-layer runtime-clean

all: source-layer

###### Build Source Layer Files ########

source-layer: bin/SourceLayerDriver

bin/SourceLayerDriver: build/SourceLayerDriver.o lib/SourcePasses.so build/TypeValueConverter.o
	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS) $(LLVMLDFLAGS) $(CLANGLIBS)

# Wildcard for any precision control file with .pc.cpp
PC_SRCS := $(wildcard inputs/*.pc.cpp)
PC_OBJS := $(patsubst inputs/%.pc.cpp, build/inputs/%.pc.o, $(PC_SRCS))

lib/SourcePasses.so: $(PC_OBJS) build/xreal.o
	$(dir_guard)
	$(CXX) -o $@ -shared $^ $(LDFLAGS)

build/inputs/%.pc.o: inputs/%.pc.cpp \
		src/SourceLayer/SourceLayerPass.h \
		src/SourceLayer/Transformer.h \
		src/SourceLayer/TypeValueConverter.h \
		src/xreal/xreal.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) $(PASSFLAGS) -c $<

build/SourceLayerDriver.o: src/SourceLayer/SourceLayerDriver.cpp src/SourceLayer/SourceLayerPass.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $< 

build/xreal.o: src/xreal/xreal.cpp src/xreal/xreal.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) $(PASSFLAGS) -c $<

build/TypeValueConverter.o: src/SourceLayer/TypeValueConverter.cpp \
							src/SourceLayer/TypeValueConverter.h \
							src/SourceLayer/Transformer.h \
							src/xreal/xreal.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) -c $<

source-layer-temp: constconverter

constconverter: bin/ConstConverter
constconverter-run: constconverter
	bin/ConstConverter inputs/test-constant.cpp --
bin/ConstConverter: build/ConstConverter.o build/xreal.o
	$(dir_guard)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LLVMLDFLAGS) $(CLANGLIBS) 
build/ConstConverter.o: src/SourceLayer/ConstConverter.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) -c $<
# typeconverter:  bin/TypeConverter
# valueconverter: bin/ValueConverter
# typeconverter-run: typeconverter
# 	bin/TypeConverter inputs/test-type-convert.xreal --
# valueconverter-run: valueconverter
# 	bin/ValueConverter inputs/test-value-convert.xreal --
# bin/ClangTooling: src/ClangTooling.cpp
# 	@mkdir -p bin
# 	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) $< $(LLVMLDFLAGS) $(CLANGLIBS)

# bin/TypeConverter: src/TypeConverter.cpp
# 	@mkdir -p bin
# 	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) $< $(LLVMLDFLAGS) $(CLANGLIBS)

# bin/ValueConverter: src/ValueConverter.cpp
# 	@mkdir -p bin
# 	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) $< $(LLVMLDFLAGS) $(CLANGLIBS)

########################################

######## Build Runtime Layer Files ########
runtime-layer: bin/RuntimeLayerDriver lib/libIREditor.so
	$(call MSG, $<)

# xreal.o must be ahead of other objects files.
bin/RuntimeLayerDriver: build/RuntimeLayer.o \
  build/xreal.o build/intermediate/libInter.a build/AtomuUtil.o \
  build/PolyFit.o build/AcesoX.o build/RootFinding.o
	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS)

INTER_SRCS         := $(wildcard intermediate/*.cpp)
INTER_ORACLE_SRCS  := $(wildcard intermediate/*.oracle.cpp)
INTER_NEED_IR_SRCS := $(filter-out $(INTER_ORACLE_SRCS), $(INTER_SRCS))

INTER_ORACLE_OBJS  := $(patsubst intermediate/%.cpp, build/intermediate/%.o, $(INTER_ORACLE_SRCS))
INTER_NEED_IR_OBJS := $(patsubst intermediate/%.cpp, build/intermediate/%.o, $(INTER_NEED_IR_SRCS))

build/intermediate/libInter.a: $(INTER_ORACLE_OBJS) $(INTER_NEED_IR_OBJS) build/handlers.o
	$(dir_guard)
	ar rcs $@ $^

# This will match *.oracle.cpp, since Make 3.82
# https://stackoverflow.com/questions/11455182/when-multiple-pattern-rules-match-a-target
build/intermediate/%.oracle.o: intermediate/%.oracle.cpp
	$(dir_guard)
	$(CXX) -c -O3 $< -o $@
	$(call MSG, "Building $< for Oracle..")

# this will match all *.double.cpp/*.float.cpp etc.
build/intermediate/%.o: intermediate/%.cpp lib/libIREditor.so
	$(dir_guard)
	$(CXX) -S -emit-llvm -O1 -g $< -o $(patsubst %.o, %.raw.ll, $@)
	$(OPT) -S -load lib/libIREditor.so -funcpass $(patsubst %.o, %.raw.ll, $@) -o $(patsubst %.o, %.cooked.ll, $@)
	$(CXX) -c -O2 $(patsubst %.o, %.cooked.ll, $@) -o $@
	$(call MSG, "Building $< for Atomu..")

build/RuntimeLayer.o: src/RuntimeLayer/RuntimeLayerDriver.cpp \
  src/xreal/xreal.h \
  src/RuntimeLayer/Atomu.h src/RuntimeLayer/ErrorDetector.h \
  src/RuntimeLayer/Communicator.h src/RuntimeLayer/opcode.h \
  src/RuntimeLayer/InstInfo.h src/RuntimeLayer/AtomuUtil.h \
  src/RuntimeLayer/AcesoX.h src/RuntimeLayer/Polynomial.h \
  src/RuntimeLayer/RootFinding.h src/RuntimeLayer/TargetFunctions.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

lib/libIREditor.so: build/IREditor.o
	$(dir_guard)
	$(CXX) -o $@ -shared $(LLVMLDFLAGS) $<

build/IREditor.o: src/RuntimeLayer/IREditor.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) $(LLVMCXXFLAGS) -c $<

build/handlers.o: src/RuntimeLayer/handlers.cpp src/RuntimeLayer/Communicator.h \
  src/RuntimeLayer/opcode.h src/RuntimeLayer/InstInfo.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/AtomuUtil.o: src/RuntimeLayer/AtomuUtil.cpp src/RuntimeLayer/AtomuUtil.h \
  src/RuntimeLayer/opcode.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/PolyFit.o: src/RuntimeLayer/PolyFit.cpp \
  src/xreal/xreal.h src/RuntimeLayer/Polynomial.h
	$(dir_guard)
	$(CXX) -I/usr/include/eigen3 -o $@ $(CXXFLAGS) -c $<

build/RootFinding.o: src/RuntimeLayer/RootFinding.cpp \
  src/RuntimeLayer/RootFinding.h src/xreal/xreal.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/testPolynomial.o: src/RuntimeLayer/testPolynomial.cpp \
  src/RuntimeLayer/Polynomial.h src/xreal/xreal.h \
  src/RuntimeLayer/PolyFit.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/AcesoX.o: src/RuntimeLayer/AcesoX.cpp src/RuntimeLayer/AcesoX.h \
  src/RuntimeLayer/Polynomial.h src/xreal/xreal.h \
  src/RuntimeLayer/PolyFit.h
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

bin/testPolynomial: build/testPolynomial.o \
  build/xreal.o build/PolyFit.o \
  build/RootFinding.o build/AcesoX.o
	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS)

runtime-clean:
	rm -rf build/intermediate/
	rm -rf data/

###########################################

######## Build Test Files ########
eval-test: bin/test

bin/test: build/test.o \
			build/xreal.o \
			build/gamma.final.float.o \
			build/gamma.final.double.o \
			build/gamma.final.longdouble.o \
			build/dilog.final.float.o \
			build/dilog.final.double.o \
			build/dilog.final.longdouble.o \
			build/lambert.final.float.o \
			build/lambert.final.double.o \
			build/lambert.final.longdouble.o \
			build/lambert.final.oracle.o

	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS) -lgsl -lgslcblas
build/test.o: final-optimized/test.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.final.float.o: final-optimized/gamma.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.final.double.o: final-optimized/gamma.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.final.longdouble.o: final-optimized/gamma.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.final.float.o: final-optimized/dilog.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.final.double.o: final-optimized/dilog.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.final.longdouble.o: final-optimized/dilog.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.final.float.o: final-optimized/lambert.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.final.double.o: final-optimized/lambert.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.final.longdouble.o: final-optimized/lambert.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.final.oracle.o: final-optimized/lambert.oracle.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

###########################################

######## Build Test-intermediate Files ########
bin/test-intermediate: build/test-intermediate.o \
			build/xreal.o \
			build/gamma.intermediate.float.o \
			build/gamma.intermediate.double.o \
			build/gamma.intermediate.longdouble.o \
			build/gamma.intermediate.oracle.o \
			build/dilog.intermediate.float.o \
			build/dilog.intermediate.double.o \
			build/dilog.intermediate.longdouble.o \
			build/dilog.intermediate.oracle.o \
			build/lambert.intermediate.float.o \
			build/lambert.intermediate.double.o \
			build/lambert.intermediate.longdouble.o \
			build/lambert.intermediate.oracle.o \
			build/lambert.final.oracle.o

	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS) -lgsl -lgslcblas
build/test-intermediate.o: eval/test-intermediate.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.intermediate.float.o: intermediate/gamma.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.intermediate.double.o: intermediate/gamma.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.intermediate.longdouble.o: intermediate/gamma.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/gamma.intermediate.oracle.o: intermediate/gamma.oracle.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/dilog.intermediate.float.o: intermediate/dilog.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.intermediate.double.o: intermediate/dilog.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.intermediate.longdouble.o: intermediate/dilog.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/dilog.intermediate.oracle.o: intermediate/dilog.oracle.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/lambert.intermediate.float.o: intermediate/lambert.float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.intermediate.double.o: intermediate/lambert.double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.intermediate.longdouble.o: intermediate/lambert.longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/lambert.intermediate.oracle.o: intermediate/lambert.oracle.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<

build/manipulate-gsl/float.o: manipulate-gsl/gsl-implement-float.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/manipulate-gsl/double.o: manipulate-gsl/gsl-implement-double.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/manipulate-gsl/longdouble.o: manipulate-gsl/gsl-implement-longdouble.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
build/manipulate-gsl/test.o: manipulate-gsl/test-manipulate-gsl.cpp
	$(dir_guard)
	$(CXX) -o $@ $(CXXFLAGS) -c $<
bin/test-manipulate-gsl: build/manipulate-gsl/test.o \
			build/xreal.o \
			build/manipulate-gsl/float.o \
			build/manipulate-gsl/double.o \
			build/manipulate-gsl/longdouble.o \
			build/lambert.final.oracle.o
	$(dir_guard)
	$(LD) -o $@ $^ $(LDFLAGS) -lgsl -lgslcblas
manipulate-gsl: bin/test-manipulate-gsl


clean:
	rm -f *.o
	rm -f *.out
	rm -rf lib/
	rm -rf build/
	rm -rf bin/
	rm -rf intermediate/
	rm -rf data/