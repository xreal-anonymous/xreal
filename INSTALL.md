# Installation

```
LLVM_VERSION=11

apt install clang-$LLVM_VERSION libclang-$LLVM_VERSION-dev libclang-cpp$LLVM_VERSION-dev clang-tools-$LLVM_VERSION

apt install libmpfr-dev libmpfrc++-dev

apt install libeigen3-dev
```
