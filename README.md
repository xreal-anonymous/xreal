# XReal Project - Artifact for Evaluation

## Installation

Please install the necessary packages that are listed in `INSTALL.md`.


## Quick Reproduce Steps

1. Build and Register Source Layer Passes
```
$ make
```

2. Execute Source Layer Passes
```bash
# 2.1 See the available/registered passes
$ bin/SourceLayerDriver
# Output: 
# Usage: bin/SourceLayerDriver [passname]
# Available passnames:
#  - dilogpass
#  - gammapass
#  - lambertpass

# 2.2 Execute Passes one by one
$ bin/SourceLayerDriver dilogpass
$ bin/SourceLayerDriver gammapass
$ bin/SourceLayerDriver lambertpass
```

3. Build and Execute Runtime Layer Passes
```bash
$ make runtime-layer
$ bin/RuntimeLayerDriver
```

4. Show the Evaluation Results
```bash
$ make eval-test
$ bin/test
```

## Introduction - Prototype and Precision Specification
1. The prototype describes a numerical algorithm, and should only contain `xreal` as the floating-point type.

- For precision-control variables in prototype, you can use any values as placeholder.

- See `dilog.xreal` for an example of the prototype.

- Put the prototype in `inputs` folder, with name `<PROG-NAME>.xreal`

2. The precision specification describes the precision-control variables in the corresponding prototype. The precision specification is an *executable pass*, and you can use the following template to construct your specification:
```c++
#include "../src/SourceLayer/SourceLayerPass.h"
#include "../src/SourceLayer/Transformer.h"
#include "../src/xreal/xreal.h"
struct myPass : sourcelayer::Pass {
    static char ID;
    myPass() : sourcelayer::Pass(ID) { }

    bool run() override {
        // Put your specification here.

        return false;
    }
};
char myPass::ID = 0; 

// Register - Producer
sourcelayer::RegisterPass<myPass> x("<PROG-NAME>");
```

- See `dilog.pc.cpp` for an example of the precision specification.

- Put the specification in `inputs` folder, with name `<PROG-NAME>.pc.cpp`
