#include "mpreal.h"
#include <random>
#include <gsl/gsl_sf.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include "../src/xreal/xreal.h"

// MPFR bits

int normal_oracle_precision = 256;

int eval_high_fp_precision = 2048;
int eval_high_oracle_precision = 3072;

// Specify function type
using oracleFunc_t = mpfr::mpreal(*)(double);

template<class FP>
using fpFunc_t = FP(*)(FP);


// General function for relative error
template<class FP>
double err(double input, fpFunc_t<FP> fp, oracleFunc_t op) {
    // Get fp result
    if (std::is_same_v<FP, mpfr::mpreal> || std::is_same_v<FP, xreal>) {
        mpfr::mpreal::set_default_prec(eval_high_fp_precision);
    }
    FP y1 = fp(input);

    // Get oracle result
    if (std::is_same_v<FP, mpfr::mpreal> || std::is_same_v<FP, xreal>) {
        mpfr::mpreal::set_default_prec(eval_high_oracle_precision);
    }
    else {
        mpfr::mpreal::set_default_prec(normal_oracle_precision);
    }
    mpfr::mpreal y2 = op(input);
    if (y2 == 0) return 0;

    mpfr::mpreal abserr = fabs(y2 - y1);
    mpfr::mpreal relerr = fabs(abserr / y2);
    return (double)relerr;
}

std::vector<double> gen_magnitude_inputs(double center, double high_eps, double low_eps, int num) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    double high_exp = log(high_eps);
    double low_exp = log(low_eps);

    std::uniform_real_distribution<double> uni_dis(high_exp, low_exp);

    std::vector<double> inputs;

    for (int i = 0; i < num; i++) {
        double cur_exp = uni_dis(gen64);
        double cur_offset = exp(cur_exp);
        double cur_input = center + cur_offset;
        inputs.push_back(cur_input);
    }

    return inputs;
}

std::vector<double> gen_uniform_inputs(double a, double b, int num) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    std::uniform_real_distribution<double> uni_dis(a, b);

    std::vector<double> inputs;

    for (int i = 0; i < num; i++) {
        double input = uni_dis(gen64);
        inputs.push_back(input);
    }

    return inputs;
}

template<class FP, class FP2>
double test_functions_with_inputs(fpFunc_t<FP> fp, oracleFunc_t op, fpFunc_t<FP2> gsl_fp, std::vector<double> inputs) {
    double num = inputs.size();

    double max_err = 0.0;
    double max_input = 0.0;
    for (int i = 0; i < num; i++) {
        FP input_fp = inputs[i];
        double input = (double)input_fp;
        double cur_err = err(input, fp, op);
        if (cur_err > max_err) {
            max_err = cur_err;
            max_input =(double) input;
        }
    }

    printf("Max Error: %.2e, when input = %.16e\n", max_err, max_input);
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(max_input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(max_input) << "\n";
    std::cout << "    AC: " << op(max_input).toString("%.20Re") << "\n";

    return max_err;
}

template<class FP, class FP2>
void test_manually(double input, fpFunc_t<FP> fp, oracleFunc_t op, fpFunc_t<FP2> gsl_fp) {
    printf("Err: %.2e\n", err(input, fp, op));
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(input) << "\n";
    std::cout << "    AC: " << op(input).toString("%.20Re") << "\n";
    // printf("    FP: %.16e\n", fp(input));
    // printf("   Ref: %.16e\n", gsl_fp(input));
    // printf("    AC: %.16e\n", (double)op(input));
}

// Specify program under test
// our loggamma
float lngamma(float);
double lngamma(double);
long double lngamma(long double);
xreal lngamma(xreal);
// gsl's loggamma
double gsl_sf_lngamma(double);
// ground truth loggamma
mpfr::mpreal loggamma(const double x) {
    return mpfr::lngamma(x);
}

void test_gamma() {
    printf("\n====================\n");
    printf("Loggamma Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(lngamma);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(lngamma);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(lngamma);
    // xreal
    fpFunc_t<xreal> fp_xreal = static_cast<fpFunc_t<xreal>>(lngamma);

    fpFunc_t<double> gsl_fp = gsl_sf_lngamma;
    oracleFunc_t op = loggamma;

    test_manually(1.0, fp_xreal, op, gsl_fp);

    // around 0, 1, 2
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-1, 1e-60, 1000);
    std::vector<double> inputs_sp_2 = gen_magnitude_inputs(1, 1e-2, 1e-20, 1000);
    std::vector<double> inputs_sp_3 = gen_magnitude_inputs(2, 1e-2, 1e-20, 1000);
    // general: from 1 to 1e30
    std::vector<double> inputs_general_1 = gen_magnitude_inputs(0, 1, 1e30, 1000);
    // general: from 0 to 10000
    std::vector<double> inputs_general_2 = gen_uniform_inputs(0, 10000, 10000);

    printf("\n-------For xreal's loggamma(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's loggamma(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's loggamma(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's loggamma(xreal)-------\n");
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_general_2);

    printf("\n-------For GSL's loggamma-------\n");
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_1);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_2);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_3);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_general_1);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_general_2);
}

// Specify program under test
// our dilog
float dilog(float);
double dilog(double);
long double dilog(long double);
xreal dilog(xreal);
// gsl's dilog
double gsl_sf_dilog(double);
// ground truth dilog
mpfr::mpreal li2(const double x) {
    return mpfr::li2(x);
}

void test_dilog() {
    printf("\n====================\n");
    printf("Dilog Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(dilog);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(dilog);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(dilog);
    // xreal
    fpFunc_t<xreal> fp_xreal = static_cast<fpFunc_t<xreal>>(dilog);

    fpFunc_t<double> gsl_fp = gsl_sf_dilog;
    oracleFunc_t op = li2;

    // around 0
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-1, 1e-60, 1000);
     // -0.5, 0.5
    std::vector<double> inputs_sp_2 = gen_uniform_inputs(-0.5, 0.5, 1000);
    // around root
    std::vector<double> inputs_sp_3 = gen_magnitude_inputs(1.2595170369845016e+01, 1e-3, 1e-18, 1000);
    // general test
    std::vector<double> inputs_general_1 = gen_uniform_inputs(-1e4, 1e4, 10000);

    printf("\n-------For xreal's dilog(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);

    printf("\n-------For xreal's dilog(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);

    printf("\n-------For xreal's dilog(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);

    printf("\n-------For xreal's dilog(xreal)-------\n");
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_general_1);

    printf("\n-------For GSL's dilog-------\n");
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_1);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_2);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_3);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_general_1);
}

// Specify program under test
float lambert_W0(float);
double lambert_W0(double);
long double lambert_W0(long double);
xreal lambert_W0(xreal);
// gsl's lambert
double gsl_sf_lambert_W0(double);
// ground truth lambert
mpfr::mpreal lambert_W0_oracle(double x);

void test_lambert() {
    printf("\n====================\n");
    printf("Lambert Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(lambert_W0);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(lambert_W0);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(lambert_W0);
    // xreal
    fpFunc_t<xreal> fp_xreal = static_cast<fpFunc_t<xreal>>(lambert_W0);

    fpFunc_t<double> gsl_fp = gsl_sf_lambert_W0;
    oracleFunc_t op = lambert_W0_oracle;

    // around 0
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-2, 1e-60, 1000);
    // around -1/e
    std::vector<double> inputs_sp_2 = gen_magnitude_inputs(-0.36787944117144233, 1e1, 1e-20, 1000);
    // from 0 to 1e30
    std::vector<double> inputs_general_1 = gen_magnitude_inputs(0, 1e-2, 1e30, 1000);
    // general test
    std::vector<double> inputs_general_2 = gen_uniform_inputs(-0.36787944117144233, 1e6, 10000);


    printf("\n-------For xreal's lambert(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's lambert(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's lambert(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_2);

    printf("\n-------For xreal's lambert(xreal)-------\n");
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_xreal, op, gsl_fp, inputs_general_2);

    printf("\n-------For GSL's lambert-------\n");
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_1);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_sp_2);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_general_1);
    test_functions_with_inputs(gsl_fp, op, fp_d, inputs_general_2);
}

template<class FP>
double test_runtime(fpFunc_t<FP> fp, double a, double b, int num, std::string funcname) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    std::uniform_real_distribution<double> uni_dis(a, b);

    // Init inputs.
    std::vector<double> inputs(num);
    for (int i = 0; i < num; i++) {
        inputs[i] = uni_dis(gen64);
    }

    // Measure runtime
    double t;
    auto start_time = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < num; i++) {
        fp(inputs[i]);
    }
    auto end_time = std::chrono::high_resolution_clock::now();

    int64_t all_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count();
    double each_time = all_time / (double)num;
    printf("%.1f ns on %s\n", each_time, funcname.c_str());
    return each_time;
}

int main() {

    // Test Accuracy
    test_gamma();
    test_dilog();
    test_lambert();

    // Test time
    // int num = 1000000;
    // test_runtime(lngamma, 0, 1e5, num, "xreal lngamma");
    // test_runtime(gsl_sf_lngamma, 0, 1e5, num, "GSL lngamma");

    // test_runtime(dilog, -1e6, 1e6, num, "xreal dilog");
    // test_runtime(gsl_sf_dilog, -1e6, 1e6, num, "GSL dilog");

    // test_runtime(lambert_W0, -0.36787944117144233, 100, num, "xreal lambert");
    // test_runtime(gsl_sf_lambert_W0, -0.36787944117144233, 100, num, "GSL lambert");
    return 0;
}