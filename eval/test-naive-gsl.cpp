#include <gsl/gsl_sf.h>
#include <cstdio>
#include <iostream>
#include <iomanip>

using namespace std;

typedef long double real;

float naive_trans_gsl_lambert_W0(float x);
double naive_trans_gsl_lambert_W0(double x);
long double naive_trans_gsl_lambert_W0(long double x);

float naive_trans_gsl_dilog(float x);
double naive_trans_gsl_dilog(double x);
long double naive_trans_gsl_dilog(long double x);

float naive_trans_gsl_lngamma(float x);
double naive_trans_gsl_lngamma(double x);
long double naive_trans_gsl_lngamma(long double x);


void test_lambert(double x) {
    real a = gsl_sf_lambert_W0(x);
    real b = naive_trans_gsl_lambert_W0((real)x);

    std::cout << "gsl_original: " << std::setprecision(20) << std::scientific << a << "\n";
    std::cout << "gsl_implant : " << std::setprecision(20) << std::scientific << b << "\n";
}

void test_dilog(double x) {
    real a = gsl_sf_dilog(x);
    real b = naive_trans_gsl_dilog((real)x);

    std::cout << "gsl_original: " << std::setprecision(20) << std::scientific << a << "\n";
    std::cout << "gsl_implant : " << std::setprecision(20) << std::scientific << b << "\n";
}

void test_lngamma(double x) {
    real a = gsl_sf_lngamma(x);
    real b = naive_trans_gsl_lngamma((real)x);
    
    std::cout << "gsl_original: " << std::setprecision(20) << std::scientific << a << "\n";
    std::cout << "gsl_implant : " << std::setprecision(20) << std::scientific << b << "\n";
}

int main() {
    while (1) {
        double in;
        scanf("%lf", &in);
        // test_lambert(in);
        // test_dilog(in);
        test_lngamma(in);
    }
    return 0;
}