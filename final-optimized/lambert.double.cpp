#include "../src/xreal/xreal.h"

namespace {
using namespace std;

double power_series_0(double x) {
    double z = x - 0.0;
    const static int series_num = 7;
    const static double series_coef[] = {
        0.00000000000000000000e+00,
        1.00000000000000000000e+00,
        -1.00000000000000000000e+00,
        1.49999999999999795748e+00,
        -2.66666666666666081524e+00,
        5.20833374183683224623e+00,
        -1.08000010031021138924e+01,
    };
    double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

const double CONST_E = 2.7182818284590452e+00;
static int terms = 12;
static double one_over_e = 1.0 / CONST_E;

static const double coefs[] = {
    -1.0000000000000000e+00,
    2.3316439815971242e+00,
    -1.8121878856393635e+00,
    1.9366311144923598e+00,
    -2.3535512018816145e+00,
    3.0668589010506319e+00,
    -4.1753356002581771e+00,
    5.8580237298747741e+00,
    -8.4010322175239774e+00,
    1.2250753501314460e+01,
    -1.8100697012472443e+01,
    2.7029044799010562e+01,
};

static double hornor_eval(double x) {
    double res = 0;
    for (int i = terms-1; i >= 0; i--) {
        res = res * x + coefs[i];
    }
    return res;
}

static int max_iters = 10;
static double tol_factor = 2.2204460492503131e-16;

static double halley_iteration(double x, double w) {
    for (int i = 0; i < max_iters; i++) {
        double tol;
        double e = exp(w);
        double p = w + 1.0;
        double t = w*e - x;

        // printf("w, t: %20.16g  %20.16g\n", (double)w, (double)t);

        if (w > 0) {
            t = (t/p)/e; /* Newton iteration */
        }
        else {
            t /= e*p - 0.5*(p + 1.0)*t/p; /* Halley iteration */
        }

        w -= t;

        tol = 10 * tol_factor * std::max(fabs(w), 1.0/(fabs(p)*e));

        if (fabs(t) < tol) {
            return w;
        }
    }
    // Should not reach here
    return w;
}
}

double lambert_W0(double x) {
    if (-1e-4 <= x && x <= 1e-4) {
        return power_series_0(x);
    }

    double one_over_e = 1.0/CONST_E;
    double comp_term = 1.2428753672788363e-17;
    double q = x + one_over_e - comp_term;

    if (x == 0) {
        return 0.0;
    }
    else if (q <= 0.0) {
        return -1.0;
    }
    else if (q < 1.0e-3) {
        double r = sqrt(q);
        double y = hornor_eval(r);
        return y;
    }
    else {
        double w;
        if (x < 1.0) {
            double p = sqrt(2.0 * CONST_E * q);
            w = -1.0 + p*(1.0 + p*(-1.0/3.0 + p*11.0/72.0));
        }
        else {
            w = log(x);
            if (x > 3.0)
                w -= log(w);
        }
        return halley_iteration(x, w);
    }
}