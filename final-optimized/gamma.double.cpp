#include "../src/xreal/xreal.h"

namespace {
const double CONST_E = 2.7182818284590452e+00;
const double CONST_PI = 3.1415926535897932e+00;
using namespace std;

// double adaptation(const double x, const double a, const double b) {
//   double s = 2*x;
//   double c = 0.0;
//   double terms[2] = {-a, -b};
//   double tmp;
//   for (int i = 0; i < 2; i++) {
//     tmp = s + terms[i];
//     if (fabs(s) >= fabs(terms[i]))
//       c = c + ((s-tmp) + terms[i]);
//     else
//       c = c + ((terms[i]-tmp) + s);
//     s = tmp;
//   }
//   s = s + c;
//   return s / (b-a);
// }

// double cheby_eval(double x, double l, double r, const double coef[], const int nums) {
//   double ux = adaptation(x, l, r);
//   double d = 0.0;
//   double dd = 0.0;

//   double ux2 = 2.0*ux;
//   double tmp;
//   // Clenshaw method
//   for (int i = nums-1; i>=1; i--) {
//     tmp = d;
//     d = ux2*d - dd + coef[i];
//     dd = tmp;
//   }
//   d = ux*d - dd + coef[0];
//   return d;
// }

// double cheby_series_1c(double x) {
//     const static int nums = 21;
//     const static double lb = 0.875;
//     const static double rb = 1.125;
//     const static double series_coef[] = {
//   6.45050030820686114876e-03,
//   -7.27428935024560454281e-02,
//   6.45886008595784311248e-03,
//   -1.97647879152229301849e-04,
//   8.38040631058663192913e-06,
//   -4.03187714835633691790e-07,
//   2.06886764686166725438e-08,
//   -1.10299629486933158746e-09,
//   6.03056165001229221594e-11,
//   -3.35671506729734812789e-12,
//   1.89371318783941594881e-13,
//   -1.07968382831317190581e-14,
//   6.20854706762156997502e-16,
//   -3.59552675175834193684e-17,
//   2.09478081327743741949e-18,
//   -1.22673020446378814215e-19,
//   7.21606546744868549833e-21,
//   -4.26142507683474157293e-22,
//   2.52531872288635289814e-23,
//   -1.50113870148716599262e-24,
//   8.94809408183140956345e-26,
//     };
//     return cheby_eval(x, lb, rb, series_coef, nums);
// }

// double cheby_series_2c(double x) {
//     const static int nums = 21;
//     const static double lb = 1.875;
//     const static double rb = 2.125;
//     const static double series_coef[] = {
//   2.52116138199945621184e-03,
//   5.27492401100004115622e-02,
//   2.52179118630618719003e-03,
//   -3.29574870200517925504e-05,
//   6.30150550284481563313e-07,
//   -1.41492622746145887431e-08,
//   3.46482523128875800606e-10,
//   -8.94544319319271458839e-12,
//   2.39152150026259333237e-13,
//   -6.55116958615572165523e-15,
//   1.82671820101206008579e-16,
//   -5.16239097439510649824e-18,
//   1.47424988396435098979e-19,
//   -4.24545362461058509509e-21,
//   1.23097684188984536006e-22,
//   -3.58968483646440164664e-24,
//   1.05188405818876514547e-25,
//   -3.09522237957042717492e-27,
//   9.14104267690556224625e-29,
//   -2.70826722484024470677e-30,
//   8.04682925126574979319e-32,
//     };
//     return cheby_eval(x, lb, rb, series_coef, nums);
// }

double power_series_1(double x) {
    double z = x - 1.0;
    const static int series_num = 9;
    const static double series_coef[] = {
      0.000000000000000000e+00,
     -5.772156649015328606e-01,
      8.224670334241132182e-01,
     -4.006856343865314285e-01,
      2.705808084277845479e-01,
     -2.073855510286739854e-01,
      1.695571769974081901e-01,
     -1.440498967685959941e-01,
      1.255096695244721419e-01,
    };
    double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

double power_series_2(double x) {
    double z = x - 2.0;
    const static int series_num = 8;
    const static double series_coef[] = {
      0.000000000000000000e+00,
      4.227843350984671394e-01,
      3.224670334241132182e-01,
     -6.735230105319809513e-02,
      2.058080842778454788e-02,
     -7.385551028673985266e-03,
      2.890510330741523286e-03,
     -1.192753911703137430e-03,
    };
    double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

const int vargamma = 7;
const int lanczos_coef_terms = 9;

static double lanczos_coef[] = {
    9.9999999999980993e-01,
    6.7652036812188510e+02,
    -1.2591392167224029e+03,
    7.7132342877765308e+02,
    -1.7661502916214060e+02,
    1.2507343278686905e+01,
    -1.3857109526572012e-01,
    9.9843695780195709e-06,
    1.5056327351493116e-07,
};

static double Ag(double z) {
    double ag = lanczos_coef[0];
    for (int i = 1; i < lanczos_coef_terms; i++) {
        ag += lanczos_coef[i] / (z + i);
    }
    return ag;
}

static double lngamma_lanczos(double x) {
    double LogRootTwoPi_ = 9.1893853320467274e-01;
    double z = x - 1.0;
    double t1 = (z+0.5)*(log((z+vargamma+0.5)/CONST_E));
    double t2 = LogRootTwoPi_ + log(Ag(z)) - vargamma;
    return t1 + t2;
}

static double near_zero_thres = 1.0000000000000000e-02;
static int near_zero_terms = 10;
static double near_zero_coefs[] = {
    -7.7215664901532861e-02,
    -1.0944004672027445e-02,
    9.2520923919113711e-02,
    -1.8271913165599813e-02,
    1.8004931096854798e-02,
    -6.8508853787238068e-03,
    3.9982395575684660e-03,
    -1.8943062168710780e-03,
    9.7473237804513221e-04,
    -4.8434392722255893e-04,
};

static double lngamma_near_zero(double x) {
    double g = 0.0;
    // g(x) = Gamma(x)*x - 1/(1+x) - x/2
    for (int i = near_zero_terms-1; i >= 0; i--) {
        g = g * x + near_zero_coefs[i];
    }
    g = g * x;
    double gee = g + 1.0/(1.0+x) + 0.5*x;
    double val = log(gee / fabs(x));
    return val;
}
}

double lngamma(double x) {
    if (1.0-1e-2 <= x && x <= 1.0+1e-2) {
        return power_series_1(x);
    }
    if (2.0-1e-2 <= x && x <= 2.0+1e-2) {
        return power_series_2(x);
    }
    if (x == 0) {
        return NAN;
    }
    else if (fabs(x) < near_zero_thres) {
        double val = lngamma_near_zero(x);
        return val;
    }
    else if (x > 0) {
        double val = lngamma_lanczos(x);
        return val;
    }
    else if (x < 0) {
        double z = 1.0 - x;
        double s = sin(CONST_PI*z);
        double as = abs(s);
        double lngamma_z = lngamma_lanczos(z);
        double val = log(CONST_PI) - (log(as) + lngamma_z);
        return val;
    }
    return 0;
}