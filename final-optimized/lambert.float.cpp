#include <algorithm>
#include "../src/xreal/xreal.h"

namespace {
using namespace std;

float power_series_0(float x) {
    float z = x - 0.0l;
    const static int series_num = 5;
    const static float series_coef[] = {
        0.0000000000e+00f,
        1.0000000000e+00f,
        -1.0000000000e+00f,
        1.5000000651e+00f,
        -2.6666668179e+00f,
    };
    float val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

const float CONST_E = 2.7182818e+00f;
static int terms = 10;
static float one_over_e = 1.0 / CONST_E;

static const float coefs[] = {
    -1.0000000e+00f,
    2.3316440e+00f,
    -1.8121879e+00f,
    1.9366311e+00f,
    -2.3535512e+00f,
    3.0668589e+00f,
    -4.1753356e+00f,
    5.8580237e+00f,
    -8.4010322e+00f,
    1.2250754e+01f,
};

static float hornor_eval(float x) {
    float res = 0;
    for (int i = terms-1; i >= 0; i--) {
        res = res * x + coefs[i];
    }
    return res;
}

static int max_iters = 8;
static float tol_factor = 1.1920929e-07f;

static float halley_iteration(float x, float w) {
    for (int i = 0; i < max_iters; i++) {
        float tol;
        float e = exp(w);
        float p = w + 1.0;
        float t = w*e - x;

        // printf("w, t: %20.16g  %20.16g\n", (double)w, (double)t);

        if (w > 0) {
            t = (t/p)/e; /* Newton iteration */
        }
        else {
            t /= e*p - 0.5*(p + 1.0)*t/p; /* Halley iteration */
        }

        w -= t;

        float temp = (1.0/(fabs(p)*e));
        tol = 10 * tol_factor * max(fabs(w), temp);

        if (fabs(t) < tol) {
            return w;
        }
    }
    // Should not reach here
    return w;
}
} // namespace

float lambert_W0_f(float x) {
    if (-1e-4f <= x && x <= 1e-4f) {
        return power_series_0(x);
    }

    float one_over_e = 1.0/CONST_E;
    float comp_term = 9.1497552e-09f;
    float q = x + one_over_e - comp_term;

    if (x == 0) {
        return 0.0;
    }
    else if (q <= 0.0) {
        return -1.0;
    }
    else if (q < 1.0e-3) {
        float r = sqrt(q);
        float y = hornor_eval(r);
        return y;
    }
    else {
        float w;
        if (x < 1.0) {
            float p = sqrt(2.0 * CONST_E * q);
            w = -1.0 + p*(1.0 + p*(-1.0/3.0 + p*11.0/72.0));
        }
        else {
            w = log(x);
            if (x > 3.0)
                w -= log(w);
        }
        return halley_iteration(x, w);
    }
}