#include "../src/xreal/xreal.h"

namespace {
const float CONST_PI = 3.1415927e+00f;
using namespace std;

float power_series_0(float x) {
    float z = x - 0.0;
    const static int series_num = 9;
    const static float series_coef[] = {
        0.000000000000000e+00f,
        1.000000000000000e+00f,
        2.500000000000000e-01f,
        1.111111111111169e-01f,
        6.250000000000688e-02f,
        3.999999979163825e-02f,
        2.777777757149674e-02f,
        2.041094132720125e-02f,
        1.562744472032085e-02f,
    };
    float val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

float adaptation(const float x, const float a, const float b) {
  float s = 2*x;
  float c = 0.0;
  float terms[2] = {-a, -b};
  float tmp;
  for (int i = 0; i < 2; i++) {
    tmp = s + terms[i];
    if (fabs(s) >= fabs(terms[i]))
      c = c + ((s-tmp) + terms[i]);
    else
      c = c + ((terms[i]-tmp) + s);
    s = tmp;
  }
  s = s + c;
  return s / (b-a);
}

float cheby_eval(float x, float l, float r, const float coef[], const int nums) {
  float ux = adaptation(x, l, r);
  float d = 0.0;
  float dd = 0.0;

  float ux2 = 2.0*ux;
  float tmp;
  // Clenshaw method
  for (int i = nums-1; i>=1; i--) {
    tmp = d;
    d = ux2*d - dd + coef[i];
    dd = tmp;
  }
  d = ux*d - dd + coef[0];
  return d;
}

float cheby_series_0_5(float x) {
    const static int nums = 21;
    const static float lb = -0.5f;
    const static float rb = 0.5f;
    const static float series_coef[] = {
  3.287003209691451e-02f,
  5.112991663343523e-01f,
  3.343807933282850e-02f,
  3.924653627777561e-03f,
  5.858057468495125e-04f,
  9.976190387389106e-05f,
  1.846660980011915e-05f,
  3.620613490297094e-06f,
  7.403546456902297e-07f,
  1.563266957278922e-07f,
  3.385394147374324e-08f,
  7.482783173892275e-09f,
  1.682061130574058e-09f,
  3.835009404218822e-10f,
  8.849558174615158e-11f,
  2.063392967839577e-11f,
  4.854708843166071e-12f,
  1.151297812683378e-12f,
  2.749539703609528e-13f,
  6.607681318809594e-14f,
  1.596890151987645e-14f,
    };
    return cheby_eval(x, lb, rb, series_coef, nums);
}

float power_series_root_12(float x) {
    float z = x - 1.2595170e+01f + -3.4878789e-07f;
    const static int series_num = 7;
    const static float series_coef[] = {
        0.000000000000000e+00f,
        -1.945657416318589e-01f,
        4.300177565288113e-03f,
        -1.291882631106331e-04f,
        3.448648726948392e-06f,
        5.669025639627004e-10f,
        -1.266418380732351e-08f,
    };
    float val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

float squarelog(float x) {
    if (x > 0) {
        return log(x) * log(x);
    }
    else if (x < 0) {
        return log(-x) * log(-x) - CONST_PI*CONST_PI;
    }
    else {
        // nan
        return log(x)*log(x);
    }
}

const float threshold = 1.0000000e-07f;
} // namespace

float dilog_f(float z) {
    if (-1e-2f <= z && z <= 1e-2f) {
        return power_series_0(z);
    }
    if (-0.5f <= z && z <= 0.5f) {
        return cheby_series_0_5(z);
    }
    if (1.2595170369845016e+01f - 1e-2f <= z && z <= 1.2595170369845016e+01f + 1e-2f) {
        return power_series_root_12(z);
    }
    if (fabs(z) <= 0.5) {
        int terms = 300;
        float Sum = 0.0;
        for (int n = 1; n <= terms; n++) {
            float fpn = n;
            float upper = pow(z, n);
            float lower = (fpn*(fpn+1)*(fpn+2))*(fpn*(fpn+1)*(fpn+2));
            float term = upper/lower;
            Sum += term;
            if (fabs(term/Sum) < threshold)
                break;
        }
        float upper_val = 4*z*z*Sum + 4*z + 23.0*z*z/4.0 + 3.0*(1-z*z)*log(1-z);
        float lower_val = 1.0 + z * (4.0 + z);
        float val = upper_val / lower_val;
        return val;
    }
    else if (0.5 < z && z <= 1) {
        float t = 1-z;
        float val = -dilog_f(t) - log(t)*log(z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (1 < z) {
        // fabs(z) > 1
        float t = 1/z;
        float val = -dilog_f(t) - 0.5*squarelog(-t) - CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (z < -0.5) {
        float t = 1-z;
        float val = -dilog_f(t) - log(t)*log(-z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else
        return 0;
    // else if (fabs(z) <= 1) {
    //     xreal t = 1-z;
    //     xreal val = -dilog_f(t) - log(t)*log(z) + xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // else if (fabs(z) > 1) {
    //     // fabs(z) > 1
    //     xreal t = 1/z;
    //     xreal val = -dilog_f(t) - 0.5*squarelog(-t) - xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // return 0;
}