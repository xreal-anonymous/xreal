#include "../src/xreal/xreal.h"

namespace {
const float CONST_E = 2.7182818e+00f;
const float CONST_PI = 3.1415927e+00f;
using namespace std;

float power_series_1(float x) {
    float z = x - 1.0;
    const static int series_num = 7;
    const static float series_coef[] = {
      0.000000000000000000e+00f,
     -5.772156649015328606e-01f,
      8.224670334241132182e-01f,
     -4.006856343865314285e-01f,
      2.705808084277845479e-01f,
     -2.073855510286739854e-01f,
      1.695571769974081901e-01f,
    //  -1.440498967685959941e-01,
    //   1.255096695244721419e-01,
    };
    float val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

float power_series_2(float x) {
    float z = x - 2.0;
    const static int series_num = 6;
    const static float series_coef[] = {
      0.000000000000000000e+00f,
      4.227843350984671394e-01f,
      3.224670334241132182e-01f,
     -6.735230105319809513e-02f,
      2.058080842778454788e-02f,
     -7.385551028673985266e-03f,
    //   2.890510330741523286e-03,
    //  -1.192753911703137430e-03,
    };
    float val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}


const int vargamma = 5;
const int lanczos_coef_terms = 7;

static float lanczos_coef[] = {
    1.0000000e+00f,
    7.6180092e+01f,
    -8.6505320e+01f,
    2.4014098e+01f,
    -1.2317396e+00f,
    1.2086510e-03f,
    -5.3952394e-06f,
};

static float Ag(float z) {
    float ag = lanczos_coef[0];
    for (int i = 1; i < lanczos_coef_terms; i++) {
        ag += lanczos_coef[i] / (z + i);
    }
    return ag;
}

static float lngamma_lanczos(float x) {
    float LogRootTwoPi_ = 9.1893853e-01f;
    float z = x - 1.0;
    float t1 = (z+0.5)*(log((z+vargamma+0.5)/CONST_E));
    float t2 = LogRootTwoPi_ + log(Ag(z)) - vargamma;
    return t1 + t2;
}

static float near_zero_thres = 1.0000000e-02f;
static int near_zero_terms = 8;
static float near_zero_coefs[] = {
    -7.7215665e-02f,
    -1.0944005e-02f,
    9.2520924e-02f,
    -1.8271913e-02f,
    1.8004931e-02f,
    -6.8508854e-03f,
    3.9982396e-03f,
    -1.8943062e-03f,
};

static float lngamma_near_zero(float x) {
    float g = 0.0;
    // g(x) = Gamma(x)*x - 1/(1+x) - x/2
    for (int i = near_zero_terms-1; i >= 0; i--) {
        g = g * x + near_zero_coefs[i];
    }
    g = g * x;
    float gee = g + 1.0/(1.0+x) + 0.5*x;
    float val = log(gee / fabs(x));
    return val;
}
} // namespace

float lngamma_f(float x) {
    if (x == 0) {
        return NAN;
    }
    if (1.0-2e-2 <= x && x <= 1.0+2e-2) {
        return power_series_1(x);
    }
    if (2.0-2e-2 <= x && x <= 2.0+2e-2) {
        return power_series_2(x);
    }
    else if (fabs(x) < near_zero_thres) {
        float val = lngamma_near_zero(x);
        return val;
    }
    else if (x > 0) {
        float val = lngamma_lanczos(x);
        return val;
    }
    else if (x < 0) {
        float z = 1.0 - x;
        float s = sin(CONST_PI*z);
        float as = abs(s);
        float lngamma_z = lngamma_lanczos(z);
        float val = log(CONST_PI) - (log(as) + lngamma_z);
        return val;
    }
    return 0;
}