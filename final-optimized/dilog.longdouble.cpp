#include "../src/xreal/xreal.h"

namespace {
const long double CONST_PI = 3.1415926535897932384626433832795029e+00l;
using namespace std;

long double power_series_0(long double x) {
    long double z = x - 0.0;
    const static int series_num = 16;
    const static long double series_coef[] = {
        0.0000000000000000000000000e+00l,
        1.0000000000000000000000000e+00l,
        2.5000000000000000000000000e-01l,
        1.1111111111111111111111111e-01l,
        6.2500000000000000000000000e-02l,
        3.9999999999999999999999999e-02l,
        2.7777777777777777777801131e-02l,
        2.0408163265306122449017518e-02l,
        1.5624999999999999048583187e-02l,
        1.2345679012345677748151761e-02l,
        1.0000000000020757795467714e-02l,
        8.2644628099412596483858113e-03l,
        6.9444441953586893236896833e-03l,
        5.9171595058915769719938394e-03l,
        5.1035905863033045706692263e-03l,
        4.4459153287794996965262192e-03l,
    };
    long double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

long double adaptation(const long double x, const long double a, const long double b) {
  long double s = 2*x;
  long double c = 0.0;
  long double terms[2] = {-a, -b};
  long double tmp;
  for (int i = 0; i < 2; i++) {
    tmp = s + terms[i];
    if (fabs(s) >= fabs(terms[i]))
      c = c + ((s-tmp) + terms[i]);
    else
      c = c + ((terms[i]-tmp) + s);
    s = tmp;
  }
  s = s + c;
  return s / (b-a);
}

long double cheby_eval(long double x, long double l, long double r, const long double coef[], const int nums) {
  long double ux = adaptation(x, l, r);
  long double d = 0.0;
  long double dd = 0.0;

  long double ux2 = 2.0*ux;
  long double tmp;
  // Clenshaw method
  for (int i = nums-1; i>=1; i--) {
    tmp = d;
    d = ux2*d - dd + coef[i];
    dd = tmp;
  }
  d = ux*d - dd + coef[0];
  return d;
}

long double cheby_series_0_5(long double x) {
    const static int nums = 36;
    const static long double lb = -0.5l;
    const static long double rb = 0.5l;
    const static long double series_coef[] = {
  3.2870032096914507519609759e-02l,
  5.1129916633435233320910714e-01l,
  3.3438079332828497264310579e-02l,
  3.9246536277775605590434182e-03l,
  5.8580574684951253252275973e-04l,
  9.9761903873891055250762711e-05l,
  1.8466609800119150687809521e-05l,
  3.6206134902970938209082256e-06l,
  7.4035464569022967765182844e-07l,
  1.5632669572789216650128067e-07l,
  3.3853941473743243751593978e-08l,
  7.4827831738922745701754840e-09l,
  1.6820611305740575758003794e-09l,
  3.8350094042188223686216137e-10l,
  8.8495581746151583233447679e-11l,
  2.0633929678395774808059428e-11l,
  4.8547088431660706681804087e-12l,
  1.1512978126833779629758030e-12l,
  2.7495397036095282232338368e-13l,
  6.6076813188095939099793553e-14l,
  1.5968901519876447855896818e-14l,
  3.8788149038056721683557659e-15l,
  9.4648944099783739697386629e-16l,
  2.3192486945749496859093491e-16l,
  5.7047761976812322219282079e-17l,
  1.4081652242643821322605295e-17l,
  3.4871638921107511796256400e-18l,
  8.6614080502079484327762995e-19l,
  2.1572840359137963535726134e-19l,
  5.3869648995823934753510379e-20l,
  1.3484128549725446739716236e-20l,
  3.3827895960217626846088264e-21l,
  8.5042974617886250019327619e-22l,
  2.1421850514093635295736945e-22l,
  5.4060417732205459418912895e-23l,
  1.3666551189182521822390965e-23l,
    };
    return cheby_eval(x, lb, rb, series_coef, nums);
}

long double power_series_root_12(long double x) {
    long double z = x - 1.2595170369845016128569625823985234e+01l + -7.0270670879373003547911510572359118e-20l;
    const static int series_num = 16;
    const static long double series_coef[] = {
  0.0000000000000000000000000e+00l,
  -1.9456574163185891825158493e-01l,
  4.3001775652881131128740722e-03l,
  -1.2918826311063310631444556e-04l,
  3.4486487269483922203876822e-06l,
  5.6689969454377410180904098e-10l,
  -1.2664183490611432998770831e-08l,
  1.6396679386439441169278262e-09l,
  -1.6422107463007326934905491e-10l,
  1.4964490502098715581830156e-11l,
  -1.3021429288669403791783889e-12l,
  1.1041551812367537015856725e-13l,
  -9.2167476016251350002549364e-15l,
  7.6164646497409423537470383e-16l,
  -6.2521675021002554762233032e-17l,
  5.1091095236967871375723367e-18l,
    };
    long double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

long double squarelog(long double x) {
    if (x > 0) {
        return log(x) * log(x);
    }
    else if (x < 0) {
        return log(-x) * log(-x) - CONST_PI*CONST_PI;
    }
    else {
        // nan
        return log(x)*log(x);
    }
}

const long double threshold = 4.9999999999999998762296341763006593e-20l;
} // namespace

long double dilog_ld(long double z) {
    if (-1e-2l <= z && z <= 1e-2l) {
        return power_series_0(z);
    }
    if (-0.5l <= z && z <= 0.5l) {
        return cheby_series_0_5(z);
    }
    if (1.2595170369845016e+01l - 1e-2l <= z && z <= 1.2595170369845016e+01l + 1e-2l) {
        return power_series_root_12(z);
    }
    if (fabs(z) <= 0.5) {
        int terms = 300;
        long double Sum = 0.0;
        for (int n = 1; n <= terms; n++) {
            long double fpn = n;
            long double upper = pow(z, n);
            long double lower = (fpn*(fpn+1)*(fpn+2))*(fpn*(fpn+1)*(fpn+2));
            long double term = upper/lower;
            Sum += term;
            if (fabs(term/Sum) < threshold)
                break;
        }
        long double upper_val = 4*z*z*Sum + 4*z + 23.0*z*z/4.0 + 3.0*(1-z*z)*log(1-z);
        long double lower_val = 1.0 + z * (4.0 + z);
        long double val = upper_val / lower_val;
        return val;
    }
    else if (0.5 < z && z <= 1) {
        long double t = 1-z;
        long double val = -dilog_ld(t) - log(t)*log(z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (1 < z) {
        // fabs(z) > 1
        long double t = 1/z;
        long double val = -dilog_ld(t) - 0.5*squarelog(-t) - CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (z < -0.5) {
        long double t = 1-z;
        long double val = -dilog_ld(t) - log(t)*log(-z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else
        return 0;
    // else if (fabs(z) <= 1) {
    //     xreal t = 1-z;
    //     xreal val = -dilog_ld(t) - log(t)*log(z) + xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // else if (fabs(z) > 1) {
    //     // fabs(z) > 1
    //     xreal t = 1/z;
    //     xreal val = -dilog_ld(t) - 0.5*squarelog(-t) - xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // return 0;
}