#include "mpreal.h"
#include <random>
#include <gsl/gsl_sf.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>

// MPFR bits
int precision = 256;

// Specify function type
using oracleFunc_t = mpfr::mpreal(*)(double);

template<class FP>
using fpFunc_t = FP(*)(FP);


// General function for relative error
template<class FP>
double err(double input, fpFunc_t<FP> fp, oracleFunc_t op) {
    FP y1 = fp(input);
    mpfr::mpreal y2 = op(input);
    mpfr::mpreal abserr = fabs(y2 - y1);
    mpfr::mpreal relerr = fabs(abserr / y2);
    return (double)relerr;
}

template<class FP, class FP2>
double test_magnitude(
    double center,
    double high_eps,
    double low_eps,
    int num,
    fpFunc_t<FP> fp,
    oracleFunc_t op,
    fpFunc_t<FP2> gsl_fp)
{
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    double high_exp = log(high_eps);
    double low_exp = log(low_eps);

    std::uniform_real_distribution<double> uni_dis(high_exp, low_exp);

    double max_err = 0.0;
    double max_input = 0.0;
    for (int i = 0; i < num; i++) {
        double cur_exp = uni_dis(gen64);
        double cur_eps = exp(cur_exp);
        FP input = center + cur_eps;
        double e = err(input, fp, op);
        if (e > max_err) {
            max_err = e;
            max_input = input;
        }
    }
    printf("Sub-region Max Error: %.2e, when input = %.16e\n", max_err, max_input);
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(max_input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(max_input) << "\n";
    std::cout << "    AC: " << op(max_input).toString("%.20Re") << "\n";
    // printf("    FP: %.16e\n", fp(max_input));
    // printf("   Ref: %.16e\n", gsl_fp(max_input));
    // printf("    AC: %.16e\n", (double)op(max_input));
    return max_err;
}

template<class FP, class FP2>
double test_uniform(
    double a,
    double b,
    int num,
    fpFunc_t<FP> fp,
    oracleFunc_t op,
    fpFunc_t<FP2> gsl_fp)
{
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    std::uniform_real_distribution<double> uni_dis(a, b);

    double max_err = 0.0;
    double max_input = 0.0;
    for (int i = 0; i < num; i++) {
        FP input = uni_dis(gen64);
        double e = err(input, fp, op);
        if (e > max_err) {
            max_err = e;
            max_input = input;
        }
    }
    printf("Sub-region Max Error: %.2e, when input = %.16e\n", max_err, max_input);
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(max_input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(max_input) << "\n";
    std::cout << "    AC: " << op(max_input).toString("%.20Re") << "\n";
    // printf("    FP: %.16e\n", fp(max_input));
    // printf("   Ref: %.16e\n", gsl_fp(max_input));
    // printf("    AC: %.16e\n", (double)op(max_input));
    return max_err;
}

template<class FP, class FP2>
void test_manually(double input, fpFunc_t<FP> fp, oracleFunc_t op, fpFunc_t<FP2> gsl_fp) {
    printf("Err: %.2e\n", err(input, fp, op));
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(input) << "\n";
    std::cout << "    AC: " << op(input).toString("%.20Re") << "\n";
    // printf("    FP: %.16e\n", fp(input));
    // printf("   Ref: %.16e\n", gsl_fp(input));
    // printf("    AC: %.16e\n", (double)op(input));
}

// Specify program under test
// our loggamma
float lngamma_f(float);
double lngamma(double);
long double lngamma_ld(long double);
// gsl's loggamma
double gsl_sf_lngamma(double);
// ground truth loggamma
mpfr::mpreal loggamma(const double x) {
    mpfr::mpreal::set_default_prec(precision);
    return mpfr::lngamma(x);
}

void test_gamma() {
    printf("\n====================\n");
    printf("Loggamma Function\n");
    mpfr::mpreal::set_default_prec(precision);

    char target_name[100];
    // float
    // fpFunc_t<float> fp = lngamma_f;
    // double
    fpFunc_t<double> fp = lngamma;
    strcpy(target_name, "double");
    // long double
    // fpFunc_t<long double> fp = lngamma_ld;

    fpFunc_t<double> gsl_fp = gsl_sf_lngamma;
    oracleFunc_t op = loggamma;

    printf("\n-------For xreal's loggamma (%s target)-------\n", target_name);
    // around 0, 1, 2
    test_magnitude(0, 1e-1, 1e-35, 1000, fp, op, gsl_fp);
    test_magnitude(1, 1e-2, 1e-20, 1000, fp, op, gsl_fp);
    test_magnitude(2, 1e-2, 1e-20, 1000, fp, op, gsl_fp);
    // from 1 to 1e30
    test_magnitude(0, 1, 1e30, 1000, fp, op, gsl_fp);
    // general test
    test_uniform(0, 10000, 50000, fp, op, gsl_fp);

    printf("\n-------For GSL's loggamma-------\n");
        // around 0, 1, 2
    test_magnitude(0, 1e-1, 1e-35, 1000, gsl_fp, op, fp);
    test_magnitude(1, 1e-2, 1e-20, 1000, gsl_fp, op, fp);
    test_magnitude(2, 1e-2, 1e-20, 1000, gsl_fp, op, fp);
    // from 1 to 1e200
    test_magnitude(0, 1, 1e30, 1000, gsl_fp, op, fp);
    // general test
    test_uniform(0, 10000, 50000, gsl_fp, op, fp);
}

// Specify program under test
// our dilog
float dilog_f(float);
double dilog(double);
long double dilog_ld(long double);
// gsl's dilog
double gsl_sf_dilog(double);
// ground truth dilog
mpfr::mpreal li2(const double x) {
    mpfr::mpreal::set_default_prec(precision);
    return mpfr::li2(x);
}

void test_dilog() {
    printf("\n====================\n");
    printf("Dilog Function\n");
    mpfr::mpreal::set_default_prec(precision);
    
    char target_name[100];
    // float
    // fpFunc_t<float> fp = dilog_f;
    // double
    fpFunc_t<double> fp = dilog;
    strcpy(target_name, "double");
    // long double
    // fpFunc_t<long double> fp = dilog_ld;
    fpFunc_t<double> gsl_fp = gsl_sf_dilog;
    oracleFunc_t op = li2;

    printf("\n-------For xreal's dilog (%s target)-------\n", target_name);
    // around 0
    test_magnitude(0, 1e-1, 1e-50, 1000, fp, op, gsl_fp);
    // -0.5, 0.5
    test_uniform(-0.5, 0.5, 10000, fp, op, gsl_fp);
    // around root
    test_magnitude(1.2595170369845016e+01, 1e-3, 1e-18, 1000, fp, op, gsl_fp);
    // general test
    test_uniform(-10000, 10000, 100000, fp, op, gsl_fp);

    printf("\n-------For GSL's dilog-------\n");
    // around 0
    test_magnitude(0, 1e-1, 1e-50, 1000, gsl_fp, op, fp);
    // -0.5, 0.5
    test_uniform(-0.5, 0.5, 10000, gsl_fp, op, fp);
    // around root
    test_magnitude(1.2595170369845016e+01, 1e-3, 1e-18, 1000, gsl_fp, op, fp);
    // general test
    test_uniform(-10000, 10000, 100000, gsl_fp, op, fp);

    // test manually
    // while (1) {
    //     double in;
    //     std::cin >> in;
    //     test_manually(in, fp, op, gsl_fp);
    // }
}

// Specify program under test
// our lambertw
float lambert_W0_f(float);
double lambert_W0(double);
long double lambert_W0_ld(long double);
// gsl's dilog
double gsl_sf_lambert_W0(double);
// ground truth dilog
mpfr::mpreal lambert_W0_oracle(double x);

void test_lambert() {
    printf("\n====================\n");
    printf("Lambert Function\n");
    mpfr::mpreal::set_default_prec(precision);

    char target_name[100];
    // float
    // fpFunc_t<float> fp = lambert_W0_f;
    // double
    fpFunc_t<double> fp = lambert_W0;
    strcpy(target_name, "double");
    // long double
    // fpFunc_t<long double> fp = lambert_W0_ld;
    fpFunc_t<double> gsl_fp = gsl_sf_lambert_W0;
    oracleFunc_t op = lambert_W0_oracle;

    printf("\n-------For xreal's lambert (%s target)-------\n", target_name);
    test_magnitude(0, 1e-2, 1e-60, 1000, fp, op, gsl_fp);
    test_magnitude(-0.36787944117144233, 1e1, 1e-20, 1000, fp, op, gsl_fp);
    test_magnitude(0, 1e-2, 1e30, 1000, fp, op, gsl_fp);
    test_uniform(-0.36787944117144233, 1e6, 10000, fp, op, gsl_fp);

    printf("\n-------For GSL's lambert-------\n");
    test_magnitude(0, 1e-2, 1e-60, 1000, gsl_fp, op, fp);
    test_magnitude(-0.36787944117144233, 1e1, 1e-20, 1000, gsl_fp, op, fp);
    test_magnitude(0, 1e-2, 1e30, 1000, gsl_fp, op, fp);
    test_uniform(-0.36787944117144233, 1e6, 10000, gsl_fp, op, fp);
}

template<class FP>
double test_runtime(fpFunc_t<FP> fp, double a, double b, int num, std::string funcname) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    std::uniform_real_distribution<double> uni_dis(a, b);

    // Init inputs.
    std::vector<double> inputs(num);
    for (int i = 0; i < num; i++) {
        inputs[i] = uni_dis(gen64);
    }

    // Measure runtime
    double t;
    auto start_time = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < num; i++) {
        fp(inputs[i]);
    }
    auto end_time = std::chrono::high_resolution_clock::now();

    int64_t all_time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time).count();
    double each_time = all_time / (double)num;
    printf("%.1f ns on %s\n", each_time, funcname.c_str());
    return each_time;
}

int main() {

    // Test Accuracy
    test_gamma();
    test_dilog();
    test_lambert();

    // Test time
    // int num = 1000000;
    // test_runtime(lngamma, 0, 1e5, num, "xreal lngamma");
    // test_runtime(gsl_sf_lngamma, 0, 1e5, num, "GSL lngamma");

    // test_runtime(dilog, -1e6, 1e6, num, "xreal dilog");
    // test_runtime(gsl_sf_dilog, -1e6, 1e6, num, "GSL dilog");

    // test_runtime(lambert_W0, -0.36787944117144233, 100, num, "xreal lambert");
    // test_runtime(gsl_sf_lambert_W0, -0.36787944117144233, 100, num, "GSL lambert");
    return 0;
}