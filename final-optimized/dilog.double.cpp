#include "../src/xreal/xreal.h"

namespace {
const double CONST_PI = 3.1415926535897932e+00;
using namespace std;

double power_series_0(double x) {
    double z = x - 0.0;
    const static int series_num = 11;
    const static double series_coef[] = {
        0.00000000000000000000e+00,
        1.00000000000000000000e+00,
        2.50000000000000000000e-01,
        1.11111111111111110934e-01,
        6.24999999999999997531e-02,
        4.00000000000099450751e-02,
        2.77777777777890648321e-02,
        2.04081630379949288977e-02,
        1.56249997742640960259e-02,
        1.23479520281115451862e-02,
        1.00020520503512513655e-02,
    };
    double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

double adaptation(const double x, const double a, const double b) {
  double s = 2*x;
  double c = 0.0;
  double terms[2] = {-a, -b};
  double tmp;
  for (int i = 0; i < 2; i++) {
    tmp = s + terms[i];
    if (fabs(s) >= fabs(terms[i]))
      c = c + ((s-tmp) + terms[i]);
    else
      c = c + ((terms[i]-tmp) + s);
    s = tmp;
  }
  s = s + c;
  return s / (b-a);
}

double cheby_eval(double x, double l, double r, const double coef[], const int nums) {
  double ux = adaptation(x, l, r);
  double d = 0.0;
  double dd = 0.0;

  double ux2 = 2.0*ux;
  double tmp;
  // Clenshaw method
  for (int i = nums-1; i>=1; i--) {
    tmp = d;
    d = ux2*d - dd + coef[i];
    dd = tmp;
  }
  d = ux*d - dd + coef[0];
  return d;
}

double cheby_series_0_5(double x) {
    const static int nums = 26;
    const static double lb = -0.5;
    const static double rb = 0.5;
    const static double series_coef[] = {
  3.28700320969145075196e-02,
  5.11299166334352333209e-01,
  3.34380793328284972643e-02,
  3.92465362777756055904e-03,
  5.85805746849512532523e-04,
  9.97619038738910552508e-05,
  1.84666098001191506878e-05,
  3.62061349029709382091e-06,
  7.40354645690229677652e-07,
  1.56326695727892166501e-07,
  3.38539414737432437516e-08,
  7.48278317389227457018e-09,
  1.68206113057405757580e-09,
  3.83500940421882236862e-10,
  8.84955817461515832334e-11,
  2.06339296783957748081e-11,
  4.85470884316607066818e-12,
  1.15129781268337796298e-12,
  2.74953970360952822323e-13,
  6.60768131880959390998e-14,
  1.59689015198764478559e-14,
  3.87881490380567216836e-15,
  9.46489440997837396974e-16,
  2.31924869457494968591e-16,
  5.70477619768123222193e-17,
  1.40816522426438213226e-17,
    };
    return cheby_eval(x, lb, rb, series_coef, nums);
}

double power_series_root_12(double x) {
    double z = x - 1.2595170369845016e+01 + 2.3845420727593159e-16;
    const static int series_num = 11;
    const static double series_coef[] = {
        0.00000000000000000000e+00,
        -1.94565741631858918252e-01,
        4.30017756528811311287e-03,
        -1.29188263110633106314e-04,
        3.44864872694839222039e-06,
        5.66899694543774101809e-10,
        -1.26641834906114329988e-08,
        1.63966793864394381329e-09,
        -1.64221074630073239395e-10,
        1.49644908057413941350e-11,
        -1.30214295609824045761e-12,
    };
    double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

double squarelog(double x) {
    if (x > 0) {
        return log(x) * log(x);
    }
    else if (x < 0) {
        return log(-x) * log(-x) - CONST_PI*CONST_PI;
    }
    else {
        // nan
        return log(x)*log(x);
    }
}

const double threshold = 9.9999999999999998e-17;
}

double dilog(double z) {
    if (-1e-2 <= z && z <= 1e-2) {
        return power_series_0(z);
    }
    if (-0.5 <= z && z <= 0.5) {
        return cheby_series_0_5(z);
    }
    if (1.2595170369845016e+01 - 1e-2 <= z && z <= 1.2595170369845016e+01 + 1e-2) {
        return power_series_root_12(z);
    }
    if (fabs(z) <= 0.5) {
        int terms = 300;
        double Sum = 0.0;
        for (int n = 1; n <= terms; n++) {
            double fpn = n;
            double upper = pow(z, n);
            double lower = (fpn*(fpn+1)*(fpn+2))*(fpn*(fpn+1)*(fpn+2));
            double term = upper/lower;
            Sum += term;
            if (fabs(term/Sum) < threshold)
                break;
        }
        double upper_val = 4*z*z*Sum + 4*z + 23.0*z*z/4.0 + 3.0*(1-z*z)*log(1-z);
        double lower_val = 1.0 + z * (4.0 + z);
        double val = upper_val / lower_val;
        return val;
    }
    else if (0.5 < z && z <= 1) {
        double t = 1-z;
        double val = -dilog(t) - log(t)*log(z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (1 < z) {
        // fabs(z) > 1
        double t = 1/z;
        double val = -dilog(t) - 0.5*squarelog(-t) - CONST_PI*CONST_PI/6.0;
        return val;
    }
    else if (z < -0.5) {
        double t = 1-z;
        double val = -dilog(t) - log(t)*log(-z) + CONST_PI*CONST_PI/6.0;
        return val;
    }
    else
        return 0;
    // else if (fabs(z) <= 1) {
    //     xreal t = 1-z;
    //     xreal val = -dilog(t) - log(t)*log(z) + xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // else if (fabs(z) > 1) {
    //     // fabs(z) > 1
    //     xreal t = 1/z;
    //     xreal val = -dilog(t) - 0.5*squarelog(-t) - xreal::pi*xreal::pi/6.0;
    //     return val;
    // }
    // return 0;
}