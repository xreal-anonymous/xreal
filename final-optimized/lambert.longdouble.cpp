#include <algorithm>
#include "../src/xreal/xreal.h"

namespace {
using namespace std;

long double power_series_0(long double x) {
    long double z = x - 0.0l;
    const static int series_num = 9;
    const static long double series_coef[] = {
        0.0000000000000000000000000e+00l,
        1.0000000000000000000000000e+00l,
        -1.0000000000000000000000000e+00l,
        1.5000000000000000000000556e+00l,
        -2.6666666666666666666668561e+00l,
        5.2083333333333133153246823e+00l,
        -1.0799999999999943163022169e+01l,
        2.3343058224623301241341595e+01l,
        -5.2012705148932616939331169e+01l,
    };
    long double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

const long double CONST_E = 2.7182818284590452353602874713526625e+00l;
static int terms = 15;
static long double one_over_e = 1.0 / CONST_E;

static const long double coefs[] = {
    -1.0000000000000000000000000000000000e+00l,
    2.3316439815971242033635360621684009e+00l,
    -1.8121878856393634902401916475684417e+00l,
    1.9366311144923597553632774576683831e+00l,
    -2.3535512018816145168215435615164840e+00l,
    3.0668589010506319128931489227040075e+00l,
    -4.1753356002581771388549841774603736e+00l,
    5.8580237298747741488150538461186213e+00l,
    -8.4010322175239773709841616885138863e+00l,
    1.2250753501314460423767939360054732e+01l,
    -1.8100697012472442755377164038863043e+01l,
    2.7029044799010561650311482280446256e+01l,
    -4.0715462808260627286134753446473663e+01l,
    6.1782846187096525741232078166317455e+01l,
    -9.4336648861866933961845324841810059e+01l,
};

static long double hornor_eval(long double x) {
    long double res = 0;
    for (int i = terms-1; i >= 0; i--) {
        res = res * x + coefs[i];
    }
    return res;
}

static int max_iters = 10;
static long double tol_factor = 5.4210108624275642997547974751339942e-20l;

static long double halley_iteration(long double x, long double w) {
    for (int i = 0; i < max_iters; i++) {
        long double tol;
        long double e = exp(w);
        long double p = w + 1.0;
        long double t = w*e - x;

        // printf("w, t: %20.16g  %20.16g\n", (double)w, (double)t);

        if (w > 0) {
            t = (t/p)/e; /* Newton iteration */
        }
        else {
            t /= e*p - 0.5*(p + 1.0)*t/p; /* Halley iteration */
        }

        w -= t;

        long double temp = (1.0/(fabs(p)*e));
        tol = 10 * tol_factor * max(fabs(w), temp);

        if (fabs(t) < tol) {
            return w;
        }
    }
    // Should not reach here
    return w;
}
} // namespace

long double lambert_W0_ld(long double x) {
    if (-1e-4l <= x && x <= 1e-4l) {
        return power_series_0(x);
    }

    long double one_over_e = 1.0/CONST_E;
    long double comp_term = -1.2466256482800212513637929217473973e-20l;
    long double q = x + one_over_e - comp_term;

    if (x == 0) {
        return 0.0;
    }
    else if (q <= 0.0) {
        return -1.0;
    }
    else if (q < 1.0e-3) {
        long double r = sqrt(q);
        long double y = hornor_eval(r);
        return y;
    }
    else {
        long double w;
        if (x < 1.0) {
            long double p = sqrt(2.0 * CONST_E * q);
            w = -1.0 + p*(1.0 + p*(-1.0/3.0 + p*11.0/72.0));
        }
        else {
            w = log(x);
            if (x > 3.0)
                w -= log(w);
        }
        return halley_iteration(x, w);
    }
}
