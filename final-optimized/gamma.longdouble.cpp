#include "../src/xreal/xreal.h"

namespace {
const long double CONST_E = 2.7182818284590452353602874713526625e+00l;
const long double CONST_PI = 3.1415926535897932384626433832795029e+00l;
using namespace std;

long double power_series_1(long double x) {
    long double z = x - 1.0;
    const static int series_num = 12;
    const static long double series_coef[] = {
        0.00000000000000000000e+00l,
        -5.77215664901532860607e-01l,
        8.22467033424113218236e-01l,
        -4.00685634386531428467e-01l,
        2.70580808427784547879e-01l,
        -2.07385551028673985266e-01l,
        1.69557176997408189953e-01l,
        -1.44049896768846118121e-01l,
        1.25509669524743027199e-01l,
        -1.11334265869564668999e-01l,
        1.00099457513113950023e-01l,
        -9.09540171462354324738e-02l,
    };
    long double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

long double power_series_2(long double x) {
    long double z = x - 2.0;
    const static int series_num = 8;
    const static long double series_coef[] = {
        0.00000000000000000000e+00l,
        4.22784335098467139393e-01l,
        3.22467033424113218236e-01l,
        -6.73523010531980951332e-02l,
        2.05808084277845478790e-02l,
        -7.38555102867398526627e-03l,
        2.89051033074152328575e-03l,
        -1.19275391170326097711e-03l,
        5.09669524743042422103e-04l,
        -2.23154758453579379597e-04l,
        9.94575127818136083037e-05l,
        -4.49262367381362445734e-05l,
    };
    long double val = series_coef[series_num-1];
    for (int i = series_num-2; i>=0; i--) {
        val = val * z + series_coef[i];
    }
    return val;
}

const int vargamma = 9;
const int lanczos_coef_terms = 11;

static long double lanczos_coef[] = {
    1.0000000000000001746633015662352732e+00l,
    5.7164001882743413791357460354167895e+03l,
    -1.4815304267684139090440730520001298e+04l,
    1.4291492776574785540251096011666685e+04l,
    -6.3481602176414588132894549444149406e+03l,
    1.3016082860583218741047051534148927e+03l,
    -1.0817670535143696346792184991212752e+02l,
    2.6056965056117558277287783876220435e+00l,
    -7.4234525102014161515274445416879654e-03l,
    5.3841364325095640629609989548394710e-08l,
    -4.0235331412682363720673362927933096e-09l,
};

static long double Ag(long double z) {
    long double ag = lanczos_coef[0];
    for (int i = 1; i < lanczos_coef_terms; i++) {
        ag += lanczos_coef[i] / (z + i);
    }
    return ag;
}

static long double lngamma_lanczos(long double x) {
    long double LogRootTwoPi_ = 9.1893853320467274178032973640561764e-01l;
    long double z = x - 1.0;
    long double t1 = (z+0.5)*(log((z+vargamma+0.5)/CONST_E));
    long double t2 = LogRootTwoPi_ + log(Ag(z)) - vargamma;
    return t1 + t2;
}

static long double near_zero_thres = 1.0000000000000000208166817117216851e-02l;
static int near_zero_terms = 10;
static long double near_zero_coefs[] = {
    -7.7215664901532860606512090082402431e-02l,
    -1.0944004672027444604604348499365292e-02l,
    9.2520923919113710983439832643724885e-02l,
    -1.8271913165599812663619705978149150e-02l,
    1.8004931096854797895298586208625324e-02l,
    -6.8508853787238068461327466713415020e-03l,
    3.9982395575684660299215803354333133e-03l,
    -1.8943062168710780214245969116327625e-03l,
    9.7473237804513220532194035111191147e-04l,
    -4.8434392722255893294912240562980557e-04l,
};

static long double lngamma_near_zero(long double x) {
    long double g = 0.0;
    // g(x) = Gamma(x)*x - 1/(1+x) - x/2
    for (int i = near_zero_terms-1; i >= 0; i--) {
        g = g * x + near_zero_coefs[i];
    }
    g = g * x;
    long double gee = g + 1.0/(1.0+x) + 0.5*x;
    long double val = log(gee / fabs(x));
    return val;
}
}

long double lngamma_ld(long double x) {
    if (1.0-1e-2 <= x && x <= 1.0+1e-2) {
        return power_series_1(x);
    }
    if (2.0-1e-2 <= x && x <= 2.0+1e-2) {
        return power_series_2(x);
    }
    if (x == 0) {
        return NAN;
    }
    else if (fabs(x) < near_zero_thres) {
        long double val = lngamma_near_zero(x);
        return val;
    }
    else if (x > 0) {
        long double val = lngamma_lanczos(x);
        return val;
    }
    else if (x < 0) {
        long double z = 1.0 - x;
        long double s = sin(CONST_PI*z);
        long double as = abs(s);
        long double lngamma_z = lngamma_lanczos(z);
        long double val = log(CONST_PI) - (log(as) + lngamma_z);
        return val;
    }
    return 0;
}