#include "mpreal.h"
#include <random>
#include <gsl/gsl_sf.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include "../src/xreal/xreal.h"

int normal_oracle_precision = 256;

// Specify function type
using oracleFunc_t = mpfr::mpreal(*)(double);

template<class FP>
using fpFunc_t = FP(*)(FP);

// Specify function type
using oracleFunc_t = mpfr::mpreal(*)(double);

template<class FP>
using fpFunc_t = FP(*)(FP);


// General function for relative error
template<class FP>
double err(double input, fpFunc_t<FP> fp, oracleFunc_t op) {
    // Get fp result
    FP y1 = fp(input);

    // Get oracle result
    mpfr::mpreal::set_default_prec(normal_oracle_precision);
    mpfr::mpreal y2 = op(input);
    if (y2 == 0) return 0;

    mpfr::mpreal abserr = fabs(y2 - y1);
    mpfr::mpreal relerr = fabs(abserr / y2);
    return (double)relerr;
}

std::vector<double> gen_magnitude_inputs(double center, double high_eps, double low_eps, int num) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    double high_exp = log(high_eps);
    double low_exp = log(low_eps);

    std::uniform_real_distribution<double> uni_dis(high_exp, low_exp);

    std::vector<double> inputs;

    for (int i = 0; i < num; i++) {
        double cur_exp = uni_dis(gen64);
        double cur_offset = exp(cur_exp);
        double cur_input = center + cur_offset;
        inputs.push_back(cur_input);
    }

    return inputs;
}

std::vector<double> gen_uniform_inputs(double a, double b, int num) {
    static std::random_device rd;
    static std::mt19937_64 gen64{rd()};

    std::uniform_real_distribution<double> uni_dis(a, b);

    std::vector<double> inputs;

    for (int i = 0; i < num; i++) {
        double input = uni_dis(gen64);
        inputs.push_back(input);
    }

    return inputs;
}

template<class FP, class FP2>
double test_functions_with_inputs(fpFunc_t<FP> fp, oracleFunc_t op, fpFunc_t<FP2> gsl_fp, std::vector<double> inputs) {
    double num = inputs.size();

    double max_err = 0.0;
    double max_input = 0.0;
    for (int i = 0; i < num; i++) {
        FP input_fp = inputs[i];
        double input = (double)input_fp;
        double cur_err = err(input, fp, op);
        if (cur_err > max_err) {
            max_err = cur_err;
            max_input =(double) input;
        }
    }

    printf("Max Error: %.2e, when input = %.16e\n", max_err, max_input);
    std::cout << "    FP: " << std::setprecision(20) << std::scientific << fp(max_input) << "\n"; 
    std::cout << "   Ref: " << std::setprecision(20) << std::scientific << gsl_fp(max_input) << "\n";
    std::cout << "    AC: " << op(max_input).toString("%.20Re") << "\n";

    return max_err;
}

float naive_trans_gsl_lngamma(float x);
double naive_trans_gsl_lngamma(double x);
long double naive_trans_gsl_lngamma(long double x);
// gsl's loggamma
double gsl_sf_lngamma(double);
// ground truth loggamma
mpfr::mpreal loggamma(const double x) {
    return mpfr::lngamma(x);
}

void test_gamma() {
    printf("\n====================\n");
    printf("Loggamma Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(naive_trans_gsl_lngamma);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(naive_trans_gsl_lngamma);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(naive_trans_gsl_lngamma);

    fpFunc_t<double> gsl_fp = gsl_sf_lngamma;
    oracleFunc_t op = loggamma;

    // around 0, 1, 2
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-1, 1e-60, 1000);
    std::vector<double> inputs_sp_2 = gen_magnitude_inputs(1, 1e-2, 1e-20, 1000);
    std::vector<double> inputs_sp_3 = gen_magnitude_inputs(2, 1e-2, 1e-20, 1000);
    // general: from 1 to 1e30
    std::vector<double> inputs_general_1 = gen_magnitude_inputs(0, 1, 1e30, 1000);
    // general: from 0 to 10000
    std::vector<double> inputs_general_2 = gen_uniform_inputs(0, 10000, 10000);

    printf("\n-------For manipulated loggamma(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_2);

    printf("\n-------For manipulated loggamma(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_2);

    printf("\n-------For manipulated loggamma(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_2);
}


float naive_trans_gsl_dilog(float x);
double naive_trans_gsl_dilog(double x);
long double naive_trans_gsl_dilog(long double x);
// gsl's dilog
double gsl_sf_dilog(double);
// ground truth dilog
mpfr::mpreal li2(const double x) {
    return mpfr::li2(x);
}

void test_dilog() {
    printf("\n====================\n");
    printf("Dilog Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(naive_trans_gsl_dilog);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(naive_trans_gsl_dilog);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(naive_trans_gsl_dilog);

    fpFunc_t<double> gsl_fp = gsl_sf_dilog;
    oracleFunc_t op = li2;

    // around 0
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-1, 1e-60, 1000);
     // -0.5, 0.5
    std::vector<double> inputs_sp_2 = gen_uniform_inputs(-0.5, 0.5, 1000);
    // around root
    std::vector<double> inputs_sp_3 = gen_magnitude_inputs(1.2595170369845016e+01, 1e-3, 1e-18, 1000);
    // general test
    std::vector<double> inputs_general_1 = gen_uniform_inputs(-1e4, 1e4, 10000);

    printf("\n-------For manipulated dilog(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);

    printf("\n-------For manipulated dilog(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);

    printf("\n-------For manipulated dilog(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_3);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);
}

float naive_trans_gsl_lambert_W0(float x);
double naive_trans_gsl_lambert_W0(double x);
long double naive_trans_gsl_lambert_W0(long double x);
// gsl's lambert
double gsl_sf_lambert_W0(double);
// ground truth lambert
mpfr::mpreal lambert_W0_oracle(double x);

void test_lambert() {
    printf("\n====================\n");
    printf("Lambert Function\n");
    // float
    fpFunc_t<float> fp_f = static_cast<fpFunc_t<float>>(naive_trans_gsl_lambert_W0);
    // double
    fpFunc_t<double> fp_d = static_cast<fpFunc_t<double>>(naive_trans_gsl_lambert_W0);
    // long double
    fpFunc_t<long double> fp_ld = static_cast<fpFunc_t<long double>>(naive_trans_gsl_lambert_W0);

    fpFunc_t<double> gsl_fp = gsl_sf_lambert_W0;
    oracleFunc_t op = lambert_W0_oracle;

    // around 0
    std::vector<double> inputs_sp_1 = gen_magnitude_inputs(0, 1e-2, 1e-60, 1000);
    // around -1/e
    std::vector<double> inputs_sp_2 = gen_magnitude_inputs(-0.36787944117144233, 1e1, 1e-20, 1000);
    // from 0 to 1e30
    std::vector<double> inputs_general_1 = gen_magnitude_inputs(0, 1e-2, 1e30, 1000);
    // general test
    std::vector<double> inputs_general_2 = gen_uniform_inputs(-0.36787944117144233, 1e6, 10000);


    printf("\n-------For manipulated lambert(float)-------\n");
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_f, op, gsl_fp, inputs_general_2);

    printf("\n-------For manipulated lambert(double)-------\n");
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_d, op, gsl_fp, inputs_general_2);

    printf("\n-------For manipulated lambert(long double)-------\n");
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_sp_2);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_1);
    test_functions_with_inputs(fp_ld, op, gsl_fp, inputs_general_2);
}

int main() {
    test_gamma();
    test_dilog();
    test_lambert();
}