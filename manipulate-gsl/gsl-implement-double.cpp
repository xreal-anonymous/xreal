#include <cmath>

typedef double real;

real naive_trans_gsl_lambert_W0(real x);
real naive_trans_gsl_dilog(real x);
real naive_trans_gsl_lngamma(real x);

namespace {
///////////////////////////////////////////////////////////////////////////////
// GSL Utility
///////////////////////////////////////////////////////////////////////////////
#define INT_MIN -2147483648

#define GSL_DBL_EPSILON        2.2204460492503131e-16
#define GSL_SQRT_DBL_EPSILON   1.4901161193847656e-08
// #define M_E        2.71828182845904523536028747135      /* e */
#define M_LNPI     1.14472988584940017414342735135      /* ln(pi) */

#define GSL_MAX(a,b) ((a) > (b) ? (a) : (b))
#define GSL_MIN(a,b) ((a) < (b) ? (a) : (b))

#define GSL_SIGN(x)    ((x) >= 0.0 ? 1 : -1)

#define GSL_SUCCESS 0
#define GSL_FAILURE 1

#define GSL_NAN NAN

#define GSL_ERROR_SELECT_2(a,b)       ((a) != GSL_SUCCESS ? (a) : ((b) != GSL_SUCCESS ? (b) : GSL_SUCCESS))
#define GSL_ERROR_SELECT_3(a,b,c)     ((a) != GSL_SUCCESS ? (a) : GSL_ERROR_SELECT_2(b,c))
#define GSL_ERROR_SELECT_4(a,b,c,d)   ((a) != GSL_SUCCESS ? (a) : GSL_ERROR_SELECT_3(b,c,d))
#define GSL_ERROR_SELECT_5(a,b,c,d,e) ((a) != GSL_SUCCESS ? (a) : GSL_ERROR_SELECT_4(b,c,d,e))

real
GSL_MAX_DBL (real a, real b)
{
  return GSL_MAX (a, b);
}
real
GSL_MIN_DBL (real a, real b)
{
  return GSL_MIN (a, b);
}

struct gsl_sf_result_struct {
  real val;
  real err;
};
typedef struct gsl_sf_result_struct gsl_sf_result;

#define EVAL_RESULT(fn) \
   gsl_sf_result result; \
   int status = fn; \
   return result.val;


#define GSL_ERROR(reason, gsl_errno) \
       do { \
       gsl_error (reason, __FILE__, __LINE__, gsl_errno) ; \
       return gsl_errno ; \
       } while (0)

///////////////////////////////////////////////////////////////////////////////
// GSL Lambert W Function
///////////////////////////////////////////////////////////////////////////////

static real halley_iteration(real x, real w_initial, unsigned int max_iters) {
    real w = w_initial;
    unsigned int i;

    for (i = 0; i < max_iters; i++) {
        real tol;
        const real e = exp(w);
        const real p = w + 1.0;
        real t = w*e - x;

        if (w > 0) {
            t = (t/p)/e;
        } else {
            t /= e*p - 0.5*(p + 1.0)*t/p;
        };

        w -= t;

        tol = 10 * GSL_DBL_EPSILON * GSL_MAX_DBL(fabs(w), 1.0/(fabs(p)*e));

        if (fabs(t) < tol) {
            return w;
        }
    }
    // Should never get here
    return w;
}

static real
series_eval(real r)
{
  static const real c[12] = {
    -1.0,
     2.331643981597124203363536062168,
    -1.812187885639363490240191647568,
     1.936631114492359755363277457668,
    -2.353551201881614516821543561516,
     3.066858901050631912893148922704,
    -4.175335600258177138854984177460,
     5.858023729874774148815053846119,
    -8.401032217523977370984161688514,
     12.250753501314460424,
    -18.100697012472442755,
     27.029044799010561650
  };
  const real t_8 = c[8] + r*(c[9] + r*(c[10] + r*c[11]));
  const real t_5 = c[5] + r*(c[6] + r*(c[7]  + r*t_8));
  const real t_1 = c[1] + r*(c[2] + r*(c[3]  + r*(c[4] + r*t_5)));
  return c[0] + r*t_1;
}

///////////////////////////////////////////////////////////////////////////////
// GSL Clausen Function
///////////////////////////////////////////////////////////////////////////////
struct cheb_series_struct {
  real * c;   /* coefficients                */
  int order;    /* order of expansion          */
  real a;     /* lower interval point        */
  real b;     /* upper interval point        */
  int order_sp; /* effective single precision order */
};
typedef struct cheb_series_struct cheb_series;

static inline int
cheb_eval_e(const cheb_series * cs,
            const real x,
            gsl_sf_result * result)
{
  int j;
  real d  = 0.0;
  real dd = 0.0;

  real y  = (2.0*x - cs->a - cs->b) / (cs->b - cs->a);
  real y2 = 2.0 * y;

  real e = 0.0;

  for(j = cs->order; j>=1; j--) {
    real temp = d;
    d = y2*d - dd + cs->c[j];
    e += fabs(y2*temp) + fabs(dd) + fabs(cs->c[j]);
    dd = temp;
  }

  { 
    real temp = d;
    d = y*d - dd + 0.5 * cs->c[0];
    e += fabs(y*temp) + fabs(dd) + 0.5 * fabs(cs->c[0]);
  }

  result->val = d;
  result->err = GSL_DBL_EPSILON * e + fabs(cs->c[cs->order]);

  return GSL_SUCCESS;
}

int gsl_sf_angle_restrict_pos_err_e(const real theta, gsl_sf_result * result)
{
  /* synthetic extended precision constants */
  const real P1 = 4 * 7.85398125648498535156e-01;
  const real P2 = 4 * 3.77489470793079817668e-08;
  const real P3 = 4 * 2.69515142907905952645e-15;
  const real TwoPi = 2*(P1 + P2 + P3);

  const real y = 2*floor(theta/TwoPi);

  real r = ((theta - y*P1) - y*P2) - y*P3;

  if(r > TwoPi) {r = (((r-2*P1)-2*P2)-2*P3); }  /* r-TwoPi */
  else if (r < 0) { /* may happen due to FP rounding */
    r = (((r+2*P1)+2*P2)+2*P3); /* r+TwoPi */
  }

  result->val = r;

  if(fabs(theta) > 0.0625/GSL_DBL_EPSILON) {
    result->val = GSL_NAN;
    result->err = fabs(result->val);
    return GSL_FAILURE;
  }
  else if(fabs(theta) > 0.0625/GSL_SQRT_DBL_EPSILON) {
    result->err = GSL_DBL_EPSILON * fabs(result->val - theta);
    return GSL_SUCCESS;
  }
  else {
    real delta = fabs(result->val - theta);
    result->err = 2.0 * GSL_DBL_EPSILON * ((delta < M_PI) ? delta : M_PI);
    return GSL_SUCCESS;
  }
}

int gsl_sf_angle_restrict_pos_e(real * theta)
{
  gsl_sf_result r;
  int stat = gsl_sf_angle_restrict_pos_err_e(*theta, &r);
  *theta = r.val;
  return stat;
}

static real aclaus_data[15] = {
  2.142694363766688447e+00,
  0.723324281221257925e-01,
  0.101642475021151164e-02,
  0.3245250328531645e-04,
  0.133315187571472e-05,
  0.6213240591653e-07,
  0.313004135337e-08,
  0.16635723056e-09,
  0.919659293e-11,
  0.52400462e-12,
  0.3058040e-13,
  0.18197e-14,
  0.1100e-15,
  0.68e-17,
  0.4e-18
};
static cheb_series aclaus_cs = {
  aclaus_data,
  14,
  -1, 1,
  8  /* FIXME:  this is a guess, correct value needed here BJG */
};


/*-*-*-*-*-*-*-*-*-*-*-* Functions with Error Codes *-*-*-*-*-*-*-*-*-*-*-*/

int gsl_sf_clausen_e(real x, gsl_sf_result *result)
{
  const real x_cut = M_PI * GSL_SQRT_DBL_EPSILON;

  real sgn = 1.0;
  int status_red;

  if(x < 0.0) {
    x   = -x;
    sgn = -1.0;
  }

  /* Argument reduction to [0, 2pi) */
  status_red = gsl_sf_angle_restrict_pos_e(&x);

  /* Further reduction to [0,pi) */
  if(x > M_PI) {
    /* simulated extra precision: 2PI = p0 + p1 */
    const real p0 = 6.28125;
    const real p1 = 0.19353071795864769253e-02;
    x = (p0 - x) + p1;
    sgn = -sgn;
  }

  if(x == 0.0) {
    result->val = 0.0;
    result->err = 0.0;
  }
  else if(x < x_cut) {
    result->val = x * (1.0 - log(x));
    result->err = x * GSL_DBL_EPSILON;
  }
  else {
    const real t = 2.0*(x*x / (M_PI*M_PI) - 0.5);
    gsl_sf_result result_c;
    cheb_eval_e(&aclaus_cs, t, &result_c);
    result->val = x * (result_c.val - log(x));
    result->err = x * (result_c.err + GSL_DBL_EPSILON);
  }

  result->val *= sgn;

  return status_red;
}

real gsl_sf_clausen(const real x)
{
  EVAL_RESULT(gsl_sf_clausen_e(x, &result));
}


///////////////////////////////////////////////////////////////////////////////
// GSL Dilog Function
///////////////////////////////////////////////////////////////////////////////
static
int
dilog_series_1(const real x, gsl_sf_result * result)
{
  const int kmax = 1000;
  real sum  = x;
  real term = x;
  int k;
  for(k=2; k<kmax; k++) {
    const real rk = (k-1.0)/k;
    term *= x;
    term *= rk*rk;
    sum += term;
    if(fabs(term/sum) < GSL_DBL_EPSILON) break;
  }

  result->val  = sum;
  result->err  = 2.0 * fabs(term);
  result->err += 2.0 * GSL_DBL_EPSILON * fabs(result->val);

  if(k == kmax)
    return GSL_FAILURE;
  else
    return GSL_SUCCESS;
}
static int
series_2(real r, gsl_sf_result * result)
{
  static const int kmax = 100;
  real rk = r;
  real sum = 0.5 * r;
  int k;
  for(k=2; k<10; k++)
  {
    real ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0));
    sum += ds;
  }
  for(; k<kmax; k++)
  {
    real ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0));
    sum += ds;
    if(fabs(ds/sum) < 0.5*GSL_DBL_EPSILON) break;
  }

  result->val = sum;
  result->err = 2.0 * kmax * GSL_DBL_EPSILON * fabs(sum);

  return GSL_SUCCESS;
}
static int
dilog_series_2(real x, gsl_sf_result * result)
{
  const int stat_s3 = series_2(x, result);
  real t;
  if(x > 0.01)
    t = (1.0 - x) * log(1.0-x) / x;
  else
  {
    static const real c3 = 1.0/3.0;
    static const real c4 = 1.0/4.0;
    static const real c5 = 1.0/5.0;
    static const real c6 = 1.0/6.0;
    static const real c7 = 1.0/7.0;
    static const real c8 = 1.0/8.0;
    const real t68 = c6 + x*(c7 + x*c8);
    const real t38 = c3 + x *(c4 + x *(c5 + x * t68));
    t = (x - 1.0) * (1.0 + x*(0.5 + x*t38));
  }
  result->val += 1.0 + t;
  result->err += 2.0 * GSL_DBL_EPSILON * fabs(t);
  return stat_s3;
}

static
int
dilog_xge0(const real x, gsl_sf_result * result)
{
  if(x > 2.0) {
    gsl_sf_result ser;
    const int stat_ser = dilog_series_2(1.0/x, &ser);
    const real log_x = log(x);
    const real t1 = M_PI*M_PI/3.0;
    const real t2 = ser.val;
    const real t3 = 0.5*log_x*log_x;
    result->val  = t1 - t2 - t3;
    result->err  = GSL_DBL_EPSILON * fabs(log_x) + ser.err;
    result->err += GSL_DBL_EPSILON * (fabs(t1) + fabs(t2) + fabs(t3));
    result->err += 2.0 * GSL_DBL_EPSILON * fabs(result->val);
    return stat_ser;
  }
  else if(x > 1.01) {
    gsl_sf_result ser;
    const int stat_ser = dilog_series_2(1.0 - 1.0/x, &ser);
    const real log_x    = log(x);
    const real log_term = log_x * (log(1.0-1.0/x) + 0.5*log_x);
    const real t1 = M_PI*M_PI/6.0;
    const real t2 = ser.val;
    const real t3 = log_term;
    result->val  = t1 + t2 - t3;
    result->err  = GSL_DBL_EPSILON * fabs(log_x) + ser.err;
    result->err += GSL_DBL_EPSILON * (fabs(t1) + fabs(t2) + fabs(t3));
    result->err += 2.0 * GSL_DBL_EPSILON * fabs(result->val);
    return stat_ser;
  }
  else if(x > 1.0) {
    /* series around x = 1.0 */
    const real eps = x - 1.0;
    const real lne = log(eps);
    const real c0 = M_PI*M_PI/6.0;
    const real c1 =   1.0 - lne;
    const real c2 = -(1.0 - 2.0*lne)/4.0;
    const real c3 =  (1.0 - 3.0*lne)/9.0;
    const real c4 = -(1.0 - 4.0*lne)/16.0;
    const real c5 =  (1.0 - 5.0*lne)/25.0;
    const real c6 = -(1.0 - 6.0*lne)/36.0;
    const real c7 =  (1.0 - 7.0*lne)/49.0;
    const real c8 = -(1.0 - 8.0*lne)/64.0;
    result->val = c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
    result->err = 2.0 * GSL_DBL_EPSILON * fabs(result->val);
    return GSL_SUCCESS;
  }
  else if(x == 1.0) {
    result->val = M_PI*M_PI/6.0;
    result->err = 2.0 * GSL_DBL_EPSILON * M_PI*M_PI/6.0;
    return GSL_SUCCESS;
  }
  else if(x > 0.5) {
    gsl_sf_result ser;
    const int stat_ser = dilog_series_2(1.0-x, &ser);
    const real log_x = log(x);
    const real t1 = M_PI*M_PI/6.0;
    const real t2 = ser.val;
    const real t3 = log_x*log(1.0-x);
    result->val  = t1 - t2 - t3;
    result->err  = GSL_DBL_EPSILON * fabs(log_x) + ser.err;
    result->err += GSL_DBL_EPSILON * (fabs(t1) + fabs(t2) + fabs(t3));
    result->err += 2.0 * GSL_DBL_EPSILON * fabs(result->val);
    return stat_ser;
  }
  else if(x > 0.25) {
    return dilog_series_2(x, result);
  }
  else if(x > 0.0) {
    return dilog_series_1(x, result);
  }
  else {
    /* x == 0.0 */
    result->val = 0.0;
    result->err = 0.0;
    return GSL_SUCCESS;
  }
}

/* Evaluate the series representation for Li2(z):
 *
 *   Li2(z) = Sum[ |z|^k / k^2 Exp[i k arg(z)], {k,1,Infinity}]
 *   |z|    = r
 *   arg(z) = theta
 *   
 * Assumes 0 < r < 1.
 * It is used only for small r.
 */
static
int
dilogc_series_1(
  const real r,
  const real x,
  const real y,
  gsl_sf_result * real_result,
  gsl_sf_result * imag_result
  )
{
  const real cos_theta = x/r;
  const real sin_theta = y/r;
  const real alpha = 1.0 - cos_theta;
  const real beta  = sin_theta;
  real ck = cos_theta;
  real sk = sin_theta;
  real rk = r;
  real real_sum = r*ck;
  real imag_sum = r*sk;
  const int kmax = 50 + (int)(22.0/(-log(r))); /* tuned for real-precision */
  int k;
  for(k=2; k<kmax; k++) {
    real dr, di;
    real ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((real)k*k) * ck;
    di = rk/((real)k*k) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabs((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < GSL_DBL_EPSILON*GSL_DBL_EPSILON) break;
  }

  real_result->val = real_sum;
  real_result->err = 2.0 * kmax * GSL_DBL_EPSILON * fabs(real_sum);
  imag_result->val = imag_sum;
  imag_result->err = 2.0 * kmax * GSL_DBL_EPSILON * fabs(imag_sum);

  return GSL_SUCCESS;
}


/* Compute
 *
 *   sum_{k=1}{infty} z^k / (k^2 (k+1))
 *
 * This is a series which appears in the one-step accelerated
 * method, which splits out one elementary function from the
 * full definition of Li_2.
 */
static int
series_2_c(
  real r,
  real x,
  real y,
  gsl_sf_result * sum_re,
  gsl_sf_result * sum_im
  )
{
  const real cos_theta = x/r;
  const real sin_theta = y/r;
  const real alpha = 1.0 - cos_theta;
  const real beta  = sin_theta;
  real ck = cos_theta;
  real sk = sin_theta;
  real rk = r;
  real real_sum = 0.5 * r*ck;
  real imag_sum = 0.5 * r*sk;
  const int kmax = 30 + (int)(18.0/(-log(r))); /* tuned for real-precision */
  int k;
  for(k=2; k<kmax; k++)
  {
    real dr, di;
    const real ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((real)k*k*(k+1.0)) * ck;
    di = rk/((real)k*k*(k+1.0)) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabs((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < GSL_DBL_EPSILON*GSL_DBL_EPSILON) break;
  }

  sum_re->val = real_sum;
  sum_re->err = 2.0 * kmax * GSL_DBL_EPSILON * fabs(real_sum);
  sum_im->val = imag_sum;
  sum_im->err = 2.0 * kmax * GSL_DBL_EPSILON * fabs(imag_sum);

  return GSL_SUCCESS;
}

int
gsl_sf_complex_log_e(const real zr, const real zi, gsl_sf_result * lnr, gsl_sf_result * theta)
{
  /* CHECK_POINTER(lnr) */
  /* CHECK_POINTER(theta) */

  if(zr != 0.0 || zi != 0.0) {
    const real ax = fabs(zr);
    const real ay = fabs(zi);
    const real min = GSL_MIN(ax, ay);
    const real max = GSL_MAX(ax, ay);
    lnr->val = log(max) + 0.5 * log(1.0 + (min/max)*(min/max));
    lnr->err = 2.0 * GSL_DBL_EPSILON * fabs(lnr->val);
    theta->val = atan2(zi, zr);
    theta->err = GSL_DBL_EPSILON * fabs(lnr->val);
    return GSL_SUCCESS;
  }
  else {
    lnr->val = NAN;
    theta->val = NAN;
    return GSL_FAILURE;
    // DOMAIN_ERROR_2(lnr, theta);
  }
}


/* Compute Li_2(z) using the one-step accelerated series.
 *
 * Li_2(z) = 1 + (1-z)ln(1-z)/z + series_2_c(z)
 *
 * z = r exp(i theta)
 * assumes: r < 1
 * assumes: r > epsilon, so that we take no special care with log(1-z)
 */
static
int
dilogc_series_2(
  const real r,
  const real x,
  const real y,
  gsl_sf_result * real_dl,
  gsl_sf_result * imag_dl
  )
{
  if(r == 0.0)
  {
    real_dl->val = 0.0;
    imag_dl->val = 0.0;
    real_dl->err = 0.0;
    imag_dl->err = 0.0;
    return GSL_SUCCESS;
  }
  else
  {
    gsl_sf_result sum_re;
    gsl_sf_result sum_im;
    const int stat_s3 = series_2_c(r, x, y, &sum_re, &sum_im);

    /* t = ln(1-z)/z */
    gsl_sf_result ln_omz_r;
    gsl_sf_result ln_omz_theta;
    const int stat_log = gsl_sf_complex_log_e(1.0-x, -y, &ln_omz_r, &ln_omz_theta);
    const real t_x = ( ln_omz_r.val * x + ln_omz_theta.val * y)/(r*r);
    const real t_y = (-ln_omz_r.val * y + ln_omz_theta.val * x)/(r*r);

    /* r = (1-z) ln(1-z)/z */
    const real r_x = (1.0 - x) * t_x + y * t_y;
    const real r_y = (1.0 - x) * t_y - y * t_x;

    real_dl->val = sum_re.val + r_x + 1.0;
    imag_dl->val = sum_im.val + r_y;
    real_dl->err = sum_re.err + 2.0*GSL_DBL_EPSILON*(fabs(real_dl->val) + fabs(r_x));
    imag_dl->err = sum_im.err + 2.0*GSL_DBL_EPSILON*(fabs(imag_dl->val) + fabs(r_y));
    return GSL_ERROR_SELECT_2(stat_s3, stat_log);
  }
}


/* Evaluate a series for Li_2(z) when |z| is near 1.
 * This is uniformly good away from z=1.
 *
 *   Li_2(z) = Sum[ a^n/n! H_n(theta), {n, 0, Infinity}]
 *
 * where
 *   H_n(theta) = Sum[ e^(i m theta) m^n / m^2, {m, 1, Infinity}]
 *   a = ln(r)
 *
 *  H_0(t) = Gl_2(t) + i Cl_2(t)
 *  H_1(t) = 1/2 ln(2(1-c)) + I atan2(-s, 1-c)
 *  H_2(t) = -1/2 + I/2 s/(1-c)
 *  H_3(t) = -1/2 /(1-c)
 *  H_4(t) = -I/2 s/(1-c)^2
 *  H_5(t) = 1/2 (2 + c)/(1-c)^2
 *  H_6(t) = I/2 s/(1-c)^5 (8(1-c) - s^2 (3 + c))
 */
static
int
dilogc_series_3(
  const real r,
  const real x,
  const real y,
  gsl_sf_result * real_result,
  gsl_sf_result * imag_result
  )
{
  const real theta = atan2(y, x);
  const real cos_theta = x/r;
  const real sin_theta = y/r;
  const real a = log(r);
  const real omc = 1.0 - cos_theta;
  const real omc2 = omc*omc;
  real H_re[7];
  real H_im[7];
  real an, nfact;
  real sum_re, sum_im;
  gsl_sf_result Him0;
  int n;

  H_re[0] = M_PI*M_PI/6.0 + 0.25*(theta*theta - 2.0*M_PI*fabs(theta));
  gsl_sf_clausen_e(theta, &Him0);
  H_im[0] = Him0.val;

  H_re[1] = -0.5*log(2.0*omc);
  H_im[1] = -atan2(-sin_theta, omc);

  H_re[2] = -0.5;
  H_im[2] = 0.5 * sin_theta/omc;

  H_re[3] = -0.5/omc;
  H_im[3] = 0.0;

  H_re[4] = 0.0;
  H_im[4] = -0.5*sin_theta/omc2;

  H_re[5] = 0.5 * (2.0 + cos_theta)/omc2;
  H_im[5] = 0.0;

  H_re[6] = 0.0;
  H_im[6] = 0.5 * sin_theta/(omc2*omc2*omc) * (8.0*omc - sin_theta*sin_theta*(3.0 + cos_theta));

  sum_re = H_re[0];
  sum_im = H_im[0];
  an = 1.0;
  nfact = 1.0;
  for(n=1; n<=6; n++) {
    real t;
    an *= a;
    nfact *= n;
    t = an/nfact;
    sum_re += t * H_re[n];
    sum_im += t * H_im[n];
  }

  real_result->val = sum_re;
  real_result->err = 2.0 * 6.0 * GSL_DBL_EPSILON * fabs(sum_re) + fabs(an/nfact);
  imag_result->val = sum_im;
  imag_result->err = 2.0 * 6.0 * GSL_DBL_EPSILON * fabs(sum_im) + Him0.err + fabs(an/nfact);

  return GSL_SUCCESS;
}


/* Calculate complex dilogarithm Li_2(z) in the fundamental region,
 * which we take to be the intersection of the unit disk with the
 * half-space x < MAGIC_SPLIT_VALUE. It turns out that 0.732 is a
 * nice choice for MAGIC_SPLIT_VALUE since then points mapped out
 * of the x > MAGIC_SPLIT_VALUE region and into another part of the
 * unit disk are bounded in radius by MAGIC_SPLIT_VALUE itself.
 *
 * If |z| < 0.98 we use a direct series summation. Otherwise z is very
 * near the unit circle, and the series_2 expansion is used; see above.
 * Because the fundamental region is bounded away from z = 1, this
 * works well.
 */
static
int
dilogc_fundamental(real r, real x, real y, gsl_sf_result * real_dl, gsl_sf_result * imag_dl)
{
  if(r > 0.98)  
    return dilogc_series_3(r, x, y, real_dl, imag_dl);
  else if(r > 0.25)
    return dilogc_series_2(r, x, y, real_dl, imag_dl);
  else
    return dilogc_series_1(r, x, y, real_dl, imag_dl);
}


/* Compute Li_2(z) for z in the unit disk, |z| < 1. If z is outside
 * the fundamental region, which means that it is too close to z = 1,
 * then it is reflected into the fundamental region using the identity
 *
 *   Li2(z) = -Li2(1-z) + zeta(2) - ln(z) ln(1-z).
 */
static
int
dilogc_unitdisk(real x, real y, gsl_sf_result * real_dl, gsl_sf_result * imag_dl)
{
  static const real MAGIC_SPLIT_VALUE = 0.732;
  static const real zeta2 = M_PI*M_PI/6.0;
  const real r = hypot(x, y);

  if(x > MAGIC_SPLIT_VALUE)
  {
    /* Reflect away from z = 1 if we are too close. The magic value
     * insures that the reflected value of the radius satisfies the
     * related inequality r_tmp < MAGIC_SPLIT_VALUE.
     */
    const real x_tmp = 1.0 - x;
    const real y_tmp =     - y;
    const real r_tmp = hypot(x_tmp, y_tmp);
    /* const real cos_theta_tmp = x_tmp/r_tmp; */
    /* const real sin_theta_tmp = y_tmp/r_tmp; */

    gsl_sf_result result_re_tmp;
    gsl_sf_result result_im_tmp;

    const int stat_dilog = dilogc_fundamental(r_tmp, x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    const real lnz    =  log(r);               /*  log(|z|)   */
    const real lnomz  =  log(r_tmp);           /*  log(|1-z|) */
    const real argz   =  atan2(y, x);          /*  arg(z) assuming principal branch */
    const real argomz =  atan2(y_tmp, x_tmp);  /*  arg(1-z)   */
    real_dl->val  = -result_re_tmp.val + zeta2 - lnz*lnomz + argz*argomz;
    real_dl->err  =  result_re_tmp.err;
    real_dl->err +=  2.0 * GSL_DBL_EPSILON * (zeta2 + fabs(lnz*lnomz) + fabs(argz*argomz));
    imag_dl->val  = -result_im_tmp.val - argz*lnomz - argomz*lnz;
    imag_dl->err  =  result_im_tmp.err;
    imag_dl->err +=  2.0 * GSL_DBL_EPSILON * (fabs(argz*lnomz) + fabs(argomz*lnz));

    return stat_dilog;
  }
  else
  {
    return dilogc_fundamental(r, x, y, real_dl, imag_dl);
  }
}



/*-*-*-*-*-*-*-*-*-*-*-* Functions with Error Codes *-*-*-*-*-*-*-*-*-*-*-*/


int
gsl_sf_dilog_e(const real x, gsl_sf_result * result)
{
  if(x >= 0.0) {
    return dilog_xge0(x, result);
  }
  else {
    gsl_sf_result d1, d2;
    int stat_d1 = dilog_xge0( -x, &d1);
    int stat_d2 = dilog_xge0(x*x, &d2);
    result->val  = -d1.val + 0.5 * d2.val;
    result->err  =  d1.err + 0.5 * d2.err;
    result->err += 2.0 * GSL_DBL_EPSILON * fabs(result->val);
    return GSL_ERROR_SELECT_2(stat_d1, stat_d2);
  }
}


int
gsl_sf_complex_dilog_xy_e(
  const real x,
  const real y,
  gsl_sf_result * real_dl,
  gsl_sf_result * imag_dl
  )
{
  const real zeta2 = M_PI*M_PI/6.0;
  const real r2 = x*x + y*y;

  if(y == 0.0)
  {
    if(x >= 1.0)
    {
      imag_dl->val = -M_PI * log(x);
      imag_dl->err = 2.0 * GSL_DBL_EPSILON * fabs(imag_dl->val);
    }
    else
    {
      imag_dl->val = 0.0;
      imag_dl->err = 0.0;
    }
    return gsl_sf_dilog_e(x, real_dl);
  }
  else if(fabs(r2 - 1.0) < GSL_DBL_EPSILON)
  {
    /* Lewin A.2.4.1 and A.2.4.2 */

    const real theta = atan2(y, x);
    const real term1 = theta*theta/4.0;
    const real term2 = M_PI*fabs(theta)/2.0;
    real_dl->val = zeta2 + term1 - term2;
    real_dl->err = 2.0 * GSL_DBL_EPSILON * (zeta2 + term1 + term2);
    return gsl_sf_clausen_e(theta, imag_dl);
  }
  else if(r2 < 1.0)
  {
    return dilogc_unitdisk(x, y, real_dl, imag_dl);
  }
  else
  {
    /* Reduce argument to unit disk. */
    const real r = sqrt(r2);
    const real x_tmp =  x/r2;
    const real y_tmp = -y/r2;
    /* const real r_tmp = 1.0/r; */
    gsl_sf_result result_re_tmp, result_im_tmp;

    const int stat_dilog =
      dilogc_unitdisk(x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    /* Unwind the inversion.
     *
     *  Li_2(z) + Li_2(1/z) = -zeta(2) - 1/2 ln(-z)^2
     */
    const real theta = atan2(y, x);
    const real theta_abs = fabs(theta);
    const real theta_sgn = ( theta < 0.0 ? -1.0 : 1.0 );
    const real ln_minusz_re = log(r);
    const real ln_minusz_im = theta_sgn * (theta_abs - M_PI);
    const real lmz2_re = ln_minusz_re*ln_minusz_re - ln_minusz_im*ln_minusz_im;
    const real lmz2_im = 2.0*ln_minusz_re*ln_minusz_im;
    real_dl->val = -result_re_tmp.val - 0.5 * lmz2_re - zeta2;
    real_dl->err =  result_re_tmp.err + 2.0*GSL_DBL_EPSILON*(0.5 * fabs(lmz2_re) + zeta2);
    imag_dl->val = -result_im_tmp.val - 0.5 * lmz2_im;
    imag_dl->err =  result_im_tmp.err + 2.0*GSL_DBL_EPSILON*fabs(lmz2_im);
    return stat_dilog;
  }
}


int
gsl_sf_complex_dilog_e(
  const real r,
  const real theta,
  gsl_sf_result * real_dl,
  gsl_sf_result * imag_dl
  )
{
  const real cos_theta = cos(theta);
  const real sin_theta = sin(theta);
  const real x = r * cos_theta;
  const real y = r * sin_theta;
  return gsl_sf_complex_dilog_xy_e(x, y, real_dl, imag_dl);
}


int
gsl_sf_complex_spence_xy_e(
  const real x,
  const real y,
  gsl_sf_result * real_sp,
  gsl_sf_result * imag_sp
  )
{
  const real oms_x = 1.0 - x;
  const real oms_y =     - y;
  return gsl_sf_complex_dilog_xy_e(oms_x, oms_y, real_sp, imag_sp);
}

///////////////////////////////////////////////////////////////////////////////
// GSL Gamma
///////////////////////////////////////////////////////////////////////////////

static real lanczos_7_c[9] = {
  0.99999999999980993227684700473478,
  676.520368121885098567009190444019,
 -1259.13921672240287047156078755283,
  771.3234287776530788486528258894,
 -176.61502916214059906584551354,
  12.507343278686904814458936853,
 -0.13857109526572011689554707,
  9.984369578019570859563e-6,
  1.50563273514931155834e-7
};

#define LogRootTwoPi_  0.9189385332046727418

static
int
lngamma_lanczos(real x, gsl_sf_result * result)
{
  int k;
  real Ag;
  real term1, term2;

  x -= 1.0; /* Lanczos writes z! instead of Gamma(z) */

  Ag = lanczos_7_c[0];
  for(k=1; k<=8; k++) { Ag += lanczos_7_c[k]/(x+k); }

  /* (x+0.5)*log(x+7.5) - (x+7.5) + LogRootTwoPi_ + log(Ag(x)) */
  term1 = (x+0.5)*log((x+7.5)/M_E);
  term2 = LogRootTwoPi_ + log(Ag);
  result->val  = term1 + (term2 - 7.0);
  result->err  = 2.0 * GSL_DBL_EPSILON * (fabs(term1) + fabs(term2) + 7.0);
  result->err += GSL_DBL_EPSILON * fabs(result->val);

  return GSL_SUCCESS;
}

inline
static
int
lngamma_1_pade(const real eps, gsl_sf_result * result)
{
  /* Use (2,2) Pade for Log[Gamma[1+eps]]/eps
   * plus a correction series.
   */
  const real n1 = -1.0017419282349508699871138440;
  const real n2 =  1.7364839209922879823280541733;
  const real d1 =  1.2433006018858751556055436011;
  const real d2 =  5.0456274100274010152489597514;
  const real num = (eps + n1) * (eps + n2);
  const real den = (eps + d1) * (eps + d2);
  const real pade = 2.0816265188662692474880210318 * num / den;
  const real c0 =  0.004785324257581753;
  const real c1 = -0.01192457083645441;
  const real c2 =  0.01931961413960498;
  const real c3 = -0.02594027398725020;
  const real c4 =  0.03141928755021455;
  const real eps5 = eps*eps*eps*eps*eps;
  const real corr = eps5 * (c0 + eps*(c1 + eps*(c2 + eps*(c3 + c4*eps))));
  result->val = eps * (pade + corr);
  result->err = 2.0 * GSL_DBL_EPSILON * fabs(result->val);
  return GSL_SUCCESS;
}

inline
static
int
lngamma_2_pade(const real eps, gsl_sf_result * result)
{
  /* Use (2,2) Pade for Log[Gamma[2+eps]]/eps
   * plus a correction series.
   */
  const real n1 = 1.000895834786669227164446568;
  const real n2 = 4.209376735287755081642901277;
  const real d1 = 2.618851904903217274682578255;
  const real d2 = 10.85766559900983515322922936;
  const real num = (eps + n1) * (eps + n2);
  const real den = (eps + d1) * (eps + d2);
  const real pade = 2.85337998765781918463568869 * num/den;
  const real c0 =  0.0001139406357036744;
  const real c1 = -0.0001365435269792533;
  const real c2 =  0.0001067287169183665;
  const real c3 = -0.0000693271800931282;
  const real c4 =  0.0000407220927867950;
  const real eps5 = eps*eps*eps*eps*eps;
  const real corr = eps5 * (c0 + eps*(c1 + eps*(c2 + eps*(c3 + c4*eps))));
  result->val = eps * (pade + corr);
  result->err = 2.0 * GSL_DBL_EPSILON * fabs(result->val);
  return GSL_SUCCESS;
}

static
int
lngamma_sgn_0(real eps, gsl_sf_result * lng, real * sgn)
{
  /* calculate series for g(eps) = Gamma(eps) eps - 1/(1+eps) - eps/2 */
  const real c1  = -0.07721566490153286061;
  const real c2  = -0.01094400467202744461;
  const real c3  =  0.09252092391911371098;
  const real c4  = -0.01827191316559981266;
  const real c5  =  0.01800493109685479790;
  const real c6  = -0.00685088537872380685;
  const real c7  =  0.00399823955756846603;
  const real c8  = -0.00189430621687107802;
  const real c9  =  0.00097473237804513221;
  const real c10 = -0.00048434392722255893;
  const real g6  = c6+eps*(c7+eps*(c8 + eps*(c9 + eps*c10)));
  const real g   = eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*g6)))));

  /* calculate Gamma(eps) eps, a positive quantity */
  const real gee = g + 1.0/(1.0+eps) + 0.5*eps;

  lng->val = log(gee/fabs(eps));
  lng->err = 4.0 * GSL_DBL_EPSILON * fabs(lng->val);
  *sgn = GSL_SIGN(eps);

  return GSL_SUCCESS;
}


int gsl_sf_lngamma_e(real x, gsl_sf_result * result)
{
  /* CHECK_POINTER(result) */

  if(fabs(x - 1.0) < 0.01) {
    /* Note that we must amplify the errors
     * from the Pade evaluations because of
     * the way we must pass the argument, i.e.
     * writing (1-x) is a loss of precision
     * when x is near 1.
     */
    int stat = lngamma_1_pade(x - 1.0, result);
    result->err *= 1.0/(GSL_DBL_EPSILON + fabs(x - 1.0));
    return stat;
  }
  else if(fabs(x - 2.0) < 0.01) {
    int stat = lngamma_2_pade(x - 2.0, result);
    result->err *= 1.0/(GSL_DBL_EPSILON + fabs(x - 2.0));
    return stat;
  }
  else if(x >= 0.5) {
    return lngamma_lanczos(x, result);
  }
  else if(x == 0.0) {
    result->val = NAN;
    return GSL_FAILURE;
    // DOMAIN_ERROR(result);
  }
  else if(fabs(x) < 0.02) {
    real sgn;
    return lngamma_sgn_0(x, result, &sgn);
  }
  else if(x > -0.5/(GSL_DBL_EPSILON*M_PI)) {
    /* Try to extract a fractional
     * part from x.
     */
    real z  = 1.0 - x;
    real s  = sin(M_PI*z);
    real as = fabs(s);
    if(s == 0.0) {
      result->val = NAN;
      return GSL_FAILURE;
    //   DOMAIN_ERROR(result);
    }
    else if(as < M_PI*0.015) {
      /* x is near a negative integer, -N */
      if(x < INT_MIN + 2.0) {
        result->val = 0.0;
        result->err = 0.0;
        return GSL_FAILURE;
      }
      else {
        int N = -(int)(x - 0.5);
        real eps = x + N;
        real sgn;
        result->val = NAN;
        return GSL_FAILURE;
        // return lngamma_sgn_sing(N, eps, result, &sgn);
      }
    }
    else {
      gsl_sf_result lg_z;
      lngamma_lanczos(z, &lg_z);
      result->val = M_LNPI - (log(as) + lg_z.val);
      result->err = 2.0 * GSL_DBL_EPSILON * fabs(result->val) + lg_z.err;
      return GSL_SUCCESS;
    }
  }
  else {
    /* |x| was too large to extract any fractional part */
    result->val = 0.0;
    result->err = 0.0;
    return GSL_FAILURE;
    // GSL_ERROR ("error", GSL_EROUND);
  }
}
}

real naive_trans_gsl_lambert_W0(real x) {
    const real one_over_E = 1.0/M_E;
    const real q = x + one_over_E;

    if (x == 0.0) {
        return 0.0;
    }
    else if (q < 0.0) {
        return -1.0;
    }
    else if (q == 0.0) {
        return -1.0;
    }
    else if (q < 1.0e-3) {
        const real r = sqrt(q);
        real val = series_eval(r);
        return val;
    }
    else {
        static const unsigned int MAX_ITERS = 10;
        real w;

        if (x < 1.0) {
            const real p = sqrt(2.0 * M_E * q);
            w = -1.0 + p*(1.0 + p*(-1.0/3.0 + p*11.0/72.0));
        }
        else {
            w = log(x);
            if (x > 3.0) w -= log(w);
        }

        return halley_iteration(x, w, MAX_ITERS);
    }
}

real naive_trans_gsl_dilog(const real x)
{
  EVAL_RESULT(gsl_sf_dilog_e(x, &result));
}


real naive_trans_gsl_lngamma(real x) {
  EVAL_RESULT(gsl_sf_lngamma_e(x, &result));
}