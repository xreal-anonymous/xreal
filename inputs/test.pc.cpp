#include <cstdint>

#include "../src/SourceLayer/SourceLayerPass.h"
#include "../src/SourceLayer/Transformer.h"
#include "../src/xreal/xreal.h"

namespace {

struct myPass : sourcelayer::Pass {
    static char ID;
    myPass() : sourcelayer::Pass(ID) {}

    bool run() override {
        printf("In Test Pass.\n");
        Transformer dbl("inputs/test-constant.cpp", "dummy", false);
        dbl.setTargetFPType(FPType::FP_Double);
        dbl.addTransformValue("var1", 1.0/xreal::e);
        dbl.addTransformValue("var2", 1 - 1.0/xreal::e);
        dbl.runTransformer();

        return false;
    }
};
// This ID can be whatever value,
// Pass register use it address (rather than value) to identify the pass.
char myPass::ID = 0;

// Register - Producer
sourcelayer::RegisterPass<myPass> x("testpass");
}  // namespace