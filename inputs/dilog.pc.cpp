#include <cstdint>

#include "../src/SourceLayer/SourceLayerPass.h"
#include "../src/SourceLayer/Transformer.h"
#include "../src/xreal/xreal.h"

namespace {

struct myPass : sourcelayer::Pass {
    static char ID;
    myPass() : sourcelayer::Pass(ID) { }

    bool run() override {
        printf("In Dilog Pass.\n");
        printf("Specifies the Precision Control Logic Here..\n");

        float flt_thres = 1e-7;
        Transformer floatTarget("inputs/dilog.xreal", "intermediate/dilog.float.cpp");
        floatTarget.setTargetFPType(FPType::FP_Float);
        floatTarget.addTransformValue("threshold", flt_thres);
        floatTarget.runTransformer();

        double dbl_thres = 1e-16;
        Transformer doubleTarget("inputs/dilog.xreal", "intermediate/dilog.double.cpp");
        doubleTarget.setTargetFPType(FPType::FP_Double);
        doubleTarget.addTransformValue("threshold", dbl_thres);
        doubleTarget.runTransformer();

        long double ld_thres = 5e-20;
        Transformer ldTarget("inputs/dilog.xreal", "intermediate/dilog.longdouble.cpp");
        ldTarget.setTargetFPType(FPType::FP_LongDouble);
        ldTarget.addTransformValue("threshold", ld_thres);
        ldTarget.runTransformer();

        xreal orcl_thres = "1e-500";
        Transformer oracleTarget("inputs/dilog.xreal", "intermediate/dilog.oracle.cpp");
        oracleTarget.setTargetFPType(FPType::FP_Real);
        oracleTarget.addTransformValue("threshold", orcl_thres);
        oracleTarget.runTransformer();

        return false;
    }
};
// This ID can be whatever value,
// Pass register use it address (rather than value) to identify the pass.
char myPass::ID = 0; 

// Register - Producer
sourcelayer::RegisterPass<myPass> x("dilogpass");
}