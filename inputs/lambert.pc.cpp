#include <cstdint>

#include "../src/SourceLayer/SourceLayerPass.h"
#include "../src/SourceLayer/Transformer.h"
#include "../src/xreal/xreal.h"

namespace {

std::vector<xreal> gen_lambert_coef(int terms);

struct myPass : sourcelayer::Pass {
    static char ID;
    myPass() : sourcelayer::Pass(ID) { }

    bool run() override {
        printf("In Lambert Pass.\n");
        printf("Specifies the Precision Control Logic Here..\n");

        Transformer floatTarget("inputs/lambert.xreal", "intermediate/lambert.float.cpp");
        floatTarget.setTargetFPType(FPType::FP_Float);
        floatTarget.addTransformValue("terms", 10);
        floatTarget.addTransformValue("max_iters", 8);
        floatTarget.addTransformValue("comp_term", -1.0/xreal::e - (float)(-1.0/xreal::e));
        floatTarget.addTransformValue("tol_factor", 1.1920928955078125e-07);
        floatTarget.addTransformVector("coefs", gen_lambert_coef(10));
        floatTarget.runTransformer();

        Transformer doubleTarget("inputs/lambert.xreal", "intermediate/lambert.double.cpp");
        doubleTarget.setTargetFPType(FPType::FP_Double);
        doubleTarget.addTransformValue("terms", 12);
        doubleTarget.addTransformValue("max_iters", 10);
        doubleTarget.addTransformValue("comp_term", -1.0/xreal::e - (double)(-1.0/xreal::e));
        doubleTarget.addTransformValue("tol_factor", 2.2204460492503131e-16);
        doubleTarget.addTransformVector("coefs", gen_lambert_coef(12));
        // doubleTarget.dump();
        doubleTarget.runTransformer();

        Transformer ldTarget("inputs/lambert.xreal", "intermediate/lambert.longdouble.cpp");
        ldTarget.setTargetFPType(FPType::FP_LongDouble);
        ldTarget.addTransformValue("terms", 15);
        ldTarget.addTransformValue("max_iters", 10);
        ldTarget.addTransformValue("comp_term", -1.0/xreal::e - (long double)(-1.0/xreal::e));
        ldTarget.addTransformValue("tol_factor", 5.421010862427564e-20);
        ldTarget.addTransformVector("coefs", gen_lambert_coef(15));
        // doubleTarget.dump();
        ldTarget.runTransformer();

        Transformer oracleTarget("inputs/lambert.xreal", "intermediate/lambert.oracle.cpp");
        oracleTarget.setTargetFPType(FPType::FP_Real);
        oracleTarget.addTransformValue("terms", 18);
        oracleTarget.addTransformValue("max_iters", 32);
        oracleTarget.addTransformValue("tol_factor", 1e-32);
        oracleTarget.addTransformVector("coefs", gen_lambert_coef(18));
        // oracleTarget.dump();
        oracleTarget.runTransformer();

        return false;
    }
};
// This ID can be whatever value,
// Pass register use it address (rather than value) to identify the pass.
char myPass::ID = 0; 

// Register - Producer
sourcelayer::RegisterPass<myPass> x("lambertpass");


// void runOnXRealSrc() {
// 	TransTarget dbl = new TransTarget();
// 	int dbl_terms = 12;
// 	dbl.addPrecCtrlVar("terms", dbl_terms);
// 	dbl.addPrecCtrlVar("max_iters", 10);
// 	dbl.addPrecCtrlVar("tol_factor", 2.2204460492503131e-16);
// 	dbl.addPrecCtrlVarList("coefs", gen_lambert_coef(dbl_terms));


// 	TransTarget ocrl = new TransTarget();
// 	int ocrl_terms = 20;
// 	ocrl.addPrecCtrlVar("terms", ocrl_terms);
// 	ocrl.addPrecCtrlVar("max_iters", 32);
// 	ocrl.addPrecCtrlVar("tol_factor", 1e-32);
// 	ocrl.addPrecCtrlVarList("coefs", gen_lambert_coef(ocrl_terms));
// }

std::vector<xreal> gen_lambert_coef(int terms) {
    xreal p = sqrt(xreal::e * 2.0);
    static std::vector<xreal> v = {
        -1,
        p,
        -1*pow(p,2) / 3,
        11*pow(p,3) / 72,
        -43*pow(p,4) / 540,
        769*pow(p,5) / 17280,
        -221*pow(p,6) / 8505,
        680863*pow(p,7) / 43545600,
        -1963*pow(p,8) / 204120,
        xreal("226287557")*pow(p,9) / xreal("37623398400"),
        xreal("-5776369")*pow(p,10) / xreal("1515591000"),
        xreal("169709463197")*pow(p,11) / xreal("69528040243200"),
        xreal("-1118511313")*pow(p,12) / xreal("709296588000"),
        xreal("667874164916771")*pow(p,13) / xreal("650782456676352000"),
        xreal("-500525573")*pow(p,14) / xreal("744761417400"),
        xreal("103663334225097487")*pow(p,15) / xreal("234281684403486720000"),
        xreal("-466901817532379")*pow(p,16) / xreal("1595278956070800000"),
        xreal("21235294185086305043")*pow(p,17) / xreal("109242202556140093440000"),
        xreal("-106040742894306601")*pow(p,18) / xreal("818378104464320400000"),
        xreal("1150497127780071399782389")*pow(p,19) / xreal("13277465363600276402995200000"),
        xreal("-2853534237182741069")*pow(p,20) / xreal("49102686267859224000000")
    };

    if (terms < 0) terms = 0;
    if (terms > v.size()) terms = v.size();
    std::vector<xreal> vslice(v.begin(), v.begin()+terms);
    return vslice;
}
}