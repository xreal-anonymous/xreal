#include <cstdint>

#include "../src/SourceLayer/SourceLayerPass.h"
#include "../src/SourceLayer/Transformer.h"
#include "../src/xreal/xreal.h"


#include <unsupported/Eigen/MPRealSupport>
#include <Eigen/Dense>
#include <Eigen/LU>

namespace {

std::vector<xreal> gen_lanczos_coef(int terms, int vargamma);
std::vector<xreal> gen_near_zero_coef(int terms);

struct myPass : sourcelayer::Pass {
    static char ID;
    myPass() : sourcelayer::Pass(ID) { }

    bool run() override {
        printf("In Gamma Pass.\n");

        xreal logRootTwoPi_ = log(sqrt(2 * xreal::pi));

        int vargamma_float = 5;
        Transformer floatTarget("inputs/gamma.xreal", "intermediate/gamma.float.cpp");
        // Set type
        floatTarget.setTargetFPType(FPType::FP_Float);
        // Lanczos
        floatTarget.addTransformValue("vargamma", vargamma_float);
        floatTarget.addTransformValue("lanczos_coef_terms", vargamma_float + 2);
        floatTarget.addTransformValue("LogRootTwoPi_", logRootTwoPi_);
        floatTarget.addTransformVector("lanczos_coef", gen_lanczos_coef(vargamma_float + 2, vargamma_float));
        // Near zero 
        floatTarget.addTransformValue("near_zero_thres", 0.01);
        floatTarget.addTransformValue("near_zero_terms", 8);
        floatTarget.addTransformVector("near_zero_coefs", gen_near_zero_coef(8));
        // Transform
        floatTarget.runTransformer();

        int vargamma_double = 7;
        Transformer doubleTarget("inputs/gamma.xreal", "intermediate/gamma.double.cpp");
        // Set type
        doubleTarget.setTargetFPType(FPType::FP_Double);
        // Lanczos
        doubleTarget.addTransformValue("vargamma", vargamma_double);
        doubleTarget.addTransformValue("lanczos_coef_terms", vargamma_double + 2);
        doubleTarget.addTransformValue("LogRootTwoPi_", logRootTwoPi_);
        doubleTarget.addTransformVector("lanczos_coef", gen_lanczos_coef(vargamma_double + 2, vargamma_double));
        // Near zero
        doubleTarget.addTransformValue("near_zero_thres", 0.01);
        doubleTarget.addTransformValue("near_zero_terms", 10);
        doubleTarget.addTransformVector("near_zero_coefs", gen_near_zero_coef(10));
        // doubleTarget.dump();
        doubleTarget.runTransformer();

        int vargamma_ld = 9;
        Transformer ldTarget("inputs/gamma.xreal", "intermediate/gamma.longdouble.cpp");
        // Set type
        ldTarget.setTargetFPType(FPType::FP_LongDouble);
        // Lanczos
        ldTarget.addTransformValue("vargamma", vargamma_ld);
        ldTarget.addTransformValue("lanczos_coef_terms", vargamma_ld + 2);
        ldTarget.addTransformValue("LogRootTwoPi_", logRootTwoPi_);
        ldTarget.addTransformVector("lanczos_coef", gen_lanczos_coef(vargamma_ld + 2, vargamma_ld));
        // Near zero
        ldTarget.addTransformValue("near_zero_thres", 0.01);
        ldTarget.addTransformValue("near_zero_terms", 10);
        ldTarget.addTransformVector("near_zero_coefs", gen_near_zero_coef(10));
        // ldTarget.dump();
        ldTarget.runTransformer();

        int vargamma_oracle = 37;
        Transformer oracleTarget("inputs/gamma.xreal", "intermediate/gamma.oracle.cpp");
        // Set type
        oracleTarget.setTargetFPType(FPType::FP_Real);
        // Lanczos
        oracleTarget.addTransformValue("vargamma", vargamma_oracle);
        oracleTarget.addTransformValue("lanczos_coef_terms", vargamma_oracle + 2);
        oracleTarget.addTransformValue("LogRootTwoPi_", logRootTwoPi_);
        oracleTarget.addTransformVector("lanczos_coef", gen_lanczos_coef(vargamma_oracle + 2, vargamma_oracle));
        // Near zero
        oracleTarget.addTransformValue("near_zero_thres", 1e-20);
        oracleTarget.addTransformValue("near_zero_terms", 10);
        oracleTarget.addTransformVector("near_zero_coefs", gen_near_zero_coef(10));
        // oracleTarget.dump();
        oracleTarget.runTransformer();

        return false;
    }
};
// This ID can be whatever value,
// Pass register use it address (rather than value) to identify the pass.
char myPass::ID = 0; 

// Register - Producer
sourcelayer::RegisterPass<myPass> x("gammapass");

}

namespace {
using namespace std;
using namespace Eigen;
using MatrixXmp = Matrix<mpfr::mpreal, Dynamic, Dynamic>;
using VectorXmp = Matrix<mpfr::mpreal, Dynamic, 1>;

xreal fact(int n) {
    if (n <= 1)
        return 1;
    else
        return n * fact(n-1);
}

xreal semifact(int n) {
    if (n <= 0)
        return 1;
    else
        return n * semifact(n-2);    
}

xreal binomial(int x, int y) {
    if (x < y || x < 0 || y < 0)
        return 0;
    return fact(x) / fact(y) / fact(x-y);
}

xreal Fg(int a, int g) {
    xreal ahalf = (xreal)a+0.5;
    xreal y = sqrt(2/xreal::pi)
                * semifact(2*a-1)
                * exp(ahalf+g)
                / pow((xreal)2, a)
                / pow(ahalf+g, ahalf);
    return y;
}

VectorXmp gen_vector_f(int terms, int vargamma) {
    VectorXmp vecf(terms);
    for (int i = 0; i < terms; i++) {
        vecf(i) = Fg(i, vargamma);
    }
    return vecf;
}

MatrixXmp gen_matrix_D(int terms) {
    MatrixXmp matD(terms, terms);
    // assign value to 2d-vector
    matD(0, 0) = 1;
    for (int i = 0; i < terms; i++) {
        xreal val;
        if (i == 0) {
            val = 1;
        }
        else {
            int j = i-1;
            val = -fact(2*j+2) / (2*fact(j)*fact(j+1));
        }
        matD(i, i) = val;
    }
    return matD;
}

MatrixXmp gen_matrix_B(int terms) {
    MatrixXmp matB(terms, terms);
    for (int j = 0; j < terms; j++) {
        matB(0, j) = 1;
    }
    for (int i = 1; i < terms; i++) {
        for (int j = i; j < terms; j++) {
            int p = 2*i-1;
            int q = p+j-i;
            matB(i, j) = binomial(q, p);
            if ((j-i)%2 == 1) {
                matB(i, j) = -matB(i, j);
            }
        }
    }
    return matB;
}

MatrixXmp gen_matrix_C(int terms) {
    MatrixXmp matC(terms, terms);
    matC(0, 0) = 0.5;
    for (int i = 1; i < terms; i++) {
        for (int j = 0; j <= i; j++) {
            int sign;
            if ((i-j)%2 == 1)
                sign = -1;
            else
                sign = 1;
            matC(i, j) = sign
                * pow((xreal)2, 2*j-1)
                * binomial(i+j, 2*j)
                * (2*i) 
                / (i+j);
        }
    }
    return matC;
}

vector<xreal> gen_lanczos_coef(int terms, int vargamma) {
    MatrixXmp D = gen_matrix_D(terms);
    MatrixXmp B = gen_matrix_B(terms);
    MatrixXmp C = gen_matrix_C(terms);
    VectorXmp f = gen_vector_f(terms, vargamma);

    MatrixXmp res = D*B*C*f;
    vector<xreal> coef = vector<xreal>(res.data(), res.data() + res.rows()*res.cols());
    return coef;
}

xreal fact_zeta(int n) {
    int fastnum = 12;
    static xreal fast_ret[12] = {};
    static bool fast_flag[12] = {};
    if (0 <= n && n < 12 && fast_flag[n] == true) {
        return fast_ret[n];
    }

    int sign = ((n%2 == 0) ? -1:1);
    xreal val = sign * fact(n) * mpfr::zeta(n+1);
    if (0 <= n && n < 12 && fast_flag[n] == false) {
        fast_flag[n] = true;
        fast_ret[n] = val;
    }
    return val;
}

vector<xreal> gen_near_zero_coef(int terms) {
    /* Mathematica: 
    var1 = Series[Gamma[x]*x-1/(1+x), {x, 0, 10}]
    Print[var1]
    Print[N[var1]] */
    xreal pi = xreal::pi;
    xreal euler = xreal::euler;

    static std::vector<xreal> v = {
        xreal("0.5") - euler,
        (-12 + 6*pow(euler,2) + pow(pi,2))/12,
        (12 - 2*pow(euler,3) - euler*pow(pi,2) + 2*fact_zeta(2))/12,
        -1 + (pow(euler,4) + pow(euler,2)*pow(pi,2) + (3*pow(pi,4))/20 - 4*euler*fact_zeta(2))/24,
        1 + (-pow(euler,5) - (5*pow(euler,3)*pow(pi,2))/3 - (3*euler*pow(pi,4))/4 + 10*pow(euler,2)*fact_zeta(2) + (5*pow(pi,2)*fact_zeta(2))/3 + fact_zeta(4))/120,
        -1 + (pow(euler,6) + (5*pow(euler,4)*pow(pi,2))/2 + (9*pow(euler,2)*pow(pi,4))/4 + (61*pow(pi,6))/168 - 20*pow(euler,3)*fact_zeta(2) - 10*euler*pow(pi,2)*fact_zeta(2) + 10*pow(fact_zeta(2),2) - 6*euler*fact_zeta(4))/720,
        1 + (-pow(euler,7) - (7*pow(euler,5)*pow(pi,2))/2 - (21*pow(euler,3)*pow(pi,4))/4 - (61*euler*pow(pi,6))/24 + 35*pow(euler,4)*fact_zeta(2) + 35*pow(euler,2)*pow(pi,2)*fact_zeta(2) + (21*pow(pi,4)*fact_zeta(2))/4 - 70*euler*pow(fact_zeta(2),2) + 21*pow(euler,2)*fact_zeta(4) + (7*pow(pi,2)*fact_zeta(4))/2 + fact_zeta(6))/5040,
        -1 + (pow(euler,8) + (14*pow(euler,6)*pow(pi,2))/3 + (21*pow(euler,4)*pow(pi,4))/2 + (61*pow(euler,2)*pow(pi,6))/6 + (1261*pow(pi,8))/720 - 56*pow(euler,5)*fact_zeta(2) - (280*pow(euler,3)*pow(pi,2)*fact_zeta(2))/3 - 42*euler*pow(pi,4)*fact_zeta(2) + 280*pow(euler,2)*pow(fact_zeta(2),2) + (140*pow(pi,2)*pow(fact_zeta(2),2))/3 - 56*pow(euler,3)*fact_zeta(4) - 28*euler*pow(pi,2)*fact_zeta(4) + 56*fact_zeta(2)*fact_zeta(4) - 8*euler*fact_zeta(6))/40320,
        1 + (-pow(euler,9) - 6*pow(euler,7)*pow(pi,2) - (189*pow(euler,5)*pow(pi,4))/10 - (61*pow(euler,3)*pow(pi,6))/2 - (1261*euler*pow(pi,8))/80 + 84*pow(euler,6)*fact_zeta(2) + 210*pow(euler,4)*pow(pi,2)*fact_zeta(2) + 189*pow(euler,2)*pow(pi,4)*fact_zeta(2) + (61*pow(pi,6)*fact_zeta(2))/2 - 840*pow(euler,3)*pow(fact_zeta(2),2) - 420*euler*pow(pi,2)*pow(fact_zeta(2),2) + 280*pow(fact_zeta(2),3) + 126*pow(euler,4)*fact_zeta(4) + 126*pow(euler,2)*pow(pi,2)*fact_zeta(4) + (189*pow(pi,4)*fact_zeta(4))/10 - 504*euler*fact_zeta(2)*fact_zeta(4) + 36*pow(euler,2)*fact_zeta(6) + 6*pow(pi,2)*fact_zeta(6) + fact_zeta(8))/362880,
        -1 + (pow(euler,10) + (15*pow(euler,8)*pow(pi,2))/2 + (63*pow(euler,6)*pow(pi,4))/2 + (305*pow(euler,4)*pow(pi,6))/4 + (1261*pow(euler,2)*pow(pi,8))/16 + (4977*pow(pi,10))/352 - 120*pow(euler,7)*fact_zeta(2) - 420*pow(euler,5)*pow(pi,2)*fact_zeta(2) - 630*pow(euler,3)*pow(pi,4)*fact_zeta(2) - 305*euler*pow(pi,6)*fact_zeta(2) + 2100*pow(euler,4)*pow(fact_zeta(2),2) + 2100*pow(euler,2)*pow(pi,2)*pow(fact_zeta(2),2) + 315*pow(pi,4)*pow(fact_zeta(2),2) - 2800*euler*pow(fact_zeta(2),3) - 252*pow(euler,5)*fact_zeta(4) - 420*pow(euler,3)*pow(pi,2)*fact_zeta(4) - 189*euler*pow(pi,4)*fact_zeta(4) + 2520*pow(euler,2)*fact_zeta(2)*fact_zeta(4) + 420*pow(pi,2)*fact_zeta(2)*fact_zeta(4) + 126*pow(fact_zeta(4),2) - 120*pow(euler,3)*fact_zeta(6) - 60*euler*pow(pi,2)*fact_zeta(6) + 120*fact_zeta(2)*fact_zeta(6) - 10*euler*fact_zeta(8))/3628800
    };

    if (terms < 0) terms = 0;
    if (terms > v.size()) terms = v.size();
    std::vector<xreal> vslice(v.begin(), v.begin()+terms);
    return vslice;
}

}
