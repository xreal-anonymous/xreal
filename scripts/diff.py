import io
def diff_files(file_origin, file_cur, keyword):
    print('\n======= Diff:', file_origin, file_cur,'=======')
    with io.open(file_origin) as f:
        data = f.readlines()
    origin_stmt_set = set()
    for line in data:
        line = line.strip()
        if line.startswith('//'):
            continue
        origin_stmt_set.add(line)
    
    with io.open(file_cur) as f:
        data = f.readlines()
    count = 0
    for line in data:
        line = line.replace(keyword, 'xreal')
        line = line.strip()
        if line.startswith('//'):
            continue
        if line not in origin_stmt_set:
            count += 1
            print(count, ":", line)

# diff_files("inputs/gamma.xreal", "intermediate/gamma.float.cpp", "float")
# diff_files("inputs/gamma.xreal", "intermediate/gamma.double.cpp", "double")
# diff_files("inputs/gamma.xreal", "intermediate/gamma.longdouble.cpp", "long double")

# diff_files("inputs/gamma.xreal", "final-manually/gamma.float.cpp", "float")
# diff_files("inputs/gamma.xreal", "final-manually/gamma.double.cpp", "double")
# diff_files("inputs/gamma.xreal", "final-manually/gamma.longdouble.cpp", "long double")

# diff_files("inputs/lambert.xreal", "intermediate/lambert.float.cpp", "float")
# diff_files("inputs/lambert.xreal", "intermediate/lambert.double.cpp", "double")
# diff_files("inputs/lambert.xreal", "intermediate/lambert.longdouble.cpp", "long double")

# diff_files("inputs/lambert.xreal", "final-manually/lambert.float.cpp", "float")
# diff_files("inputs/lambert.xreal", "final-manually/lambert.double.cpp", "double")
# diff_files("inputs/lambert.xreal", "final-manually/lambert.longdouble.cpp", "long double")

diff_files("inputs/dilog.xreal", "intermediate/dilog.float.cpp", "float")
diff_files("inputs/dilog.xreal", "intermediate/dilog.double.cpp", "double")
diff_files("inputs/dilog.xreal", "intermediate/dilog.longdouble.cpp", "long double")

diff_files("inputs/dilog.xreal", "final-manually/dilog.float.cpp", "float")
diff_files("inputs/dilog.xreal", "final-manually/dilog.double.cpp", "double")
diff_files("inputs/dilog.xreal", "final-manually/dilog.longdouble.cpp", "long double")


# diff_files("inputs/dilog.xreal", "intermediate/dilog.double.cpp", "double")