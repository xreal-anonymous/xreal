import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import os

titles = ['name', 'target', 'maxerr']

# float ulp: 1.192e-07
# double ulp: 2.220e-16
# long double ulp: 1.084e-19

data = [
    # ['ulp', 'float', 1.192e-07],
    # ['ulp', 'double', 2.220e-16],
    # ['ulp', 'long double', 1.084e-19],
    # Loggamma data
    ['loggamma', 'float', 7.39e-06],
    ['loggamma', 'double', 3.42e-14],
    ['loggamma', 'long double', 1.23e-17],
    # Dilog data
    ['dilog', 'float', 2.78e-06],
    ['dilog', 'double', 1.53e-14],
    ['dilog', 'long double', 8.07e-18],
    # Lambert data
    ['lambertW', 'float', 7.12e-07],
    ['lambertW', 'double', 1.57e-15],
    ['lambertW', 'long double', 7.01e-19],
]

plot_offset = 20

df = pd.DataFrame(data=data, columns=titles)
df['logmaxerr'] = np.log10(df['maxerr'])
df['logmaxerr_offset'] = df['logmaxerr'] + plot_offset

ulp = {
    'float': 1.192e-07,
    'double': 2.220e-16,
    'long double': 1.084e-19,
}

style_dash = {
    'ulp': [5,3],
    'loggamma': '',
    'lambertw': '',
    'dilog': '',
}
# https://github.com/mwaskom/seaborn/issues/1624
markers = {
    'ulp': ',',
    'loggamma': 'v',
    'lambertw': 'H',
    'dilog': '^',
}


print(df)


def draw_line_chart():
    plt.figure(figsize=(4.5,3.5))

    sns.set_style("darkgrid")
    sns.color_palette("Paired")

    ax = sns.lineplot(data=df,
                x='target', y='logmaxerr', hue='name',
                style="name", markers=markers, dashes=style_dash, markersize=10,
                palette='mako'
                )
    # ax.set_title('Maximum Error in Targets')
    ax.set_ylim(-20, -4)
    ax.set_yticks([-18, -16, -14, -12, -10, -8, -6])
    ax.set_yticklabels(['1e-18', '1e-16', '1e-14', '1e-12', '1e-10', '1e-8', '1e-6'])

    ax.set_ylabel("Maximum Error")
    ax.set_xlabel("")

    plt.legend(title='Function')

    plt.tight_layout()

    if not os.path.exists("figures"):
        os.makedirs("figures")
    plt.savefig("figures/err-on-targets.pdf")

    plt.show()

def draw_bar_chart():
    plt.figure(figsize=(6,3))

    sns.set_style("darkgrid")
    sns.color_palette("Paired")

    ax = sns.barplot(data=df,
                x='name', y='logmaxerr_offset' , hue='target',
                # style="name", markers=markers, dashes=style_dash, markersize=10,
                palette='deep',
                # bottom=-5
                )
    # Add a dash line of ulp in each target
    for ix, a in enumerate(ax.patches):
        x_start = a.get_x()
        width = a.get_width()
        if ix // 3 == 0:
            base = np.log10(ulp['float'])
        if ix // 3 == 1:
            base = np.log10(ulp['double'])
        if ix // 3 == 2:
            base = np.log10(ulp['long double'])
        base_offset = base + plot_offset
        ax.plot([x_start, x_start + width], [base_offset, base_offset], '--', color='whitesmoke', linewidth=1)

    # ax.set_title('Maximum Error in Targets')
    yticks = [-20, -18, -16, -14, -12, -10, -8, -6]
    yticks_plus_20 = list(map(lambda x: x + plot_offset, yticks))
    ylim = [-20, -4]
    ylim_plus_20 = list(map(lambda x: x + plot_offset, ylim))

    ax.set_ylim(ylim_plus_20)
    ax.set_yticks(yticks_plus_20)
    ax.set_yticklabels(['1e-20', '1e-18', '1e-16', '1e-14', '1e-12', '1e-10', '1e-8', '1e-6'])

    ax.set_ylabel("Maximum Error", fontsize=12)
    ax.set_xlabel("")

    ax.tick_params(axis='x', labelsize=13)
    ax.tick_params(axis='y', labelsize=11)

    plt.legend(title='Targets',
                loc='upper center',
                bbox_to_anchor=(0.5, 1.3),
                ncol=3,
                title_fontsize=13,
                fontsize=12,
                )

    plt.tight_layout()

    if not os.path.exists("figures"):
        os.makedirs("figures")
    plt.savefig("figures/err-on-targets.pdf", bbox_inches='tight')

    plt.show()

def main():
    # draw_line_chart()
    draw_bar_chart()

if __name__ == "__main__":
    main()