#include "AtomuUtil.h"
#include "opcode.h"

#include <cstring>
#include <random>
#include <cfloat>

namespace AtomuUtil {

double i64ToDouble(uint64_t i) {
    // *(double*)(&i) is an undefined behavior
    // See: https://en.cppreference.com/w/cpp/language/reinterpret_cast
    // Use memcpy instead.
    // In c++20, bit_cast<double> could also be okay.
    double d;
    static_assert(sizeof d == sizeof i, "[double] and [uint64_t] should have same bits.");
    std::memcpy(&d, &i, sizeof i);
    return d;
}
uint64_t doubleToI64(double d) {
    uint64_t i;
    static_assert(sizeof i == sizeof d, "[double] and [uint64_t] should have same bits.");
    std::memcpy(&i, &d, sizeof d);
    return i;
}

float i32ToFloat(uint32_t i) {
    float f;
    static_assert(sizeof f == sizeof i, "[float] and [uint32_t] should have same bits.");
    std::memcpy(&f, &i, sizeof i);
    return f;
}
uint32_t floatToI32(float f) {
    uint32_t i;
    static_assert(sizeof i == sizeof f, "[float] and [uint32_t] should have same bits.");
    std::memcpy(&i, &f, sizeof f);
    return i;
}

uint64_t getDoubleSign(double d) {
    uint64_t i = doubleToI64(d);
    return (i & DBL_SIGNMASK) >> 63;
}

uint64_t getDoubleExpo(double d) {
    uint64_t i = doubleToI64(d);
    return (i & DBL_EXPOMASK) >> 52;
}

uint64_t getDoubleFrac(double d) {
    uint64_t i = doubleToI64(d);
    return (i & DBL_FRACMASK);
}

double buildDouble(uint64_t sign, uint64_t expo, uint64_t frac) {
    uint64_t bits;
    bits  = (sign << 63) & DBL_SIGNMASK;
    bits |= (expo << 52) & DBL_EXPOMASK;
    bits |= (frac & DBL_FRACMASK);
    return i64ToDouble(bits);
}

double doubleULP(double d) {
    uint64_t bits = doubleToI64(d);
    // make it positive.
    bits = bits & (DBL_EXPOMASK | DBL_FRACMASK);
    d = i64ToDouble(bits);
    double d_plus = i64ToDouble(bits+1);
    return d_plus - d;
}
float floatULP(float f) {
    uint32_t bits = floatToI32(f);
    bits = bits & (FLT_EXPOMASK | FLT_FRACMASK);
    f = i32ToFloat(bits);
    float f_plus = i32ToFloat(bits+1);
    return f_plus - f;
}

double randUni01() {
    static std::mt19937_64 mt_generator_64(0xbeefbeef);
    static std::uniform_real_distribution<double> uni01(0.0, 1.0);
    return uni01(mt_generator_64);
}

uint64_t rand64() {
    // important to make the generator 'static' here.
    static std::mt19937_64 mt_generator_64(0xdeadbeef);
    return mt_generator_64();
}
uint32_t rand32() {
    uint32_t lower = static_cast<uint32_t>(rand64());
    return lower;
}

double randDouble() {
    return i64ToDouble(rand64());
}
float randFloat() {
    return i32ToFloat(rand32());
}

double rawCondition(int32_t opcode, double lhs, double rhs) {
    double cond1, cond2;
    double dzdist;
    switch (opcode)
    {
    case static_cast<int>(BinOpType::FP_Add):
        dzdist = fabs(lhs+rhs);
        cond1 = fabs(lhs) / dzdist;
        cond2 = fabs(rhs) / dzdist;
        return cond1 + cond2 - dzdist;
    case static_cast<int>(BinOpType::FP_Sub):
        dzdist = fabs(lhs-rhs);
        cond1 = fabs(lhs) / dzdist;
        cond2 = fabs(rhs) / dzdist;
        return cond1 + cond2 - dzdist;
    case static_cast<int>(CallOpType::SIN):
        // cond1 = fabs(lhs / tan(lhs));
        // x \to n*pi, n \in Z.
        cond1 = 1 / fabs(remainder(lhs, AtomuUtil::PI));
        return cond1;
    case static_cast<int>(CallOpType::COS):
        // cond1 = fabs(lhs * tan(lhs));
        // x \to n*pi + pi/2, n \in Z.
        cond1 = 1 / fabs(remainder((remainder(lhs, AtomuUtil::PI)-AtomuUtil::PI_2),AtomuUtil::PI));
        return cond1;
    case static_cast<int>(CallOpType::TAN):
        // cond1 = fabs(lhs / (sin(lhs) * cos(lhs)));
        // x \to n*pi/2, n \in Z.
        cond1 = 1 / fabs(remainder(lhs, AtomuUtil::PI_2));
        return cond1;
    case static_cast<int>(CallOpType::ASIN):
        cond1 = fabs(lhs / (sqrt(1-lhs*lhs) * asin(lhs)));
        return cond1;
    case static_cast<int>(CallOpType::ACOS):
        cond1 = fabs(lhs / (sqrt(1-lhs*lhs) * acos(lhs)));
        return cond1;
    case static_cast<int>(CallOpType::SINH):
        cond1 = fabs(lhs / tanh(lhs));
        return cond1;
    case static_cast<int>(CallOpType::COSH):
        cond1 = fabs(lhs * tanh(lhs));
        return cond1;
    case static_cast<int>(CallOpType::LOG):
        dzdist = fabs(lhs - 1);
        cond1 = fabs(1 / log(lhs));
        return cond1 - dzdist;
    case static_cast<int>(CallOpType::LOG10):
        dzdist = fabs(lhs - 1);
        cond1 = fabs(1 / log(lhs));
        return cond1 - dzdist;
    case static_cast<int>(CallOpType::POW):
        cond1 = fabs(rhs);
        cond2 = fabs(rhs * log(lhs));
        return cond1 + cond2;
    default:
        return -DBL_MAX;
    }
    return -DBL_MAX;
}

double revisedCondition(int32_t opcode, double lhs, double rhs) {
    double cond1, cond2, dzdist;
    switch(opcode) {
        case static_cast<int>(BinOpType::FP_Add):
            dzdist = fabs(lhs+rhs);
            cond1 = fabs(lhs) / dzdist;
            cond2 = fabs(rhs) / dzdist;
            return cond1 + cond2;
        case static_cast<int>(BinOpType::FP_Sub):
            dzdist = fabs(lhs-rhs);
            cond1 = fabs(lhs) / dzdist;
            cond2 = fabs(rhs) / dzdist;
            return cond1 + cond2;
        case static_cast<int>(CallOpType::SIN):
            cond1 = fabs(lhs / tan(lhs));
            return cond1;
        case static_cast<int>(CallOpType::COS):
            cond1 = fabs(lhs * tan(lhs));
            return cond1;
        case static_cast<int>(CallOpType::TAN):
            cond1 = fabs(lhs / (sin(lhs) * cos(lhs)));
            return cond1;
        case static_cast<int>(CallOpType::ASIN):
            cond1 = fabs(lhs / (sqrt(1-lhs*lhs) * asin(lhs)));
            return cond1;
        case static_cast<int>(CallOpType::ACOS):
            cond1 = fabs(lhs / (sqrt(1-lhs*lhs) * acos(lhs)));
            return cond1;
        case static_cast<int>(CallOpType::SINH):
            cond1 = fabs(lhs / tanh(lhs));
            return cond1;
        case static_cast<int>(CallOpType::COSH):
            cond1 = fabs(lhs * tanh(lhs));
            return cond1;
        case static_cast<int>(CallOpType::LOG):
            dzdist = fabs(lhs - 1);
            cond1 = fabs(1 / log(lhs));
            return cond1;
        case static_cast<int>(CallOpType::LOG10):
            dzdist = fabs(lhs - 1);
            cond1 = fabs(1 / log(lhs));
            return cond1;
        case static_cast<int>(CallOpType::POW):
            cond1 = fabs(rhs);
            cond2 = fabs(rhs * log(lhs));
            return cond1 + cond2;
        default:
            return 1;
    }
    return 1;
}

} // namespace AtomuUtil
