#pragma once

#include <cstdint>

namespace AtomuUtil {
    const uint64_t DBL_SIGNMASK = 0x8000000000000000uLL;
    const uint64_t DBL_EXPOMASK = 0x7FF0000000000000uLL;
    const uint64_t DBL_FRACMASK = 0x000FFFFFFFFFFFFFuLL;

    const uint64_t DBL_POSINF = 0x7FF0000000000000uLL;
    const uint64_t DBL_NEGINF = 0xFFF0000000000000uLL;
    const uint64_t DBL_NAN    = 0x7FFFFFFFFFFFFFFFuLL;

    const uint32_t FLT_SIGNMASK = 0x80000000;
    const uint32_t FLT_EXPOMASK = 0x7F800000;
    const uint32_t FLT_FRACMASK = 0x007FFFFF;

    const uint32_t FLT_POSINF = 0x7F800000;
    const uint32_t FLT_NEGINF = 0xFF800000;
    const uint32_t FLT_NAN    = 0x7FFFFFFF;

    const double PI   = 3.14159265358979;
    const double PI_2 = 1.57079632679490;

    double   i64ToDouble(uint64_t i);
    uint64_t doubleToI64(double d);
    float    i32ToFloat(uint32_t i);
    uint32_t floatToI32(float f);

    uint64_t getDoubleSign(double d);
    uint64_t getDoubleExpo(double d);
    uint64_t getDoubleFrac(double d);

    double buildDouble(uint64_t sign, uint64_t expo, uint64_t frac);

    double doubleULP(double d);

    double randUni01();
    uint64_t rand64();
    uint32_t rand32();
    double randDouble();
    float  randFloat();

    double rawCondition(int32_t opcode, double lhs, double rhs);
    double revisedCondition(int32_t opcode, double lhs, double rhs);
} // namespace AtomuUtil