#pragma once

#include <vector>
#include <functional>
#include "../xreal/xreal.h"

template<class FPTy>
using FPFuncTy = std::function<FPTy(FPTy)>;

template<class FPTy, class FPTyOrcl=double>
class ErrorDetector {
protected:
    FPFuncTy<FPTy> originFuncPtr;
    FPFuncTy<FPTyOrcl> oracleFuncPtr;
public:
    ErrorDetector(FPFuncTy<FPTy> _originFuncPtr, FPFuncTy<FPTyOrcl> _oracleFuncPtr)
        : originFuncPtr(_originFuncPtr), oracleFuncPtr(_oracleFuncPtr) { }
    virtual std::vector<FPTy> detectErrorInputs() = 0;
};