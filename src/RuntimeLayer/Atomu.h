#pragma once

#include <map>
#include <set>
#include <utility>
#include <type_traits>
#include <random>
#include <algorithm>
#include <chrono>

#include "ErrorDetector.h"
#include "Communicator.h"
#include "AtomuUtil.h"

namespace Atomu {

// template<class FPTy>
// using CondInput = std::pair<double, FPTy>;
template<class FPTy>
struct CondInput {
    double condition;
    FPTy   input;
    CondInput() { }
    CondInput(double c, FPTy i) : condition(c), input(i) { }
    bool operator < (const CondInput<FPTy> & c) const {
        return condition < c.condition;
    }
    bool operator > (const CondInput<FPTy> & c) const {
        return condition > c.condition;
    }
};

template<class FPTy>
bool CondInputGreater(const CondInput<FPTy> & a, const CondInput<FPTy> & b) {
    return a.condition > b.condition;
}

// On each instruction, we maintain the inputs->conditions on this instruction.
template<class FPTy>
class InstConditions {
private:
    InstInfo instInfo;
    uint32_t maxRecordSize = 100;
public:
    std::set<CondInput<FPTy>> conditionWithInput;

public:
    InstConditions() : instInfo(0, 0) { }
    InstConditions(InstInfo info) : instInfo(info) { }
    InstConditions(uint64_t instID, int32_t instType) : instInfo(instID, instType) { }

    void addInputCondition(FPTy input, double cond) {
        using std::isfinite;
        
        // Ignore condition == nan / inf.
        if (isfinite(cond) == false)
            return;

        // Insert to our set.
        conditionWithInput.insert(CondInput<FPTy>(cond, input));

        // Maintain the size of our set.
        // Remove the smallest condition: set.begin() from the set.
        if (conditionWithInput.size() > maxRecordSize) {
            conditionWithInput.erase(conditionWithInput.begin());
        }
    }

    InstInfo getInstInfo() {
        return instInfo;
    }
    uint32_t getMaxRecordSize() {
        return maxRecordSize;
    }

    uint32_t getRecordSize() {
        return conditionWithInput.size();
    }

    CondInput<FPTy> getMaxConditionInput() {
        return *(conditionWithInput.rbegin());
    }

    std::set<CondInput<FPTy>> getCondInput() {
        return conditionWithInput;
    }

    std::vector<CondInput<FPTy>> getCondInputVector() {
        return std::vector<CondInput<FPTy>>(conditionWithInput.rbegin(), conditionWithInput.rend());
    }

    void setCondInput(const std::vector<CondInput<FPTy>>& condInputVec) {
        conditionWithInput.clear();
        conditionWithInput.insert(condInputVec.begin(), condInputVec.end());
    }

    // void addCondInput(const std::vector<CondInput<FPTy>>& condInputVec) {
    //     conditionWithInput.insert(condInputVec.begin(), condInputVec.end());
    //     while (conditionWithInput.size() > maxRecordSize) {
    //         conditionWithInput.erase(conditionWithInput.begin());
    //     }
    // }
};

template<class FPTy>
class FunctionRuntimeInfo {
private:
    FPFuncTy<FPTy> originFuncPtr;
    Communicator &comm;

    // Status: 0 for okay, 1 for nan.
    int32_t status;

public:
    FunctionRuntimeInfo(FPFuncTy<FPTy> _originFuncPtr)
        : originFuncPtr(_originFuncPtr), comm( Communicator::getInstance() )
    {
        comm.initCommunicator();
    }

    FPTy call(FPTy x) {
        // Clean Runtime information.
        comm.clear();
        // Get result.
        FPTy out = originFuncPtr(x);
        // Set status.
        if (std::isnan(out)) {
            status = 1;
        }
        else {
            status = 0;
        }
        return out;
    }

    bool isSuccess() {
        return (status == 0);
    }

    std::vector<InstRuntimeInfo> getRuntimeInfo() {
        return comm.getInstRuntimeInfoList();
    }
};

template<class FPTy>
class AtomuDetector : public ErrorDetector<FPTy, xreal> {
private:
    FunctionRuntimeInfo<FPTy> function;
    std::map<uint64_t, InstConditions<FPTy>> instMap;

    std::vector<FPTy> ErroneousInputs;

    int32_t unstableInstCount = 0;

    // Params:
    // For atomu_1_RandomSearch
    double initExpRange = 20;
    double initCenterRate = 0.15;
    uint32_t randomIteration = 100000;
    // For atomu_2_EvolutionSearch
    double evoGeometricP = 0.25;
    double evoNormalFactorStart = 1e-2;
    double evoNormalFactorEnd   = 1e-13;
    uint32_t evoIterations = 100;
    uint32_t evoPerIterationSize = 100;

    // Random generator
    std::mt19937 mtGenerator;

public:
    AtomuDetector(FPFuncTy<FPTy> _originFuncPtr, FPFuncTy<xreal> _oracleFuncPtr) :
        ErrorDetector<FPTy, xreal>(_originFuncPtr, _oracleFuncPtr),
        function(_originFuncPtr),
        mtGenerator(0xdeadbeef)
    {
    }

    std::vector<FPTy> detectErrorInputs() {
        execute();
        return ErroneousInputs;
    }

private:
    void execute() {
        atomu_init();
        atomu_1_RandomSearch();
        atomu_2_EvolutionSearch();
        atomu_3_InputsValidation();
    }

    void atomu_init() {
        if (!instMap.empty()) {
            std::map<uint64_t, InstConditions<FPTy>> emptyMap;
            std::swap(emptyMap, instMap);
        }
    }

    // Generate random inputs for initialization.
    FPTy initRandom() {
        // Currently we do:
        // for double and long double, we use a double version center-crowded init
        // for float, we use a pure random init.
        // Perhaps we can optimize this strategy later.
        if (std::is_same<FPTy, double>::value == true
            || std::is_same<FPTy, long double>::value == true) {
            double x = AtomuUtil::randDouble();
            double p01 = AtomuUtil::randUni01();
            if (p01 < initCenterRate) {
                uint64_t xsign = AtomuUtil::getDoubleSign(x);
                uint64_t xfrac = AtomuUtil::getDoubleFrac(x);
                uint64_t xexpo = 1023 + (uint64_t)(initExpRange*2*AtomuUtil::randUni01() - initExpRange);
                x = AtomuUtil::buildDouble(xsign, xexpo, xfrac);
            }
            return x;
        }
        if (std::is_same<FPTy, float>::value == true) {
            float x = AtomuUtil::randFloat();
            return x;
        }
        // Shoule never reach here.
        return AtomuUtil::randDouble();
    }

    // void maintainRuntimeInfo(FPTy input, bool isForSpecificInst, uint64_t specificInstID) {
    //     // Only log when the execution is success.
    //     if (function.isSuccess() == false)
    //         return;
    //     std::vector<InstRuntimeInfo> runtimeInfo = function.getRuntimeInfo();
    //     for (const auto &instRuntimeInfo : runtimeInfo) {
    //         if (instMap.count(instRuntimeInfo.instID) == 0) {
    //             instMap[instRuntimeInfo.instID] = InstConditions<FPTy>(instRuntimeInfo);
    //         }
    //         double cond = AtomuUtil::revisedCondition(instRuntimeInfo.instType, instRuntimeInfo.op1, instRuntimeInfo.op2);
    //         if (!isfinite(cond))
    //             continue;
    //         InstConditions<FPTy> &curInstCondition = instMap[instRuntimeInfo.instID];
    //         curInstCondition.addInputCondition(input, cond);
    //     }
    //     return;
    // }

    void atomu_1_RandomSearch() {
        using namespace std::chrono;
        auto start = steady_clock::now();

        FPTy x, y;
        for (int i = 1; i <= randomIteration; i++) {
            x = initRandom();
            y = function.call(x);
            
            // maintain runtime info to instMap
            if (function.isSuccess() == false)
                continue;
            std::vector<InstRuntimeInfo> runtimeInfo = function.getRuntimeInfo();
            for (const auto &instRuntimeInfo : runtimeInfo) {
                if (instMap.count(instRuntimeInfo.instID) == 0) {
                    instMap[instRuntimeInfo.instID] = InstConditions<FPTy>(instRuntimeInfo.instID, instRuntimeInfo.instType);
                }
                double cond = AtomuUtil::revisedCondition(instRuntimeInfo.instType, instRuntimeInfo.op1, instRuntimeInfo.op2);
                if (!std::isfinite(cond))
                    continue;
                // Insert the input/condition to instMap.
                InstConditions<FPTy> &curInstCondition = instMap[instRuntimeInfo.instID];
                curInstCondition.addInputCondition(x, cond);
            }
        }

        auto end = steady_clock::now();
        std::cout << "[Atomu] 1. Random Search Done."
                <<" Cost: " << duration_cast<milliseconds>(end - start).count() << " ms"
                << "\n";
    }

    void atomu_2_EvolutionSearch() {
        using namespace std;
        using namespace std::chrono;
        auto start = steady_clock::now();

        std::geometric_distribution<int> geoDist(evoGeometricP);
        std::normal_distribution<double> normDist(0, 1);

        FPTy x, y;

        for (auto &kv : instMap) {
            // Searching on current Instruction ID
            uint64_t instID = kv.first;
            InstConditions<FPTy> &curInstCondition = kv.second;

            // For Debug.
            // printInstConditions(curInstCondition);
            // getchar();

            // Decide whether this instruction is worth searching.
            // 1. Has we trigger this inst in so far?
            if (curInstCondition.getRecordSize() == 0)
                continue;
            // 2. If the largest condition is still small, ignore it.
            if (curInstCondition.getMaxConditionInput().condition < 1e1)
                continue;

            unstableInstCount++;
            // Turn CondInput from set to vector, largest condition first.
            std::vector<CondInput<FPTy>> CondInputList = curInstCondition.getCondInputVector();

            // Start the evolution search.
            for (int i = 0; i < evoIterations; i++) {
                double evoNormalFactor = exp(log(evoNormalFactorStart) +
                    ((double)i / evoIterations) * log(evoNormalFactorEnd / evoNormalFactorStart));
                for (int j = 0; j < evoPerIterationSize; j++) {
                    // evo.1 select.
                    int selectedIndex;
                    while ((selectedIndex = geoDist(mtGenerator)) >= CondInputList.size());
                    FPTy curInput = CondInputList[selectedIndex].input;

                    // evo.2 mutate.
                    FPTy mutVal = evoNormalFactor * fabs(curInput) * normDist(mtGenerator);
                    FPTy newInput = curInput + mutVal;
                    if (!isfinite(newInput)) {
                        continue;
                    }

                    // evo.3 run new input.
                    y = function.call(newInput);
                    if (function.isSuccess() == false)
                        continue;
                    std::vector<InstRuntimeInfo> runtimeInfo = function.getRuntimeInfo();
                    for (const auto &instRuntimeInfo : runtimeInfo) {
                        // we only care about current inst ID
                        if (instRuntimeInfo.instID != instID)
                            continue;
                        double newCond = AtomuUtil::revisedCondition(instRuntimeInfo.instType, instRuntimeInfo.op1, instRuntimeInfo.op2);
                        if (!isfinite(newCond))
                            continue;
                        CondInputList.push_back(CondInput<FPTy>(newCond, newInput));
                    }
                }
                // After each iteration, only reserve top 100 inputs.
                std::sort(CondInputList.begin(), CondInputList.end(), CondInputGreater<FPTy>);
                CondInputList.resize(curInstCondition.getMaxRecordSize());
            }
            // After all iterations, store CondInputList to curInstCondition (instMap).
            curInstCondition.setCondInput(CondInputList);
        }

        auto end = steady_clock::now();
        std::cout << "[Atomu] 2. Evolution Search Done."
                <<" Cost: " << duration_cast<milliseconds>(end - start).count() << " ms"
                << "\n";
    }

    double calcErrorWithOracle(FPTy input) {
        FPTy output;
        xreal oracle;
        double err;
        output = function.call(input);
        if (function.isSuccess() == false)
            return 0;
        oracle = this->oracleFuncPtr(input);
        if (oracle == 0)
            return 0;
        return (double)fabs((oracle-output) / oracle);
    }

    void atomu_3_InputsValidation() {
        using namespace std::chrono;
        auto start = steady_clock::now();

        std::vector<std::pair<double, FPTy>> ErrInputs;
        for (auto & kv : instMap) {
            uint64_t instID = kv.first;
            InstConditions<FPTy> &curInstCondition = kv.second;
            // std::vector<CondInput<FPTy>> CondInputVec = curInstCondition.getCondInputVector();

            // For Debug.
            // printInstConditions(curInstCondition);
            // getchar();

            if (curInstCondition.getRecordSize() == 0)
                continue;
            if (curInstCondition.getMaxConditionInput().condition < 1e1)
                continue;

            // FPTy firstInput;
            // double firstError;
            // firstInput  = curInstCondition.getMaxConditionInput().input;
            // firstError  = calcErrorWithOracle(firstInput);
            // ErrInputs.push_back({firstError, firstInput});

            FPTy maxErrInputOnInst;
            double maxErrOnInst = 0;
            bool found = false;
            auto CondInputVec = curInstCondition.getCondInputVector();
            for (const auto & condinput : CondInputVec) {
                // Check inputs on each inst.
                // Get the input with largest error. Push to ErrInputs
                FPTy input = condinput.input;
                FPTy fpOut = function.call(input);
                xreal oracleOut = this->oracleFuncPtr(input);
                if (oracleOut == 0)
                    continue;
                double relativeErr = (double)(fabs(oracleOut - fpOut) / fabs(oracleOut));
                if (!std::isfinite(relativeErr))
                    continue;
                if (relativeErr > maxErrOnInst) {
                    maxErrOnInst = relativeErr;
                    maxErrInputOnInst = input;
                    found = true;
                }
            }
            if (found) {
                ErrInputs.push_back({maxErrOnInst, maxErrInputOnInst});
                // std::cout << "instID: " << curInstCondition.getInstInfo().dump() << "\n";
                // std::cout << "  maxErr: " << maxErrOnInst << "\n";
                // std::cout << "  input:  " << maxErrInputOnInst << "\n";
            }
        }
        std::sort(ErrInputs.begin(), ErrInputs.end());
        std::reverse(ErrInputs.begin(), ErrInputs.end()); // Largest error first.
        // put them into ErroneousInputs
        for (const auto & ei : ErrInputs) {
            double LDBL_THRES = 1e-13;
            double DBL_THRES  = 1e-10;
            double FLT_THRES  = 1e-3;
            double DEFAULT_THRES  = 1e-10;

            if (std::is_same<FPTy, float>::value) {
                if (ei.first > FLT_THRES) {
                    ErroneousInputs.push_back(ei.second);
                }
            }
            else if (std::is_same<FPTy, double>::value) {
                if (ei.first > DBL_THRES) {
                    ErroneousInputs.push_back(ei.second);
                }
            }
            else if (std::is_same<FPTy, long double>::value) {
                if (ei.first > LDBL_THRES) {
                    ErroneousInputs.push_back(ei.second);
                }
            }
            else {
                if (ei.first > DEFAULT_THRES) {
                    ErroneousInputs.push_back(ei.second);
                }
            }

        }

        auto end = steady_clock::now();
        std::cout << "[Atomu] 3. Oracle Validation Done."
                <<" Cost: " << duration_cast<milliseconds>(end - start).count() << " ms"
                << "\n";

        return;
    }

    void printInstConditions(InstConditions<FPTy> &instCond) {
        using namespace std;
        int showTopNum = 5;
        cout << instCond.getInstInfo().dump() << "\n";
        cout << "  Records: " << instCond.getRecordSize() << "\n";
        // cout << "    Max Cond Input: " << scientific << instCond.getMaxConditionInput().input
        //     << "\tCond:" << instCond.getMaxConditionInput().condition;

        auto iter = instCond.conditionWithInput.rbegin();
        for (int i = 0; i < showTopNum; i++) {
            if (iter == instCond.conditionWithInput.rend())
                break;
            cout << "    Top Cond Input: " << scientific << iter->input
                << "\tCond: " << iter->condition << "\n";
            ++iter;
        }
        cout << "--------------\n";
    }
};



} // End of namespace Atomu