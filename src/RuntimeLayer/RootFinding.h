#pragma once

#include "../xreal/xreal.h"
#include <functional>

xreal findRoot(const std::function<xreal(xreal)>& func, const xreal& lbound, const xreal& rbound);
