#include "Polynomial.h"
#include "PolyFit.h"

#include "RootFinding.h"
#include "AcesoX.h"

#include <iostream>

void testPoly() {
    ChebySeries chebpoly;
    chebpoly.lBound = 1;
    chebpoly.rBound = 4;
    chebpoly.coefs = {1.542,0.49296,-0.040488,0.0066968,-0.0013836,0.00030211};
    chebpoly.degree = 5;

    std::cout << "Result of sqrt(2): " << chebpoly.eval(2) << "\n";
}

void testPolyFit() {
    std::vector<xreal> xs = {1,2,3};
    std::vector<xreal> ys = {3,5,8};
    std::vector<xreal> ws = {1,1,1};

    int degree = 3;
    int numd = 1, denomd = 1;

    // std::vector<xreal> coefs = PolyFit::PowerFitWeighted(xs, ys, ws, degree);
    // PowerSeries poly;
    // poly.coefs = coefs;
    // poly.degree = degree;
    // poly.show();

    std::vector<xreal> xs_norm;
    xreal l = 1, r = 3;
    xs_norm.resize(xs.size());
    for (int i = 0; i < xs.size(); i++) {
        xs_norm[i] = reduce(xs[i], l, r);
    }
    std::vector<xreal> coefs = PolyFit::ChebyFitWeighted(xs_norm, ys, ws, degree);
    ChebySeries poly;
    poly.coefs = coefs;
    poly.degree = degree;
    poly.lBound = l;
    poly.rBound = r;
    poly.show();

    // std::vector<xreal> coefs = PolyFit::RationalFit(xs, ys, numd, denomd);
    // RationalSeries poly;
    // poly.coefs = coefs;
    // poly.numDegree = numd;
    // poly.denomDegree = denomd;
    // poly.show();

    std::cout << "\nEval: \n";
    std::cout << "1.0: " << poly.eval(1.0) << "\n";
    std::cout << "1.5: " << poly.eval(1.5) << "\n";
    std::cout << "2.0: " << poly.eval(2.0) << "\n";
    std::cout << "2.5: " << poly.eval(2.5) << "\n";
    std::cout << "3.0: " << poly.eval(3.0) << "\n";
}

xreal legendre_P2(xreal x) {
    return 0.5*(3.0*x*x - 1.0);
}

xreal foo(xreal x) {
    return 1/x - 1/tan(x);
}

void testRootFinding() {
    PowerSeries powerSeries = AcesoX::repairWithPowerSeries(foo, 0, 1e-6, 4);
    powerSeries.show();
}

void testAcesoX() {
    xreal rt = findRoot(legendre_P2, 0.5, 0.6);
    std::cout << "Root of legendre_P2: " << rt.toString("%.40Re") << "\n";

    PowerSeries powerSeries = AcesoX::repairWithPowerSeries(legendre_P2, rt, 1e-6, 4);
    powerSeries.show();

    double x1 = 0.5773502691896257;
    double x2 = 0.5773502691896283;
    std::cout << "power eval: " << powerSeries.eval(x1).toString("%.16Re") << "\n";
    std::cout << "power eval: " << powerSeries.eval(x2).toString("%.16Re") << "\n";
}

int main() {
    testPoly();
    testPolyFit();
    testRootFinding();
    testAcesoX();
}