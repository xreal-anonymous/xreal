#include "AcesoX.h"
#include "PolyFit.h"

namespace {

int IntervalChebyshevNodes = 1000;

std::vector<xreal> ChebyNodes(const xreal& l, const xreal& r, int num) {
    std::vector<xreal> nativeNodes(num);
    std::vector<xreal> actualNodes(num);
    xreal fpk;
    xreal fpn = num;
    for (int k = 1; k <= num; k++) {
        fpk = k;
        nativeNodes[k-1] = cos((2*fpk-1)/(2*fpn)*xreal::pi);
    }

    xreal center = (l + r) / 2.0;
    xreal radius = (r - l) / 2.0;
    for (int i = 0; i < num; i++) {
        actualNodes[i] = center + nativeNodes[i]*radius;
    }

    return actualNodes;
}

bool guessCenterIsZero(const std::function<xreal(xreal)>& func, const xreal& center) {
    xreal res = func(center);
    if (res == 0)
        return true;
    // if (isfinite(res) && res != 0)
    //     return false;

    // res is nan or inf.
    // test a small value, and guess
    if (center == 0 && fabs(func(1e-50)) < 1e-40)
        return true;
    if (center != 0 && fabs(func(center + center*1e-30)) < 1e-20)
        return true;
    return false;
}

}; // End of anonymous namespace

namespace AcesoX {

PowerSeries repairWithPowerSeries(
    const std::function<xreal(xreal)>& func,
    const xreal& center,
    const xreal& radius,
    int degree,
    int side
)
{
    // Init PowerSeries
    PowerSeries powerSeries;
    powerSeries.center = center;
    powerSeries.radius = fabs(radius);
    powerSeries.degree = degree;
    powerSeries.side   = side;
    bool reducedRange = false;
    if (powerSeries.radius > 1) {
        reducedRange = true;
    }
    powerSeries.isReduceRange = reducedRange;

    xreal lbound, rbound;
    lbound = powerSeries.center - powerSeries.radius;
    rbound = powerSeries.center + powerSeries.radius;
    if (side == -1) {
        // [center-radius, center]
        rbound = powerSeries.center;
    }
    else if (side == 1) {
        // [center, center+radius]
        lbound = powerSeries.center;
    }

    // Generate Chebyshev nodes
    int intervalNodes = IntervalChebyshevNodes;
    std::vector<xreal> xs = ChebyNodes(lbound, rbound, intervalNodes);
    std::vector<xreal> ys(intervalNodes);
    for (int i = 0; i < intervalNodes; i++) {
        ys[i] = func(xs[i]);
    }

    for (int i = 0; i < intervalNodes; i++) {
        xs[i] = xs[i] - powerSeries.center;
        if (reducedRange) {
            xs[i] = xs[i] / powerSeries.radius;
        }
    }

    // Fit with values
    // Should we add reversed values as weights?
    if (guessCenterIsZero(func, powerSeries.center)) {
        powerSeries.coefs = PolyFit::PowerFitNoConstant(xs, ys, powerSeries.degree);
    }
    else {
        powerSeries.coefs = PolyFit::PowerFit(xs, ys, powerSeries.degree);
    }

    return powerSeries;
}

ChebySeries repairWithChebySeries(
    const std::function<xreal(xreal)>& func,
    const xreal& lbound,
    const xreal& rbound,
    int degree
)
{
    // Init ChebySeries
    ChebySeries chebySeries;
    chebySeries.lBound = min(lbound, rbound);
    chebySeries.rBound = max(lbound, rbound);
    chebySeries.degree = degree;

    // Generate Chebyshev nodes
    int intervalNodes = IntervalChebyshevNodes;
    std::vector<xreal> xs = ChebyNodes(
        chebySeries.lBound,
        chebySeries.rBound,
        intervalNodes);
    std::vector<xreal> ys(intervalNodes);
    for (int i = 0; i < intervalNodes; i++) {
        ys[i] = func(xs[i]);
    }
    for (int i = 0; i < intervalNodes; i++) {
        xs[i] = reduce(xs[i], chebySeries.lBound, chebySeries.rBound);
    }

    // Fit with values
    // Should we add reversed values as weights?
    chebySeries.coefs = PolyFit::ChebyFit(xs, ys, chebySeries.degree);

    return chebySeries;
}

}; // End of namespace AcesoX