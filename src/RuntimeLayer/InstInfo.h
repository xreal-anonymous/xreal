#pragma once

#include "opcode.h"
#include <string>

class InstInfo {
private:
    // Core info
    uint64_t instID;
    int32_t  instType;
public:
    InstInfo(uint64_t _instID, int32_t _instType) {
        instID = _instID;
        instType = _instType;
    }
    std::string dump() {
        using std::to_string;
        std::string line;
        line.append(to_string(instID));
        line.append(",");
        line.append(to_string(instType));
        return line;
    }
};

class InstRuntimeInfo {
public:
    uint64_t instID;
    int32_t instType;
    double op1;
    double op2;
    InstRuntimeInfo(uint64_t _instID, int32_t _instType, double _op1, double _op2)
        : instID(_instID), instType(_instType), op1(_op1), op2(_op2) { }
};

class InstDebugInfo : public InstInfo {
private:
    // Debug info
    std::string sourceFileName;
    std::string functionName;
    int32_t debugLine;
    int32_t debugCol;
public:
    InstDebugInfo(uint64_t _instID, int32_t _instType,
                std::string _sourceFileName, std::string _functionName,
                int32_t _debugLine, int32_t _debugCol)
                : InstInfo(_instID, _instType)
    {
        sourceFileName = _sourceFileName;
        functionName = _functionName;
        debugLine = _debugLine;
        debugCol = _debugCol;
    }
    std::string dump() {
        using std::to_string;
        std::string line = InstInfo::dump();
        line.append(",");
        line.append(sourceFileName);
        line.append(",");
        line.append(functionName);
        line.append(",");
        line.append(to_string(debugLine));
        line.append(",");
        line.append(to_string(debugCol));
        return line;
    }
};
