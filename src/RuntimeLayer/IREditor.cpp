/* 
 * IR Editor
 * Catch Floating-point atomic operations
 * Such as + - * / sin log etc.
 * 
 * Author: Daming
 * Date: 2021.08.17
 */

#include <string>
#include <vector>
#include <fstream>
#include <set>
#include <map>

#include "llvm/Demangle/Demangle.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"

#include "opcode.h"
#include "InstInfo.h"

using namespace llvm;

namespace {

using std::vector;
using std::string;

class MyFunctionPass : public FunctionPass {
private:
    // Created Function Declaration.
    // From Module.getOrInsertFunction()
    FunctionCallee basicOPHandler;
    FunctionCallee basicCallHandler;

    // Function modified.
    bool modified;

    // Name of function and module.
    string functionName;
    string sourceFileName;

    // Instructed Opration List
    vector<InstDebugInfo> instDebugInfoList;

    // Only insert handler calls for unsafe atomic operations?
    // If true, e.g., + - sin cos will be caught; * / sqrt exp will not.
    bool onlyUnsafe = true;
    const static std::set<Instruction::BinaryOps> unsafeAtomicOp;
    const static std::set<CallOpType> unsafeAtomicCall;

public : 
    static char ID;
    const static std::map<string, CallOpType> callNameMap;

    MyFunctionPass() : FunctionPass(ID) { }

    bool runOnFunction(Function &func) override {
        // Init class members
        initHandlersAndConstants(func);

        // Reference's declaration must come with definition.
        // we cannot put ctx in init function.
        LLVMContext &ctx = func.getContext();

        // Output Function information.
        // Use LLVM's demangler to get de-mangled name.
        // outs() << "Module: " << func.getParent()->getName()
        //        << "  Function: " << demangle(func.getName().str())
        //        << "\n";

        // Traverse every BasicBlock.
        for (auto bb_it = func.begin(), bb_it_end = func.end(); bb_it != bb_it_end; ++bb_it) {
            BasicBlock &bb = *bb_it;

            // Traverse every Instruction.
            for (auto inst_it = bb.begin(), inst_it_end = bb.end(); inst_it != inst_it_end; ++inst_it) {
                Instruction &inst = *inst_it;

                // fadd, fsub, fmul, fdiv
                if (isa<BinaryOperator>(inst)) {
                    meetBinaryOperatorInst(&inst, ctx);
                }

                if (isa<CallInst>(inst)) {
                    meetCallInst(&inst, ctx);
                }
            }
        }

        postRunOnFunction(func);

        return modified;
    }
private:
    void meetBinaryOperatorInst(Instruction* pInst, LLVMContext& ctx) {
        Instruction &inst = *pInst;

        BinaryOperator* binaryOp = cast<BinaryOperator>(&inst);
        Instruction::BinaryOps opCode = binaryOp->getOpcode();
        if (opCode == Instruction::FAdd
            || opCode == Instruction::FSub
            || opCode == Instruction::FMul
            || opCode == Instruction::FDiv)
        {

        }
        else {
            // Not a FP binary operation. Return.
            return;
        }

        // Check if only insert for unsafe
        if (onlyUnsafe && unsafeAtomicOp.count(opCode) == 0) {
            return;
        }

        // Build a function call before the binary operation.
        // params for the function call:
        // details in createBasicOPHandler
        // (1)rhs lhs (2)optype (3)instID
        IRBuilder<> builder(binaryOp);
        modified = true;

        // (1)rhs lhs
        Value* v0 = binaryOp->getOperand(0);
        Value* v1 = binaryOp->getOperand(1);

        // Cast in case v0/v1 is not a double.
        v0 = convertToDouble(v0, builder, ctx);
        v1 = convertToDouble(v1, builder, ctx);

        // (2)optype
        int opType_int;
        if (opCode == Instruction::FAdd) {
            opType_int = static_cast<int>(BinOpType::FP_Add);
        } else if (opCode == Instruction::FSub) {
            opType_int = static_cast<int>(BinOpType::FP_Sub);
        } else if (opCode == Instruction::FMul) {
            opType_int = static_cast<int>(BinOpType::FP_Mul);
        } else {
            opType_int = static_cast<int>(BinOpType::FP_Div);
        }
        ConstantInt* opType = ConstantInt::get(Type::getInt32Ty(ctx), opType_int);

        // (3)instID
        uint64_t instID_int = reinterpret_cast<uint64_t>(binaryOp);
        ConstantInt* instID = ConstantInt::get(Type::getInt64Ty(ctx), instID_int);

        // Assemble the params
        vector<Value*> handlerParams = {
            v0,
            v1,
            opType,
            instID,
        };

        // Insert the handler call. 
        CallInst* callInst = builder.CreateCall(basicOPHandler, handlerParams);

        // Get debug location if exists.
        int32_t debugLine_int = 0, debugCol_int = 0;
        const DebugLoc &debugLoc = pInst->getDebugLoc();
        if (debugLoc) {
            debugLine_int = debugLoc.getLine();
            debugCol_int  = debugLoc.getCol();
        }

        // Maintain InstInfo.
        InstDebugInfo instDebugInfo(instID_int, opType_int, sourceFileName, functionName, debugLine_int, debugCol_int);
        instDebugInfoList.push_back(instDebugInfo);

        return;
    }

    void meetCallInst(Instruction *pInst, LLVMContext &ctx) {
        Instruction &inst = *pInst;

        CallInst* callInst = cast<CallInst>(&inst);

        string calledFunctionName = callInst->getCalledFunction()->getName().str();
        string demangledName = myDemangle(calledFunctionName);
        // outs() << "      MyDemangle: " << myDemangle(calledFunctionName) << "\n";

        /* Worth mentioning:
         * Suppose we have a long double or float x.
         * If we have "using std::log" or "using namespace std", then
         *   the call stack is std::log(x) -> logf(x)/logl(x),
         *   thus two calls, "std::log" and "logf" will be caught after demangling.
         *   It is okay.
         *     If we only catch logf, we will only have one InstID, and will
         *     only search once on this InstID.
         *     It will reduce the search effectiveness of Atomu.
         * 
         * If we don't have anything with std, then
         *   first, x will be fpext or fptrunc to double,
         *   then, the default log(double) is used,
         */

        bool caught = false;
        auto it = callNameMap.find(demangledName);
        if (it == callNameMap.end()) {
            // Not a basic call in math.h
            return;
        }
        CallOpType callOpType = it->second;

        // Check if only insert for unsafe
        if (onlyUnsafe && unsafeAtomicCall.count(callOpType) == 0) {
            return;
        }

        // All operands should be Floating-point type.
        // Ignore cos(xreal x)
        for (uint32_t i = 0; i < callInst->getNumArgOperands(); i++) {
            Value *v = callInst->getArgOperand(i);
            if (v->getType()->isFloatingPointTy() == false) {
                return;
            }
        }

        caught = true;
        int callEnum_int = static_cast<int>(callOpType);
        // outs() << "    ** Basic Function Caught: " << demangledName << "\n";
        // outs() << "       Args size:" << callInst->getNumArgOperands() << '\n';

        // Build a function call before the basic function call.
        // params for the function call:
        // details in createBasicCallHandler
        // (1)arg0 arg1 (2)callEnum (3)instID
        IRBuilder<> builder(callInst);
        modified = true;

        // (1)arg0 arg1
        Value *v0, *v1;
        uint32_t argNum = callInst->getNumArgOperands();
        if (argNum == 1) {
            v0 = callInst->getArgOperand(0);
            v1 = v0; // just repeat
        }
        else if (argNum == 2) {
            v0 = callInst->getArgOperand(0);
            v1 = callInst->getArgOperand(1);
        }
        else {
            errs() << "Cannot Handle This Function: Wrong Args Number.\n";
            return;
        }
        // Cast in case v0/v1 is not a double.
        v0 = convertToDouble(v0, builder, ctx);
        v1 = convertToDouble(v1, builder, ctx);

        // (2)callEnum
        ConstantInt* callEnum = ConstantInt::get(Type::getInt32Ty(ctx), callEnum_int);

        // (3)instID
        uint64_t instID_int = reinterpret_cast<uint64_t>(callInst);
        ConstantInt* instID = ConstantInt::get(Type::getInt64Ty(ctx), instID_int);

        // Assemble the params
        vector<Value*> handlerParams = {
            v0,
            v1,
            callEnum,
            instID,
        };

        // Insert the handler call. 
        CallInst* handlerCall = builder.CreateCall(basicCallHandler, handlerParams);

        // Get debug location if exists.
        int32_t debugLine_int = 0, debugCol_int = 0;
        const DebugLoc &debugLoc = pInst->getDebugLoc();
        if (debugLoc) {
            debugLine_int = debugLoc.getLine();
            debugCol_int  = debugLoc.getCol();
        }

        // Maintain InstInfo.
        InstDebugInfo instDebugInfo(instID_int, callEnum_int, sourceFileName, functionName, debugLine_int, debugCol_int);
        instDebugInfoList.push_back(instDebugInfo);

        return;
    }

    void initHandlersAndConstants(Function &func) {
        // Record the name of function and module.
        this->functionName   = demangle(func.getName().str());
        this->sourceFileName = func.getParent()->getSourceFileName();

        // Create Handler Function Declaration.
        // getOrInsertFunction() return FunctionCallee
        this->basicOPHandler   = createBasicOPHandler(func);
        this->basicCallHandler = createBasicCallHandler(func);

        // Function modified.
        // return value for runOnFunction.
        this->modified = false;

        // Clear the instInfoList
        this->instDebugInfoList.clear();

        return;
    }

    void postRunOnFunction(Function &func) {
        writeInstInfoList();
    }

    void writeInstInfoList() {
        std::ofstream fout;
        // Example of source name: build/intermediate/gamma.double.cpp
        // We want:                              data/gamma.double.instinfo
        // outs() << getSourceFileName << "\n";
        std::size_t truncPos;

        // get gamma.double.ll
        truncPos = sourceFileName.find_last_of("/");
        string pureFileName = sourceFileName.substr(truncPos+1); // Also works for not found, since npos is -1.

        truncPos = pureFileName.find_last_of(".");
        string pureFileNameWithoutSuffix = pureFileName.substr(0, truncPos);

        string instInfoFilePath = "data/" + pureFileNameWithoutSuffix + ".instinfo";

        // Make Dir
        system("mkdir -p data");
        // Open file
        fout.open(instInfoFilePath, std::ofstream::out | std::ofstream::app);
        if (!fout.is_open()) {
            errs() << "Open File Failed: " << instInfoFilePath << "\n";
        }
        // Write InstInfo
        for (int i = 0; i < instDebugInfoList.size(); i++) {
            fout << instDebugInfoList[i].dump() << "\n";
        }
        fout.close();
    }

    FunctionCallee createBasicOPHandler(Function &func) {
        /* void binOpHandler(
            double lhs,
            double rhs,
            int32_t opType,
            uint64_t instID)
        */
        LLVMContext &ctx = func.getContext();
        Module* module   = func.getParent();
        string OPHandlerName = "binOpHandler";
        Type* retType = Type::getVoidTy(ctx);
        vector<Type*> paramType = {
            Type::getDoubleTy(ctx),
            Type::getDoubleTy(ctx),
            Type::getInt32Ty(ctx),
            Type::getInt64Ty(ctx),
        };
        FunctionType* OPHandlerType = FunctionType::get(retType, paramType, false);

        FunctionCallee OPHandler = module->getOrInsertFunction(OPHandlerName, OPHandlerType);
        return OPHandler;
    }

    FunctionCallee createBasicCallHandler(Function &func) {
        /* void callOpHandler(
            double arg1,
            double arg2,
            int32_t callEnum,
            uint64_t instID)
        */
        LLVMContext &ctx = func.getContext();
        Module* module   = func.getParent();
        string OPHandlerName = "callOpHandler";
        Type* retType = Type::getVoidTy(ctx);
        vector<Type*> paramType = {
            Type::getDoubleTy(ctx),
            Type::getDoubleTy(ctx),
            Type::getInt32Ty(ctx),
            Type::getInt64Ty(ctx),
        };
        FunctionType* OPHandlerType = FunctionType::get(retType, paramType, false);

        FunctionCallee OPHandler = module->getOrInsertFunction(OPHandlerName, OPHandlerType);
        return OPHandler;
    }

    // Small Utility Functions
    string myDemangle(const string& mangledName) {
        ItaniumPartialDemangler Demangler;
        // partialDemangle returns true for error. 
        if (Demangler.partialDemangle(mangledName.c_str()) == true) {
            return mangledName;
        }
        else {
            char buf[128] = {};
            size_t N = 128;
            char* demangledName = Demangler.getFunctionBaseName(buf, &N);
            string strName(demangledName);
            return strName;
        }
    }

    Value* convertToDouble(Value* v, IRBuilder<>& builder, LLVMContext& ctx) {
        if (v->getType()->isFloatingPointTy() && !v->getType()->isDoubleTy()) {
            v = builder.CreateFPCast(v, Type::getDoubleTy(ctx));
        }
        return v;
    }
};  // end of class MyFunctionPass

const std::set<Instruction::BinaryOps> MyFunctionPass::unsafeAtomicOp = {
    Instruction::FAdd,
    Instruction::FSub,
};

const std::set<CallOpType> MyFunctionPass::unsafeAtomicCall = {
    CallOpType::SIN,
    CallOpType::COS,
    CallOpType::TAN,
    CallOpType::ASIN,
    CallOpType::ACOS,
    CallOpType::SINH,
    CallOpType::COSH,
    CallOpType::LOG,
    CallOpType::LOG10,
    CallOpType::POW,
};

const std::map<std::string, CallOpType> MyFunctionPass::callNameMap = {
    {"sin", CallOpType::SIN},
    {"sinf", CallOpType::SIN},
    {"sinl", CallOpType::SIN},
    {"cos", CallOpType::COS},
    {"cosf", CallOpType::COS},
    {"cosl", CallOpType::COS},
    {"tan", CallOpType::TAN},
    {"tanf", CallOpType::TAN},
    {"tanl", CallOpType::TAN},
    {"asin", CallOpType::ASIN},
    {"asinf", CallOpType::ASIN},
    {"asinl", CallOpType::ASIN},
    {"acos", CallOpType::ACOS},
    {"acosf", CallOpType::ACOS},
    {"acosl", CallOpType::ACOS},
    {"atan", CallOpType::ATAN},
    {"atanf", CallOpType::ATAN},
    {"atanl", CallOpType::ATAN},
    {"atan2", CallOpType::ATAN2},
    {"atan2f", CallOpType::ATAN2},
    {"atan2l", CallOpType::ATAN2},
    {"sinh", CallOpType::SINH},
    {"sinhf", CallOpType::SINH},
    {"sinhl", CallOpType::SINH},
    {"cosh", CallOpType::COSH},
    {"coshf", CallOpType::COSH},
    {"coshl", CallOpType::COSH},
    {"tanh", CallOpType::TANH},
    {"tanhf", CallOpType::TANH},
    {"tanhl", CallOpType::TANH},
    {"exp", CallOpType::EXP},
    {"expf", CallOpType::EXP},
    {"expl", CallOpType::EXP},
    {"log", CallOpType::LOG},
    {"logf", CallOpType::LOG},
    {"logl", CallOpType::LOG},
    {"log10", CallOpType::LOG10},
    {"log10f", CallOpType::LOG10},
    {"log10l", CallOpType::LOG10},
    {"sqrt", CallOpType::SQRT},
    {"sqrtf", CallOpType::SQRT},
    {"sqrtl", CallOpType::SQRT},
    {"pow", CallOpType::POW},
    {"powf", CallOpType::POW},
    {"powl", CallOpType::POW},
};

}   // end of anonymous namespace

char MyFunctionPass::ID = 0;

static RegisterPass<MyFunctionPass> FP("funcpass", "My Function Pass");