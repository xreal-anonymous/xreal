/* Use system's Eigen & mpreal
 * Eigen version: 3.3.7
 * mpreal / mpfrc++ version: 3.6.6
 * These two libraries need to be dealt carefully.
 * For example, mpreal 3.6.8 is not compatible with eigen 3.3.7 to 3.4.0.
 */

#include <unsupported/Eigen/MPRealSupport>
#include <Eigen/Dense>
#include <mpreal.h>
#include <cassert>

#include "../xreal/xreal.h"
#include "Polynomial.h"

using namespace Eigen;

using MatrixXmp = Matrix<mpfr::mpreal, Dynamic, Dynamic>;
using VectorXmp = Matrix<mpfr::mpreal, Dynamic, 1>;

namespace PolyFit {

std::vector<xreal> PowerFitWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree)
{
    // Size of Xs, Ys and Ws should be same.
    assert(Xs.size() == Ys.size());
    assert(Xs.size() == Ws.size());

    // Init Space of
    // X: Eigen Matrix
    // y: Eigen Vector,
    // w: Eigen Vector, used as Diagonal later.
    MatrixXmp X(Xs.size(), degree+1);
    VectorXmp y(Ys.size());
    VectorXmp w(Ws.size());

    // Init value of X, y and w.
    for (int i = 0; i < Xs.size(); i++) {
        xreal xi = Xs[i];

        X(i, 0) = 1.0;
        for (int j = 1; j <= degree; j++) {
            X(i, j) = X(i, j-1) * xi;
        }
    }
    for (int i = 0; i < Ys.size(); i++)
        y(i) = Ys[i];
    for (int i = 0; i < Ws.size(); i++)
        w(i) = Ws[i];

    // Solve the Fitting with SVD.
    VectorXmp coefsVec = (X.transpose() * w.asDiagonal() * X)
                        .bdcSvd(ComputeThinU | ComputeThinV)
                        .solve(X.transpose() * w.asDiagonal() * y);

    // Construct coefs
    std::vector<xreal> coefs(coefsVec.data(), coefsVec.data() + coefsVec.rows()*coefsVec.cols());
    return coefs;
}

std::vector<xreal> PowerFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree)
{
    assert(Xs.size() == Ys.size());
    std::vector<xreal> Ws(Xs.size(), (xreal)1.0);
    return PowerFitWeighted(Xs, Ys, Ws, degree);
}

std::vector<xreal> PowerFitNoConstantWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree)
{
    // Size of Xs, Ys and Ws should be same.
    assert(Xs.size() == Ys.size());
    assert(Xs.size() == Ws.size());

    // Init Space of
    // X: Eigen Matrix
    // y: Eigen Vector,
    // w: Eigen Vector, used as Diagonal later.
    MatrixXmp X(Xs.size(), degree);
    VectorXmp y(Ys.size());
    VectorXmp w(Ws.size());

    // Init value of X, y and w.
    for (int i = 0; i < Xs.size(); i++) {
        xreal xi = Xs[i];

        X(i, 0) = xi;
        for (int j = 1; j < degree; j++) {
            X(i, j) = X(i, j-1) * xi;
        }
    }
    for (int i = 0; i < Ys.size(); i++)
        y(i) = Ys[i];
    for (int i = 0; i < Ws.size(); i++)
        w(i) = Ws[i];

    // Solve the Fitting with SVD.
    VectorXmp coefsVec = (X.transpose() * w.asDiagonal() * X)
                        .bdcSvd(ComputeThinU | ComputeThinV)
                        .solve(X.transpose() * w.asDiagonal() * y);

    // Construct coefs
    // with first term as (implicit) 0.
    std::vector<xreal> coefs;
    coefs.push_back(0.0);
    for (int i = 0; i < coefsVec.size(); i++)
        coefs.push_back(coefsVec(i));
    return coefs;
}

std::vector<xreal> PowerFitNoConstant(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree)
{
    assert(Xs.size() == Ys.size());
    std::vector<xreal> Ws(Xs.size(), (xreal)1.0);
    return PowerFitNoConstantWeighted(Xs, Ys, Ws, degree);
}

std::vector<xreal> ChebyFitWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree)
{
    // Size of Xs, Ys and Ws should be same.
    assert(Xs.size() == Ys.size());
    assert(Xs.size() == Ws.size());

    // Init Space of
    // X: Eigen Matrix
    // y: Eigen Vector,
    // w: Eigen Vector, used as Diagonal later.
    MatrixXmp X(Xs.size(), degree+1);
    VectorXmp y(Ys.size());
    VectorXmp w(Ws.size());

    // Init value of X, y and w.
    for (int i = 0; i < Xs.size(); i++) {
        xreal xi = Xs[i];
        xreal xi2 = 2.0*xi;

        for (int j = 0; j <= degree; j++) {
            if (j == 0) {
                X(i, j) = 1.0;
            }
            else if (j == 1) {
                X(i, j) = xi;
            }
            else {
                X(i, j) = X(i, j-1)*xi2 - X(i, j-2);
            }
        }
    }
    for (int i = 0; i < Ys.size(); i++)
        y(i) = Ys[i];
    for (int i = 0; i < Ws.size(); i++)
        w(i) = Ws[i];

    // Solve the Fitting with SVD.
    VectorXmp coefsVec = (X.transpose() * w.asDiagonal() * X)
                        .bdcSvd(ComputeThinU | ComputeThinV)
                        .solve(X.transpose() * w.asDiagonal() * y);

    // Construct coefs
    std::vector<xreal> coefs(coefsVec.data(), coefsVec.data() + coefsVec.rows()*coefsVec.cols());
    return coefs;
}

std::vector<xreal> ChebyFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree)
{
    assert(Xs.size() == Ys.size());
    std::vector<xreal> Ws(Xs.size(), (xreal)1.0);
    return ChebyFitWeighted(Xs, Ys, Ws, degree);
}

inline xreal SIGN(const xreal& a, const xreal& b) {
    return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);
}

// inline void EigenVec2StdVec(const VectorXmp& eigenvec, std::vector<xreal>& vec) {
//     vec.resize(eigenvec.rows());
//     for (int i = 0; i < eigenvec.rows(); i++)
//         vec[i] = eigenvec(i);
//     return;
// }

std::vector<xreal> RationalFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int numDegree,
    int denomDegree)
{
    assert(Xs.size() == Ys.size());
    // From Numerical Receipt: ratlsq.
    // Init necessary values
    const int MAXIT=5;
    int i, it, j, ncof=numDegree+denomDegree+1, npt=Xs.size();
    xreal dev, devmax, e, hth, power, sum;
    VectorXmp bb(npt), coff(ncof), ee(npt), wt(npt);
    MatrixXmp X(npt, ncof);

    dev = (xreal)1e99;
    // Init best rational series.
    RationalSeries ratbest;
    ratbest.numDegree = numDegree;
    ratbest.denomDegree = denomDegree;
    // EigenVec2StdVec(coff, ratbest.coefs);
    ratbest.coefs.assign(coff.data(), coff.data()+coff.rows()*coff.cols());

    for (int i = 0; i < npt; i++) {
        wt[i] = 1.0;
        ee[i] = 1.0;
    }
    e = 0.0;
    for (it = 0; it < MAXIT; it++) {
        for (i = 0; i < npt; i++) {
            power = wt[i];
            bb[i] = power*(Ys[i]+SIGN(e, ee[i]));
            for (j = 0; j < numDegree+1; j++) {
                X(i, j) = power;
                power *= Xs[i];
            }
            power = -bb[i];
            for (j = numDegree+1; j < ncof; j++) {
                power *= Xs[i];
                X(i, j) = power;
            }
        }

        coff = X.bdcSvd(ComputeThinU | ComputeThinV).solve(bb);

        devmax = sum = 0;
        RationalSeries ratcur;
        ratcur.numDegree = numDegree;
        ratcur.denomDegree = denomDegree;
        // EigenVec2StdVec(coff, ratcur.coefs);
        ratcur.coefs.assign(coff.data(), coff.data()+coff.rows()*coff.cols());
        
        for (j = 0; j < npt; j++) {
            ee[j] = ratcur.eval(Xs[j]) - Ys[j];
            wt[j] = fabs(ee[j]);
            sum += wt[j];
            if (wt[j] > devmax) devmax = wt[j];
        }

        e = sum/npt;
        if (devmax <= dev) {
            ratbest = ratcur;
            dev = devmax;
        }
    }

    return ratbest.coefs;
}

};
