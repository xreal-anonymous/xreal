#pragma once

#include "../xreal/xreal.h"
#include <functional>

using doubleFunc_t = double(*)(double);
using realFunc_t   = xreal(*)(xreal);
using floatFunc_t  = float(*)(float);
using longDoubleFunc_t = long double(*)(long double);

using doubleFuncCpp_t = std::function<double(double)>;
using realFuncCpp_t   = std::function<xreal(xreal)>;
using floatFuncCpp_t  = std::function<float(float)>;
using longDoubleFuncCpp_t = std::function<long double(long double)>;

double lambert_W0(double x);
xreal lambert_W0(xreal x);

long double lngamma(long double x);
double lngamma(double x);
float lngamma(float x);
xreal lngamma(xreal x);

double dilog(double x);
xreal dilog(xreal x);