#pragma once

const int MAXOPNUM = 30;

enum class BinOpType {
    FP_Add = 20,
    FP_Sub = 21,
    FP_Mul = 22,
    FP_Div = 23
};

enum class CallOpType {
    SIN   = 1,
    COS   = 2,
    TAN   = 3,
    ASIN  = 4,
    ACOS  = 5,
    ATAN  = 6,
    ATAN2 = 7,
    SINH  = 8,
    COSH  = 9,
    TANH  = 10,
    EXP   = 11,
    LOG   = 12,
    LOG10 = 13,
    SQRT  = 14,
    POW   = 15
};
