#pragma once

#include "opcode.h"
#include "InstInfo.h"
#include <vector>


// Do as few as possible.
// Since the communicator is called before *EVERY* floating-point instruction.
class Communicator {
// Singleton Pattern.
// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
public:
    static Communicator& getInstance() {
        static Communicator instance;
        return instance;
    }
private:
    Communicator() { }

public:
    Communicator(Communicator const &)   = delete;
    void operator=(Communicator const &) = delete;
// Singleton Pattern Done.

private:
    std::vector<InstRuntimeInfo> instRuntimeInfoList;
    bool switchOn = false;
public:
    void writeInstRuntimeInfo(uint64_t instID, int32_t instType, double op1, double op2) {
        if (switchOn) {
            instRuntimeInfoList.push_back(InstRuntimeInfo(instID, instType, op1, op2));
        }
    }

    void initCommunicator() {
        switchOn = true;
        clearInfoList();
    }

    void clearInfoList() {
        std::vector<InstRuntimeInfo> emptyList;
        std::swap(instRuntimeInfoList, emptyList);
        instRuntimeInfoList.reserve(128);
    }

    void clear() {
        clearInfoList();
    }

    bool isEmpty() {
        return instRuntimeInfoList.empty();
    }

    std::vector<InstRuntimeInfo> getInstRuntimeInfoList() {
        return instRuntimeInfoList;
    }

};
