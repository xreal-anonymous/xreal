#pragma once

#include "Polynomial.h"
#include "../xreal/xreal.h"

#include <functional>


namespace AcesoX {

PowerSeries repairWithPowerSeries(
    const std::function<xreal(xreal)>& func,
    const xreal& center,
    const xreal& radius,
    int degree,
    int side=0
);

ChebySeries repairWithChebySeries(
    const std::function<xreal(xreal)>& func,
    const xreal& lbound,
    const xreal& rbound,
    int degree
);

};