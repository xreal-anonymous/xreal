#pragma once

#include "../xreal/xreal.h"
#include <vector>
#include <cassert>
#include <iostream>
#include <iomanip>

namespace {
    xreal reduce(const xreal& x, const xreal& lBound, const xreal& rBound) {
        using std::fabs;
        xreal sum = 2*x, c = 0.0, temp, u;
        temp = sum - lBound;
        if (fabs(sum) >= fabs(lBound))
            c = c + ((sum-temp)-lBound);
        else
            c = c + ((-lBound-temp)+sum);
        sum = temp;
        temp = sum - rBound;
        if (fabs(sum) >= fabs(rBound))
            c = c + ((sum-temp)-rBound);
        else
            c = c + ((-rBound-temp)+sum);
        sum = temp;

        sum = sum + c;
        u = sum / (rBound - lBound);
        return u;
    }
};

class PowerSeries {
public:
    xreal center, radius;
    bool isReduceRange;
    int degree;
    std::vector<xreal> coefs;
    // side= 0 for both side;
    // side=-1 for [center-radius, center];
    // side= 1 for [center, center+radius];
    int side;

    xreal eval(xreal x) {
        assert(coefs.size() == degree + 1);

        // Only use Taylor form. f(x-a) = f(a) + c1*(x-a) + c2*(x-a)^2 + ...
        x = x - center;
        if (isReduceRange) {
            x = x / radius;
        }

        // Horner method.
        int n = coefs.size();
        xreal y = coefs[n-1];
        for (int i = n-2; i>=0; i--) {
            y = y*x + coefs[i];
        }
        return y;
    }
    void show() {
        std::cout << "Power Series:\n";
        std::cout << "Degree: [ " << degree << " ], ";
        std::cout << "Range: ( " << center << " +/- " << radius << " )\n";
        std::cout << "coefs:\n";
        for (int i = 0; i < coefs.size(); i++) {
            std::cout << "  " << coefs[i].toString("%.30Re") << ",\n";
        }
        show_compensate();
    }
    void show_compensate() {
        float center_f = (float)center;
        float comp_f = (float)(center_f - center);

        double center_d = (double)center;
        double comp_d = (double)(center_d - center);

        long double center_ld = (long double)center;
        long double comp_ld = (long double)(center_ld - center);

        std::cout << "Float:" << "z=x-" << std::setprecision(7) << std::scientific
            << center_f << "f +" << comp_f << "f;\n";
        std::cout << "Double:" << "z=x-" << std::setprecision(16) << std::scientific
            << center_d << " + " << comp_d << ";\n";
        std::cout << "Long Double:" << "z=x-" << std::setprecision(34) << std::scientific
            << center_ld << "l + " << comp_ld << "l;\n";
    }

    bool inRegion(xreal x) {
        xreal l = center-radius;
        xreal r = center+radius;
        if (side == -1)
            r = center;
        else if (side == 1)
            l = center;
        // Check
        if (l <= x && x <= r)
            return true;
        return false;
    }
};

class ChebySeries {
public:
    xreal lBound, rBound;
    int degree;
    std::vector<xreal> coefs;
    xreal eval(xreal x) {
        assert(coefs.size() == degree + 1);

        // in Chebyshev Polynomial, x is always reduced to [-1, 1]
        xreal u = reduce(x, lBound, rBound);
        xreal u2 = 2.0 * u;
        // Clenshaw method
        int n = coefs.size();
        xreal d = 0.0, dd = 0.0, temp;
        for (int i = n-1; i>=1; i--) {
            temp = d;
            d = u2*d - dd + coefs[i];
            dd = temp;
        }
        d = u*d - dd + coefs[0];
        return d;
    }
    void show() {
        std::cout << "Chebyshev Series:\n";
        std::cout << "Degree: [ " << degree << " ], ";
        std::cout << "Range: ( " << lBound << " , " << rBound << " )\n";
        std::cout << "coefs:\n";
        for (int i = 0; i < coefs.size(); i++) {
            std::cout << "  " << coefs[i].toString("%.30Re") << ",\n";
        }
    }
    bool inRegion(xreal x) {
        if (lBound <= x && x <= rBound)
            return true;
        return false;
    }
};

class RationalSeries {
public:
    xreal lBound, rBound;
    int numDegree, denomDegree;
    // numTerms=5: p0, p1, p2, p3, p4, p5
    // denomTerms=5: q1, q2, q3, q4, q5
    std::vector<xreal> coefs;
    xreal eval(xreal x) {
        assert(coefs.size() == numDegree + denomDegree + 1);
        xreal num = coefs[numDegree];
        for (int i = numDegree-1; i>=0; i--) {
            num = num*x + coefs[i];
        }
        xreal demon = coefs[numDegree + denomDegree];
        for (int i = numDegree+denomDegree-1; i>numDegree; i--) {
            demon = demon*x + coefs[i];
        }
        demon = demon*x + 1.0; // implicit q0 = 1.0;
        return num / demon;
    }
    void show() {
        std::cout << "\nRational Series:\n";
        std::cout << "[ " << numDegree << " / " << denomDegree << " ]\n";
        std::cout << "coefs:\n";
        for (int i = 0; i < coefs.size(); i++) {
            std::cout << "  " << coefs[i].toString("%.15Re") << ",\n";
        }
    }
};

