#include <cassert>
#include "RootFinding.h"

inline bool hasSameSign(const xreal &a, const xreal& b) {
    return ((a > 0 && b > 0) || (a < 0 && b < 0));
}

xreal findRoot(const std::function<xreal(xreal)>& func, const xreal& lbound, const xreal& rbound) {
    if (func(lbound) == 0) return lbound;
    if (func(rbound) == 0) return rbound;
    bool diffSign = !hasSameSign(func(lbound), func(rbound));
    assert(diffSign == true);

    xreal l = min(lbound, rbound);
    xreal r = max(lbound, rbound);
    xreal lres = func(l);
    xreal rres = func(r);

    xreal relativeThreshold = 1e-80;
    while (1) {
        xreal dist = fabs((r - l)/max(l, r));
        if (dist < relativeThreshold)
            return l;
        xreal mid = l + (r - l)/2;
        xreal midres = func(mid);
        if (hasSameSign(lres, midres)) {
            l = mid;
            lres = midres;
            continue;
        }
        else {
            r = mid;
            rres = midres;
            continue;
        }
    }
}