#pragma once

#include <vector>
#include "../xreal/xreal.h"
#include "Polynomial.h"

namespace PolyFit {

std::vector<xreal> PowerFitWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree);

std::vector<xreal> PowerFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree);

std::vector<xreal> PowerFitNoConstantWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree);

std::vector<xreal> PowerFitNoConstant(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree);

std::vector<xreal> ChebyFitWeighted(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    const std::vector<xreal>& Ws,
    int degree);

std::vector<xreal> ChebyFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int degree);

std::vector<xreal> RationalFit(
    const std::vector<xreal>& Xs,
    const std::vector<xreal>& Ys,
    int numDegree,
    int denomDegree);
};
