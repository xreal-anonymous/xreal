#include "../xreal/xreal.h"
#include <cstdio>
#include <chrono>
#include <iomanip>
#include <functional>

#include "Atomu.h"
#include "AcesoX.h"
#include "RootFinding.h"
#include "TargetFunctions.h"

void testRepairLngamma() {
    doubleFuncCpp_t originFunc = static_cast<doubleFunc_t>(lngamma);
    realFuncCpp_t oracleFunc = static_cast<realFunc_t>(lngamma);
    Atomu::AtomuDetector<double> atomu(originFunc, oracleFunc);
    auto res = atomu.detectErrorInputs();
    std::cout << "-------\nAtomu's Error Detection Result:\n";
    for (auto i : res) {
        double err = fabs((double)((oracleFunc(i)-originFunc(i))/oracleFunc(i)));
        std::cout << std::scientific << std::setprecision(10) << "Input: " << std::showpos << i;
        std::cout << std::noshowpos << std::setprecision(5) << "  Error: " << err << "\n";
    }

    xreal rt = findRoot(oracleFunc, -2.46, -2.45);
    std::cout << "root:" << rt << "\n";
    PowerSeries ser_rt = AcesoX::repairWithPowerSeries(oracleFunc, rt, 1e-4, 6);
    ser_rt.show();
    PowerSeries ser_1 = AcesoX::repairWithPowerSeries(oracleFunc, 1, 1e-2, 15, 0);
    PowerSeries ser_2 = AcesoX::repairWithPowerSeries(oracleFunc, 2, 1e-2, 15, 0);
    std::cout << "LogGamma at x=1:\n";
    ser_1.show();
    std::cout << "LogGamma at x=2:\n";
    ser_2.show();

    // ChebySeries ser_1c = AcesoX::repairWithChebySeries(oracleFunc, 0.875, 1.125, 20);
    // ChebySeries ser_2c = AcesoX::repairWithChebySeries(oracleFunc, 1.875, 2.125, 20);
    // ser_1c.show();
    // ser_2c.show();

    double tests[] = {
        // 1-1e-3,
        // 1-1e-6,
        // 1-1e-13,
        // 1-1e-15,
        
        // 1+1e-15,
        // 1+1e-12,
        // 1+1e-8,
        // 1+1e-4,
        1.0010666935318842e+00
    };
    for (double t : tests) {
        std::cout << std::setprecision(15);
        std::cout << "lngamma(1+" << 1-t << "): \n" 
            << "origin: " << originFunc(t) << " err: " << (originFunc(t)-oracleFunc(t)) / oracleFunc(t) << "\n"
            << "repair: " << ser_1.eval(t) << " err: " << (ser_1.eval(t)-oracleFunc(t)) / oracleFunc(t) << "\n"
            << "oracle: " << oracleFunc(t) << "\n";
    }
    
    double tests_0[] = {
        1e-3,
        1e-6,
        1e-13,
        1e-15,
        1e-20,
        1e-40,
        0.03,
        0.04,
        0.05,
        0.12,
    };
    for (double t : tests_0) {
        std::cout << "lngamma(" << t << "): \n" 
            << "origin: " << originFunc(t) << " err: " << (originFunc(t)-oracleFunc(t)) / oracleFunc(t) << "\n"
            << "oracle: " << oracleFunc(t) << "\n";
    }
}

void testRepairLambertW() {
    doubleFuncCpp_t originFunc = static_cast<doubleFunc_t>(lambert_W0);
    realFuncCpp_t   oracleFunc = static_cast<realFunc_t>(lambert_W0);

    Atomu::AtomuDetector<double> atomu(originFunc, oracleFunc);

    auto res = atomu.detectErrorInputs();
    std::cout << "-------\nAtomu's Error Detection Result:\n";
    for (auto i : res) {
        double err = fabs((double)((oracleFunc(i)-originFunc(i))/oracleFunc(i)));
        std::cout << std::scientific << std::setprecision(10) << "Input: " << std::showpos << i;
        std::cout << std::noshowpos << std::setprecision(5) << "  Error: " << err << "\n";
    }

    PowerSeries ser_0 = AcesoX::repairWithPowerSeries(oracleFunc, 0, 1e-4, 6);
    ser_0.show();
    xreal in = -1e-20;
    std::cout << "lambert(" << in << "): " << ser_0.eval(in) << "\n";
}

void testRepairDilog() {
    doubleFuncCpp_t originFunc = static_cast<doubleFunc_t>(dilog);
    realFuncCpp_t   oracleFunc = static_cast<realFunc_t>(dilog);

    Atomu::AtomuDetector<double> atomu(originFunc, oracleFunc);
    auto res = atomu.detectErrorInputs();
    std::cout << "-------\nAtomu's Error Detection Result:\n";
    for (auto i : res) {
        double err = fabs((double)((oracleFunc(i)-originFunc(i))/oracleFunc(i)));
        std::cout << std::scientific << std::setprecision(10) << "Input: " << std::showpos << i;
        std::cout << std::noshowpos << std::setprecision(5) << "  Error: " << err << "\n";
    }

    PowerSeries ser_0 = AcesoX::repairWithPowerSeries(oracleFunc, 0, 1e-2, 10);
    ser_0.show();

    ChebySeries ser_1_1 = AcesoX::repairWithChebySeries(oracleFunc, -0.5, 0.5, 25);
    ser_1_1.show();

    xreal rt = findRoot(oracleFunc, +1.2595170370e+01 - 1, +1.2595170370e+01 + 1);
    PowerSeries ser_rt = AcesoX::repairWithPowerSeries(oracleFunc, rt, 1e-2, 10);
    ser_rt.show();

    std::vector<double> xs = {
        -1.2345,
        -0.2345,
        -1e-2,
        -1e-10,
        1e-40,
        1e-6,
        0.12345,
        0.9832,
        -2.6794919243e-01,
        +1.2595170370e+01,
    };

    for (auto x : xs) {
        std::cout << "x: " << x << "\n";
        if (ser_0.inRegion(x))
            std::cout << "  PowerSeries: " << ser_0.eval(x).toString("%.16Re") << "\n";
        else if (ser_1_1.inRegion(x))
            std::cout << "  ChebySeries: " << ser_1_1.eval(x).toString("%.16Re") << "\n";
        else if (ser_rt.inRegion(x))
            std::cout << "  PowerSeries: " << ser_rt.eval(x).toString("%.16Re") << "\n";
        std::cout << "  Oracle:      " << oracleFunc(x).toString("%.16Re") << "\n";
    }
}

void testAtomu() {
    // C style function pointer to disambiguate the overloaded functions.
    // https://stackoverflow.com/questions/30393285/stdfunction-fails-to-distinguish-overloaded-functions
    // https://stackoverflow.com/questions/10111042/wrap-overloaded-function-via-stdfunction

    // std::function<float(float)> originFunc   = static_cast<floatFuncType>(lngamma);
    // std::function<long double(long double)> originFunc = static_cast<longdoubleFuncType>(lngamma);
    doubleFuncCpp_t originFunc = static_cast<doubleFunc_t>(lambert_W0);
    realFuncCpp_t oracleFunc   = static_cast<realFunc_t>(lambert_W0);

    Atomu::AtomuDetector<double> atomu(originFunc, oracleFunc);

    auto res = atomu.detectErrorInputs();
    std::cout << "-------\nAtomu's Error Detection Result:\n";
    for (auto i : res) {
        double err = fabs((double)((oracleFunc(i)-originFunc(i))/oracleFunc(i)));
        std::cout << std::scientific << std::setprecision(10) << "Input: " << std::showpos << i;
        std::cout << std::noshowpos << std::setprecision(5) << "  Error: " << err << "\n";
    }
}

void testRunTime(int64_t loops) {
    using namespace std::chrono;
    auto start = steady_clock::now();

    // Test:
    // 1. lngamma
    // 2. C-style function pointer
    // 3. Cpp-style std::function wrapper
    doubleFunc_t funcPtr_c = lngamma;
    std::function<double(double)> funcPtr_cpp = static_cast<doubleFunc_t>(lngamma);

    for (int64_t i = 0; i < loops; i++) {
        double t = lngamma((double)i);
        // double t = funcPtr_c((double)i);
        // double t = funcPtr_cpp((double)i);
    }

    auto end = steady_clock::now();

    std::cout << "\nTime in milliseconds: "
        << duration_cast<milliseconds>(end - start).count()
        << "ms\n";

    return;
}

void testUserInput() {
    while (1) {
        double in, out;
        long double lin, lout;
        xreal xin, xout;

        std::printf("input:\n");
        std::scanf("%lf", &in);

        xin = in;
        lin = in;

        out = lngamma(in);
        xout = lngamma(xin);
        lout = lngamma(lin);

        std::printf("double:   %.17e\n", out);
        std::printf("long dbl: %.17Le\n", lout);
        std::printf("oracle:   %.17Le\n", (long double)xout);
        if ((double)xout != 0) {
            printf("  Relative Error (d):  %.2e\n", (double)(fabs(out-xout)/xout));
        }
        if ((long double)xout != 0) {
            printf("  Relative Error (ld): %.2e\n", (double)(fabs(lout-xout)/xout));
        }
        std::printf("\n");
    }
    return;
}

int main() {
    // testRunTime(10000000);
    // testUserInput();
    // testAtomu();

    testRepairLngamma();
    // testRepairDilog();
    // testRepairLambertW();
    return 0;
}