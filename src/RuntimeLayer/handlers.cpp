#include <cstdint>
#include "Communicator.h"

extern "C" void binOpHandler(double a, double b, int32_t opType, uint64_t instID);
extern "C" void callOpHandler(double a, double b, int32_t callType, uint64_t instID);

void binOpHandler(double a, double b, int32_t opType, uint64_t instID) {
    Communicator &comm = Communicator::getInstance();
    comm.writeInstRuntimeInfo(instID, opType, a, b);
}

void callOpHandler(double a, double b, int32_t callType, uint64_t instID) {
    Communicator &comm = Communicator::getInstance();
    comm.writeInstRuntimeInfo(instID, callType, a, b);
}