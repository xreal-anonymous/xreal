#ifndef XREAL_H
#define XREAL_H

#include <mpreal.h>
#include <cstdint>
#include <iostream>

class xreal : public mpfr::mpreal {
private:
	const static uint32_t _var_least_prec       = 2048;
	const static uint32_t _const_var_least_prec = 2048;

	static uint32_t _const_var_prec;

	// Trick to do something before main()
	static uint32_t doInitialization();
	static uint32_t doInit;

public:
	static mpfr::mpreal pi;
	static mpfr::mpreal e;
	static mpfr::mpreal euler;

	// Constructor based on typical mpreal constructor
	template <typename... Args>
	xreal(Args &&... args) : mpfr::mpreal(std::forward<Args>(args)...)
	{
		// No longer needed since we set the default precision in doInitialization()
		// 
		// Check if precision is enough
		// Also need: xreal(Args &&... args) : mpfr::mpreal((args)...)
		// if (get_prec() < _var_least_prec) {
		// 	set_prec(_var_least_prec);
		// 	std::cerr << "-- XReal precision too low. --\n";
		// 	((std::cerr << std::forward<Args>(args) << " "), ...);
		// 	std::cerr << "\n-- End --\n";
		// }
	}
};

#endif