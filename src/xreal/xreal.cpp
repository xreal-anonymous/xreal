#include "xreal.h"

// Tricks for initializing precision..

uint32_t xreal::_const_var_prec = (uint32_t)fmax(_const_var_least_prec, xreal::get_default_prec());
mpfr::mpreal xreal::pi = mpfr::const_pi(_const_var_prec);
mpfr::mpreal xreal::e = exp(mpfr::mpreal(1, _const_var_prec));
mpfr::mpreal xreal::euler = mpfr::const_euler(_const_var_prec);

uint32_t xreal::doInitialization() {
    // std::cout << "Init xreal...\n";
    // getchar();
    set_default_prec(_var_least_prec);
    return _var_least_prec;
}

uint32_t xreal::doInit = xreal::doInitialization();
