#ifndef TYPEVALUECONVERTER_H
#define TYPEVALUECONVERTER_H

class Transformer;

int execTypeValueConverter(Transformer& t);

#endif