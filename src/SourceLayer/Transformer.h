#ifndef TRANSFORMER_H
#define TRANSFORMER_H

#include <iostream>
#include <string>
#include <cstdint>
#include <vector>
#include <map>

#include "../xreal/xreal.h"
#include "TypeValueConverter.h"

class TransformerSource {
private:
	std::string sourceFilePath;
public:
	TransformerSource(const std::string& srcPath) {
		sourceFilePath = srcPath;
	}
	std::string getSourceFilePath() {
		return sourceFilePath;
	}
	// For ClangTool
	std::vector<std::string> getSourceList() {
		std::vector<std::string> srcList = { sourceFilePath };
		return srcList;
	}
};

enum class FPType { FP_Double, FP_Real, FP_Float, FP_LongDouble, FP_Other };

class TransformerTarget {
private:
	std::string targetFilePath;

	FPType targetFPType = FPType::FP_Double; // use double by default.
	std::string targetFPTypeStr;
	uint32_t targetFPTypeDigits;

public:
	TransformerTarget(const std::string& tgtPath) {
		targetFilePath = tgtPath;
		initTypeStrDigits(FPType::FP_Double);
	}
	std::string getTargetFilePath() {
		return targetFilePath;
	}

	void initTypeStrDigits(FPType fpt) {
		targetFPType = fpt;
		switch(fpt) {
			case FPType::FP_Double: {
				targetFPTypeStr    = "double";
				targetFPTypeDigits = mpfr::bits2digits(53)+1; // double has 53 bits fraction
				break;
			}
			case FPType::FP_Real: {
				targetFPTypeStr    = "xreal";
				targetFPTypeDigits = mpfr::bits2digits(xreal::get_default_prec())+1;
				break;
			}
			case FPType::FP_Float: {
				targetFPTypeStr    = "float";
				targetFPTypeDigits = mpfr::bits2digits(23)+1; // float has 23 bits fraction, in IEEE 754
				break;
			}
			case FPType::FP_LongDouble: {
				targetFPTypeStr    = "long double";
				targetFPTypeDigits = mpfr::bits2digits(80)+1; // a guess, since long double is not a standard
				break;
			}
			case FPType::FP_Other: {
				// When other type are used, we need to manually set str and bits.
				targetFPTypeStr    = "double";
				targetFPTypeDigits = mpfr::bits2digits(53)+1;
				break;
			}
		}
	}

	void setTargetFPType(FPType fpt) {
		initTypeStrDigits(fpt);
	}
	FPType getTargetFPType() const {
		return targetFPType;
	}
	void setTargetFPTypeStr(const std::string& FPTypeStr) {
		targetFPTypeStr = FPTypeStr;
	}
	std::string getTargetFPTypeStr() const {
		return targetFPTypeStr;
	}
	void setTargetFPTypeDigits(uint32_t digits) {
		targetFPTypeDigits = digits;
	}
	uint32_t getTargetFPTypeDigits() const {
		return targetFPTypeDigits;
	}
};

class Transformer {
private:
	TransformerSource transSource;
	TransformerTarget transTarget;
	bool writeToFileFlag;

	std::map<std::string, xreal> transValue;
	std::map<std::string, std::vector<xreal>> transVector;

public:
	// src: source file path; tgt: target file path; writeToFile: whether write to file, default as true
	Transformer(const std::string& src, const std::string& tgt, bool writeToFile = true)
		: transSource(src), transTarget(tgt), writeToFileFlag(writeToFile)
	{
	}

	void setTargetFPType(FPType fpt) {
		transTarget.setTargetFPType(fpt);
	}
	void setTargetFPTypeStr(const std::string& FPType) {
		transTarget.setTargetFPTypeStr(FPType);
	}
	void setTargetFPTypeDigits(uint32_t digits) {
		transTarget.setTargetFPTypeDigits(digits);
	}

	FPType getTargetFPType() const {
		return transTarget.getTargetFPType();
	}

	std::string getTargetFPTypeStr() const {
		return transTarget.getTargetFPTypeStr();
	}

	uint32_t getTargetFPTypeDigits() const {
		return transTarget.getTargetFPTypeDigits();
	}

	bool hasValueName(std::string name) const {
		if (transValue.find(name) == transValue.end()) {
			return false;
		}
		return true;
	}

	bool hasVectorName(std::string name) const {
		if (transVector.find(name) == transVector.end()) {
			return false;
		}
		return true;
	}

	xreal getValueByName(std::string name) {
		if (hasValueName(name)) {
			return transValue[name];
		}
		return 0.0;
	}
	std::vector<xreal> getVectorByName(std::string name) {
		if (hasVectorName(name)) {
			return transVector[name];
		}
		return {};
	}

	std::string getSourceFilePath() {
		return transSource.getSourceFilePath();
	}

	std::string getTargetFilePath() {
		return transTarget.getTargetFilePath();
	}

	bool isWriteToFile() {
		return writeToFileFlag;
	}

	std::string dumpIntValue(const xreal &value) {
		std::string s = std::to_string((int64_t)value);
		return s;
	}
	std::string dumpFPValue(const xreal &value) {
		FPType t = getTargetFPType();
		if (t == FPType::FP_Double) {
			std::string s = value.toString("%.16Re");
			return s;
		}
		if (t == FPType::FP_Float) {
			std::string s = value.toString("%.7Re");
			s = s + 'f'; // float suffix
			return s;
		}
		if (t == FPType::FP_LongDouble) {
			// long double could have 112/128 or 64/80 fraction bits.
			std::string s = value.toString("%.34Re"); 
			s = s + 'l'; // long double suffix
			return s;
		}
		if (t == FPType::FP_Other) {
			int digits = getTargetFPTypeDigits();
			std::string formatstr = "%." + std::to_string(digits) + "Re";
			std::string s = value.toString(formatstr);
			// smaller than float
			if (digits <= 7) {
				s += 'f';
			}
			// larger than double
			if (digits >= 16) {
				s += 'l';
			}
			return s;
		}
		// Error
        std::cerr << "Wrong call for [dumpFPValue], TargetFPType is not a built in floating-point type. Try [dumpRealValue].\n";
		return "";
    }
	std::string dumpRealValue(const xreal &value) {
		// should we use default precision or value's precision?
		int prec = (int)xreal::get_default_prec();

		std::string formatstr = "%." +
			std::to_string( mpfr::bits2digits(prec) + 1 )
			+ "Re";
		std::string s = "xreal(\"";
		s += value.toString(formatstr);
		s += "\", ";
		s += std::to_string(prec);
		s += ")";
		return s;
    }

	std::vector<std::string> getSourceList() {
		return transSource.getSourceList();
	}

	void addTransformValue(const std::string& name, const xreal& value) {
		transValue[name] = value;
	}

	template <typename FP>
	void addTransformVector(const std::string& name, const std::vector<FP>& valueVector) {
		// In case the values are in vector<double> and other types.
		// xreal is convertible from double, int, and even string, etc.
		if (std::is_convertible<FP, xreal>::value == false) {
			std::cerr << "Value vector for " << name << " is with invalid type.\n";
			return;
		}
		std::vector<xreal> tempVec(valueVector.begin(), valueVector.end());
		transVector[name] = tempVec;
	}

	void dump() {
		std::cout << "XReal precision: " << xreal::get_default_prec() << "\n";
		std::cout << "Source File: " << transSource.getSourceFilePath() << "\n";
		std::cout << "Target File: " << transTarget.getTargetFilePath() << "\n";
		std::cout << "Target FPType: " << transTarget.getTargetFPTypeStr() << "\n";
		std::cout << "Target FPType Fraction Digits: " << transTarget.getTargetFPTypeDigits() << "\n";
		std::cout << "Logged Values: \n";
		for (const auto& item : transValue) {
			std::cout << "  " << item.first << ", " << item.second
					  << " [ " << item.second.get_prec() << " ]\n";
		}
		std::cout << "Logged Vectors: \n";
		for (const auto& item : transVector) {
			std::cout << "  " << item.first << ", {\n";
			for (const auto& value : item.second) {
				std::cout << "    " << value
						  << " [ " << value.get_prec() << " ]\n";
			}
			std::cout << "  }\n";
		}
	}

	int runTransformer() {
		int res = execTypeValueConverter(*this);
		return res;
	}

};

#endif