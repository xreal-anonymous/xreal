#include "SourceLayerPass.h"
#include <string>

int main(int argc, char **argv) {
	auto& reg = sourcelayer::PassRegistry::getInstance();

	if (argc <= 1) {
		printf("Usage: bin/SourceLayerDriver [passname]\n");
		printf("Available passnames:\n");
		vector<string> nameList = reg.getAvailablePassNames();
		for (const string& s : nameList) {
			printf("  - %s\n", s.c_str());
		}
		return 0;
	}

	string passname = argv[1];

	auto passinfo = reg.getPassInfo(passname);

	if (passinfo == nullptr) {
		printf("PassInfo Not Found.\n");
		return 0;
	}

	auto pass = passinfo->createPass();
	pass->run();
}