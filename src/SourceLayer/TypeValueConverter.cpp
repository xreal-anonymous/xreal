#include "Transformer.h"

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

#include <vector>
#include <set>
#include <utility>

using namespace clang;
using namespace clang::tooling;

namespace {

static llvm::cl::OptionCategory TypeValueConverterToolingCategory("Type Value Converter");

const std::string XRealName = "xreal";

const std::set<std::string> constantNames = {
    "xreal::e",
    "xreal::pi"
};

// Check whether "type" is xreal.
bool isXReal(const QualType& type) {
    const IdentifierInfo *baseTypeIdentifier = type.getBaseTypeIdentifier();
    // if type is an 'int', 'double', baseTypeIdentifier will be NULL.
    if (baseTypeIdentifier != nullptr) {
        if (baseTypeIdentifier->isStr(XRealName)) {
            return true;
        }
    }
    return false;
}

// Check whether "type" contains "strName", like "xreal*", "int[]", "vector<xreal>", etc.
bool containsXRealRelated(const QualType& type) {
    const Type* t = type.getTypePtr();
    // t->dump();

    // Works for xreal, xreal[], xreal*
    if (isXReal(type)) {
        return true;
    }

    // Next, handle vector<xreal>, vector<vector<xreal>>, etc.
    if(isa<TemplateSpecializationType>(t)) {
        const TemplateSpecializationType *tt = cast<TemplateSpecializationType>(t);
        unsigned numArgs = tt->getNumArgs();
        for (unsigned i = 0; i < numArgs; i++) {
            QualType paramType = tt->getArg(i).getAsType();
            if (containsXRealRelated(paramType)) {
                return true;
            }
        }
    }
    // getchar();

    return false;
}


enum class ValueKind { VK_integer,
                       VK_floating,
                       VK_xreal };

bool isVariable(const QualType &type, ValueKind &vk) {
    const Type *baseTypePtr = type.getTypePtr();
    // Only divided into three cases, thus the check is inaccurate
    if (baseTypePtr->isIntegerType()) {
        vk = ValueKind::VK_integer;
        return true;
    }

    if (baseTypePtr->isFloatingType()) {
        vk = ValueKind::VK_floating;
        return true;
    }

    if (baseTypePtr->isClassType()) {
        const RecordDecl *RD = baseTypePtr->getAsRecordDecl();
        if (RD->getName() == "xreal") {
            vk = ValueKind::VK_xreal;
            return true;
        }
    }
    return false;
}

bool isArray(const QualType &type, ValueKind &vk) {
    const Type *baseTypePtr = type.getTypePtr();
    if (!baseTypePtr->isArrayType()) return false;
    const Type *elementTypePtr = baseTypePtr->getArrayElementTypeNoTypeQual();
    return isVariable(elementTypePtr->getCanonicalTypeInternal(), vk);
}

bool isVector(const QualType &type, ValueKind &vk) {
    const Type *baseTypePtr = type.getTypePtr();
    if (!baseTypePtr->isClassType()) return false;
    const RecordDecl *RD = baseTypePtr->getAsRecordDecl();
    if (RD->getName() != "vector") return false;
    const auto *TD = baseTypePtr->getAs<TemplateSpecializationType>();
    QualType instType = TD->getArg(0).getAsType();
    return isVariable(instType, vk);
}


class ConstConverterVisitor : public RecursiveASTVisitor<ConstConverterVisitor> {
private:
    ASTContext *CTX;
    Rewriter &PCRewriter;
    Transformer &TF;

    bool isMostTopLocation;
    SourceLocation mostTopLocation;
    std::set<std::string> metConstantNames;
    std::vector<SourceRange> bannedList;

public:
    ConstConverterVisitor(ASTContext *ctx, Rewriter &r, Transformer &t)
        : CTX(ctx), PCRewriter(r), TF(t) {
        isMostTopLocation = true;
    }

    bool VisitDecl(Decl *d) {
        SourceManager &SM = CTX->getSourceManager();
        auto stloc = d->getBeginLoc();
        auto edloc = d->getEndLoc();

        if (SM.isInMainFile(stloc) == false)
            return true;

        // Log most top location.
        logMostTopLocation(d);

        // Avoid rewrite constant in precision control variable.
        // e.g., xreal terms = xreal::e * 10; and "terms" is in Transformer's trans value.
        // we should not rewrite xreal::e, to avoid call rewriter at here multiple times.
        logBannedLocation(d);

        return true;
    }

    bool VisitStmt(Stmt *s) {
        SourceManager &SM = CTX->getSourceManager();
        auto stloc = s->getBeginLoc();
        auto edloc = s->getEndLoc();

        // Only interest in main file, i.e., excluding header files
        // https://stackoverflow.com/questions/37977758/clang-ast-visitor-avoid-traversing-include-files
        if (SM.isInMainFile(stloc) == false)
            return true;

        if (isa<DeclRefExpr>(s)) {
            DeclRefExpr *dre = cast<DeclRefExpr>(s);

            // xreal::e, xreal::pi always has qualifier "xreal::"
            if (dre->hasQualifier()) {
                // llvm::errs() << "namedDecl (Qualified): " << dre->getFoundDecl()->getQualifiedNameAsString() << "\n";
                if (constantNames.find(
                        dre->getFoundDecl()->getQualifiedNameAsString()) != constantNames.end()) {
                    meetConstant(dre);
                    // llvm::errs() << "[CONSTANT FOUND]\n";
                }
            }
        }
        // End for VisitStmt
        return true;
    }

private:

    void logMostTopLocation(Decl *d) {
        // This flag shows if the mostTopLocation are already set.
        if (isMostTopLocation == false)
            return;

        if (isa<TranslationUnitDecl>(d))
            return;

        SourceManager &SM = CTX->getSourceManager();
        // We should now at the first Decl in the main file.
        mostTopLocation = d->getBeginLoc();
        // Set flag to false, no longer reach here.
        isMostTopLocation = false;
        llvm::errs() << "Most Top Location: \n"
                     << "  " << mostTopLocation.printToString(SM) << "\n";
        return;
    }

    // The logic here should be sync up with
    // TypeValueConverterVisitor::visitDeclForValue
    void logBannedLocation(Decl *d) {
        if (isa<VarDecl>(d)) {
            VarDecl *vd = cast<VarDecl>(d);
            if (vd->isThisDeclarationADefinition() != VarDecl::Definition)
                return;

            QualType varType = vd->getType();
            std::string name = vd->getNameAsString();
            ValueKind vk;

            if (isVariable(varType, vk)) {
                if (TF.hasValueName(name)) {
                    bannedList.push_back(vd->getSourceRange());
                }
            }
            if (isArray(varType, vk) || isVector(varType, vk)) {
                if (TF.hasVectorName(name)) {
                    bannedList.push_back(vd->getSourceRange());
                }
            }
        }
        return;
    }

    bool strReplace(std::string &str, const std::string &from, const std::string &to) {
        size_t start_pos = str.find(from);
        if (start_pos == std::string::npos)
            return false;
        str.replace(start_pos, from.length(), to);
        return true;
    }

    void meetConstant(DeclRefExpr *dre) {
        // Do two things:
        // (1) write constant value at the mostTopLocation
        // (2) rewrite "xreal::e"

        // If target is FP_Real, don't do anything.
        if (TF.getTargetFPType() == FPType::FP_Real)
            return;

        std::string symbolName = dre->getFoundDecl()->getQualifiedNameAsString();
        std::string constDeclareStr =
            "const $FPTYPE $CONSTNAME = $CONSTVALUE;\n";
        if (symbolName == "xreal::e") {
            std::string constVarName = "CONST_E";
            // First time meet xreal::e, do job (1).
            if (metConstantNames.find(symbolName) == metConstantNames.end()) {
                // insert into meet set.
                metConstantNames.insert(symbolName);
                // construct declaration.
                strReplace(constDeclareStr, "$FPTYPE", TF.getTargetFPTypeStr());
                strReplace(constDeclareStr, "$CONSTNAME", constVarName);
                strReplace(constDeclareStr, "$CONSTVALUE", TF.dumpFPValue(xreal::e));
                PCRewriter.InsertTextBefore(mostTopLocation, constDeclareStr);
            }

            // If dre not in banned list, do job (2)
            SourceRange dreRange = SourceRange(dre->getBeginLoc(), dre->getEndLoc());
            bool isBanned = false;
            for (const SourceRange& range : bannedList) {
                if (range.fullyContains(dreRange)) {
                    isBanned = true;
                    break;
                }
            }
            if (isBanned == false) {
                PCRewriter.ReplaceText(dreRange, constVarName);
            }
        } else if (symbolName == "xreal::pi") {
            std::string constVarName = "CONST_PI";
            // Similar to "xreal::e"
            if (metConstantNames.find(symbolName) == metConstantNames.end()) {
                metConstantNames.insert(symbolName);
                strReplace(constDeclareStr, "$FPTYPE", TF.getTargetFPTypeStr());
                strReplace(constDeclareStr, "$CONSTNAME", constVarName);
                strReplace(constDeclareStr, "$CONSTVALUE", TF.dumpFPValue(xreal::pi));
                PCRewriter.InsertTextBefore(mostTopLocation, constDeclareStr);
            }

            SourceRange dreRange = SourceRange(dre->getBeginLoc(), dre->getEndLoc());
            bool isBanned = false;
            for (const SourceRange &range : bannedList) {
                if (range.fullyContains(dreRange)) {
                    isBanned = true;
                    break;
                }
            }
            if (isBanned == false) {
                PCRewriter.ReplaceText(dreRange, constVarName);
            }
        }
        // Add more else if for xreal::log2, log10, etc.
        return;
    }
};

class TypeValueConverterVisitor
    : public RecursiveASTVisitor<TypeValueConverterVisitor> {
private:
    ASTContext *CTX;
    Rewriter &PCRewriter;
    Transformer &TF;

    // Logging rewrited locations.
    std::set<std::pair<FileID, uint32_t>> rewrited;

public:
    TypeValueConverterVisitor(ASTContext *ctx, Rewriter &r, Transformer &t)
        : CTX(ctx), PCRewriter(r), TF(t) { }

    bool VisitDecl(Decl *d) {
        SourceManager &SM = CTX->getSourceManager();
        auto stloc = d->getBeginLoc();
        auto edloc = d->getEndLoc();

        // Only interest in main file, i.e., excluding header files
        // https://stackoverflow.com/questions/37977758/clang-ast-visitor-avoid-traversing-include-files
        if (SM.isInMainFile(stloc) == false)
            return true;

        // If target type is xreal, we don't need to rewrite type
        // otherwise, we need to rewrite type
        if (TF.getTargetFPType() != FPType::FP_Real) {
            visitDeclForType(d);
        }

        // Rewrite values got from Transformer
        visitDeclForValue(d);

        // llvm::errs() << "Here.\n";

        return true;
    }

private:
    void visitDeclForType(Decl *d) {
        // Modify return type for FunctionDecl
        if (isa<FunctionDecl>(d)) {
            FunctionDecl *fd = cast<FunctionDecl>(d);
            QualType returnType = fd->getReturnType();
            if (containsXRealRelated(returnType)) {
                // llvm::outs() << "[Function Return] contains xreal.\n"; fd->dumpColor(); llvm::outs() << "\n";
                TypeLoc retTypeLoc = fd->getFunctionTypeLoc().getReturnLoc();
                rewriteOnTypeLoc(retTypeLoc);
            }
            // End for FunctionDecl
            return;
        }
        // Modify type for VarDecl
        if (isa<VarDecl>(d)) {
            VarDecl *vd = cast<VarDecl>(d);
            QualType varType = vd->getType();
            if (containsXRealRelated(varType)) {
                // llvm::outs() << "[Variable Type] contains xreal.\n"; vd->dumpColor(); llvm::outs() << "\n";
                // getTypeSourceInfo from DeclaratorDecl
                TypeLoc varTypeLoc = vd->getTypeSourceInfo()->getTypeLoc();
                rewriteOnTypeLoc(varTypeLoc);
            }
            // End for VarDecl
            return;
        }
        // Modify type for TypedefDecl
        if (isa<TypedefDecl>(d)) {
            TypedefDecl *td = cast<TypedefDecl>(d);
            QualType originType = td->getUnderlyingType();
            if (containsXRealRelated(originType)) {
                TypeLoc underlyingTypeLoc = td->getTypeSourceInfo()->getTypeLoc();
                rewriteOnTypeLoc(underlyingTypeLoc);
            }
            // End for TypedefDecl
            return;
        }
    }

    void visitDeclForValue(Decl *d) {
        if (isa<VarDecl>(d)) {
            VarDecl *vd = cast<VarDecl>(d);
            if (vd->isThisDeclarationADefinition() != VarDecl::Definition)
                return;
            
            QualType varType = vd->getType();
            std::string name = vd->getNameAsString();
            ValueKind vk;

            if (isVariable(varType, vk)) {
                if (TF.hasValueName(name)) {
                    rewriteVariable(vd, vk, TF.getValueByName(name));
                }
            }
            if (isArray(varType, vk) || isVector(varType, vk)) {
                if (TF.hasVectorName(name)) {
                    rewriteArrayOrVector(vd, vk, TF.getVectorByName(name));
                }
            }
        }
        return;
    }

    int rewriteOnTypeLoc(TypeLoc &typeLoc) {
        SourceManager &SM = CTX->getSourceManager();

        int rewriteStatus = 0;

        // Is a vector< >, disassemble it.
        // https://lists.llvm.org/pipermail/cfe-dev/2013-February/028117.html
        if (TemplateSpecializationTypeLoc templateTypeLoc = typeLoc.getAs<TemplateSpecializationTypeLoc>()) {
            unsigned numArgs = templateTypeLoc.getNumArgs();
            for (unsigned i = 0; i < numArgs; i++) {
                TemplateArgumentLoc argLoc = templateTypeLoc.getArgLoc(i);
                TypeLoc argTypeLoc = argLoc.getTypeSourceInfo()->getTypeLoc();
                rewriteStatus += rewriteOnTypeLoc(argTypeLoc);
            }
            return rewriteStatus;
        }

        // Using a while loop with TypeLoc.getNextTypeLoc() to disassemble Type
        // e.g for "int*" the TypeLoc is a PointerLoc and next TypeLoc is for "int".
        // Loop until we find a RecordDecl, since 'xreal' is a RecordType.
        // https://clang.llvm.org/doxygen/classclang_1_1TypeLoc.html
        TypeLoc curTypeLoc = typeLoc;
        std::vector<TypeLoc> rewriteTargetList;
        while (curTypeLoc.isNull() == false) {
            if (curTypeLoc.getTypePtr()->isRecordType() == true) {
                if (isXReal(curTypeLoc.getType())) {
                    rewriteTargetList.push_back(curTypeLoc);
                }
            }
            curTypeLoc = curTypeLoc.getNextTypeLoc();
        }

        if (rewriteTargetList.size() == 0) {
            // llvm::outs() << "RecordType not found.\n";
            // llvm::outs() << typeLoc.getBeginLoc().printToString(SM) << "\n";
            return 0;
        }

        // llvm::outs() << "Rewrite List: " << rewriteTargetList.size() << "\n";

        for (const auto &loc : rewriteTargetList) {
            // Get SourceRange based on TypeLoc.
            // Don't know the difference between getSourceRange and getLocalSourceRange,
            // But getLocalSourceRange sometimes becomes <<invalid sloc>>
            // Thus we use getSourceRange for now.
            SourceRange range = loc.getSourceRange();
            // llvm::outs() << "Source Range:\n";
            // range.dump(SM);

            SourceLocation beginLoc = range.getBegin();
            SourceLocation endLoc = range.getEnd();

            auto curLoc = std::make_pair(SM.getFileID(beginLoc), beginLoc.getRawEncoding());

            // Never rewrited. Can be rewrited.
            if (rewrited.find(curLoc) == rewrited.end()) {
                // For single token, like 'xreal', rewrite directly.
                if (beginLoc == endLoc) {
                    PCRewriter.ReplaceText(range, TF.getTargetFPTypeStr());
                    rewrited.insert(curLoc);
                    rewriteStatus += 1;
                    // llvm::outs() << "Rewrite single token.\n";
                }
                // For multiple tokens, like 'xreal a []', the place is like to be first.
                // Should not reach this branck.
                // Since "xreal*" and etc. should be disassembled in the previous while loop.
                else {
                    SourceRange newRange(beginLoc, beginLoc);
                    PCRewriter.ReplaceText(newRange, TF.getTargetFPTypeStr());
                    rewrited.insert(curLoc);
                    rewriteStatus += 1;
                    llvm::errs() << "Rewrite multiple tokens. (Trying)\n";
                }
            } else {
                // Already rewrited, just return.
                // llvm::outs() << "Rewrite with previous location, skip over.\n";
            }
        }
        return rewriteStatus;
    }

    /* Rewrite Logic here:
     * ValueKind is the type in the *source file*.
     * TF.getTargetFPType() is the type in the *target file*.
     * 
     * if VK_integer:
     *   we should dump a integer, use TF.dumpIntValue(value)
     * 
     * if VK_floating or VK_xreal (mostly we should meet xreal,
     *            since we ask developers to use xreal in the original source file):
     *   we should dump a floating point based on TF.getTargetFPType()
     *   
     *   if TF.getTargetFPType() == FPType::FP_Real, use TF.dumpRealValue(value)
     *   else, use TF.dumpFPValue(value)
     */
    std::string dumpValue(const xreal &value, ValueKind vk) {
        if (vk == ValueKind::VK_integer) {
            return TF.dumpIntValue(value);
        }
        if (vk == ValueKind::VK_floating) {
            return TF.dumpFPValue(value);
        }
        if (vk == ValueKind::VK_xreal) {
            if (TF.getTargetFPType() == FPType::FP_Real) { 
                // llvm::errs() << "Dumping Real.\n";
                return TF.dumpRealValue(value);
            }
            else {
                // llvm::errs() << "Dumping FP.\n";
                return TF.dumpFPValue(value);
            }
        }
        llvm::errs() << "Not supported kind of value\n";
        return "";
    }

    void rewriteVariable(VarDecl *vd, ValueKind vk, const xreal &value) {
        // No initializer
        if (vd->hasInit() == false) {
            std::string insertText = " = " + dumpValue(value, vk);
            PCRewriter.InsertTextAfterToken(vd->getSourceRange().getEnd(),
                                            insertText);
            return;
        }
        // Has Initializer
        auto style = vd->getInitStyle();
        const Expr *init = vd->getInit();
        if (style == VarDecl::CInit) {
            // getInit() will extract everything after '='
            PCRewriter.ReplaceText(init->getSourceRange(), dumpValue(value, vk));
        } else if (vk != ValueKind::VK_xreal) {
            std::string replaceText = dumpValue(value, vk);
            if (style == VarDecl::CallInit) {
                // Special case: 'int a(1);'
            } else {
                // Special case: 'int a{1};'
                replaceText = " = " + replaceText;
            }
            PCRewriter.ReplaceText(init->getSourceRange(), replaceText);
        } else {
            // getInit() will extract everything including the variable name
            std::string replaceText = vd->getNameAsString() + " = " + dumpValue(value, vk);
            PCRewriter.ReplaceText(init->getSourceRange(), replaceText);
        }
    }

    void rewriteArrayOrVector(VarDecl *vd, ValueKind vk, const std::vector<xreal> &values) {
        // Dump values to CInit style initializer
        std::string dataText = "{\n";
        for (const xreal &value : values)
            dataText += "    " + dumpValue(value, vk) + ",\n";
        dataText += "}";
        // Special treatment for array type
        const TypeSourceInfo *TSI = vd->getTypeSourceInfo();
        TypeLoc varTypeLoc = TSI->getTypeLoc();
        auto arrTypeLoc = varTypeLoc.getAs<ArrayTypeLoc>();
        if (arrTypeLoc.isNull() == false) {
            PCRewriter.ReplaceText(arrTypeLoc.getBracketsRange(), "[]");
        }
        // No initializer
        if (vd->hasInit() == false) {
            std::string insertText = " = " + dataText;
            PCRewriter.InsertTextAfterToken(vd->getSourceRange().getEnd(),
                                            insertText);
            return;
        }
        // Has Initializer
        auto style = vd->getInitStyle();
        const Expr *init = vd->getInit();
        if (style == VarDecl::CInit) {
            // getInit() will extract everything after '='
            PCRewriter.ReplaceText(init->getSourceRange(), dataText);
        } else if (isArray(vd->getType(), vk) && style == VarDecl::ListInit) {
            // Special case: Array with list initializer will only match list
            // 'vk' is ignored here
            std::string replaceText = " = " + dataText;
            PCRewriter.ReplaceText(init->getSourceRange(), replaceText);
        } else {
            // getInit() will extract everything including the variable name
            // but for class like 'xreal', getInit() will only include the
            // name as the initializer
            PCRewriter.ReplaceText(init->getSourceRange(), vd->getNameAsString());
            std::string insertText = " = " + dataText;
            PCRewriter.InsertTextAfterToken(vd->getSourceRange().getEnd(),
                                            insertText);
        }
    }
};

class TypeValueConverterConsumer : public ASTConsumer {
private:
    TypeValueConverterVisitor TVCVisitor;
    ConstConverterVisitor CCVisitor;
public:
    explicit TypeValueConverterConsumer(ASTContext *ctx, Rewriter &r, Transformer &t)
        : TVCVisitor(ctx, r, t), CCVisitor(ctx, r, t) { }
    virtual void HandleTranslationUnit(ASTContext &ctx) {
        TVCVisitor.TraverseDecl(ctx.getTranslationUnitDecl());

        CCVisitor.TraverseDecl(ctx.getTranslationUnitDecl());
    }
};

class TypeValueConverterFrontendAction : public ASTFrontendAction {
private:
    Transformer &TF;
    Rewriter PCRewriter;
public:
    TypeValueConverterFrontendAction(Transformer &t) : TF(t) { }

    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(
        CompilerInstance &Compiler, llvm::StringRef InFile)
    {
        // Initialize Rewriter
        PCRewriter.setSourceMgr(Compiler.getSourceManager(), Compiler.getLangOpts());

        // Return ASTConsumer
        return std::make_unique<TypeValueConverterConsumer>(&Compiler.getASTContext(), PCRewriter, TF);
    }

    virtual void EndSourceFileAction() {
        SourceManager &SM = PCRewriter.getSourceMgr();
        llvm::outs() << "\n** EndSourceFileAction for: "
                     << SM.getFileEntryForID(SM.getMainFileID())->getName()
                     << " **\n\n";
        PCRewriter.getEditBuffer(SM.getMainFileID()).write(llvm::outs());
        llvm::outs() << "\n****\n";

        // Avoid writing the original file.
        if (TF.getSourceFilePath() == TF.getTargetFilePath()) {
            llvm::errs() << "Transformer got same [Source Path] and [Target Path], won't rewrite the [Source Path].\n";
            return;
        }

        // Transformer can have a "false" flag of writing to file.
        if (TF.isWriteToFile()) {
            // Write to TargetFilePath.
            std::error_code ec;
            std::string outPath = TF.getTargetFilePath();
            // Create all non-existent directories.
            // llvm::errs() << "Parent path: " << llvm::sys::path::parent_path(outPath) << "\n";
            llvm::sys::fs::create_directories(llvm::sys::path::parent_path(outPath));
            // Write to file.
            llvm::raw_fd_ostream outFile(TF.getTargetFilePath(), ec, llvm::sys::fs::OF_None);
            PCRewriter.getEditBuffer(SM.getMainFileID()).write(outFile);

            llvm::outs() << "Transformed program is written to: " << outPath << "\n";
        }
        return;
    }
};

template <typename ActionT>
std::unique_ptr<FrontendActionFactory> TVCFrontendActionFactory(Transformer &t) {
    class SimpleFrontendActionFactory : public FrontendActionFactory {
    private:
        Transformer &TF;
    public:
        SimpleFrontendActionFactory(Transformer& lt) : TF(lt) {}

        std::unique_ptr<FrontendAction> create() override {
            return std::make_unique<ActionT>(TF);
        }
    };

    return std::unique_ptr<FrontendActionFactory>(
        new SimpleFrontendActionFactory(t));
}
}  // namespace

int execTypeValueConverter(Transformer &t) {
    // TODO: In future we could make this function cleaner.

    int dummyArgc = 3;
    // The third "--" is used for the following error:
    // > Error while trying to load a compilation database
    // > Could not auto-detect compilation database for file ......
    const char* dummyArgv[] = {
        "./dummytool",
        "./dummyinput.cc",
        "--"
    };

    clang::tooling::CommonOptionsParser op(dummyArgc, dummyArgv, TypeValueConverterToolingCategory);
    clang::tooling::ClangTool Tool(op.getCompilations(), t.getSourceList());

    // Manually told tool to use c++
    // https://stackoverflow.com/questions/63148816/how-a-h-file-can-parse-as-c-in-clangtool
    Tool.appendArgumentsAdjuster(getInsertArgumentAdjuster("-xc++", clang::tooling::ArgumentInsertPosition::BEGIN));
    // Manually assign standard c++17
    Tool.appendArgumentsAdjuster(getInsertArgumentAdjuster("--std=c++17", clang::tooling::ArgumentInsertPosition::BEGIN));

    int result = Tool.run(
        TVCFrontendActionFactory<TypeValueConverterFrontendAction>(t).get());
    
    llvm::outs() << "\nClang Tool Run Result: " << result << "\n";
    return result;
}
