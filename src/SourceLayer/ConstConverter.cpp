#include <set>
#include <utility>
#include <vector>

#include "Transformer.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

using namespace clang;
using namespace clang::tooling;

static llvm::cl::OptionCategory ConstConverterToolingCategory("Const Converter");

std::set<std::string> constantNames = {
    "xreal::e",
    "xreal::pi"
};

class ConstConverterVisitor : public RecursiveASTVisitor<ConstConverterVisitor> {
private:
    ASTContext *CTX;
    Rewriter &PCRewriter;
    Transformer &TF;

    bool isMostTopLocation;
    SourceLocation mostTopLocation;
    std::set<std::string> metConstantNames;

public:
    ConstConverterVisitor(ASTContext *ctx, Rewriter &r, Transformer &t)
        : CTX(ctx), PCRewriter(r), TF(t) {
        isMostTopLocation = true;
    }

    bool VisitDecl(Decl *d) {

        SourceManager &SM = CTX->getSourceManager();
        auto stloc = d->getBeginLoc();
        auto edloc = d->getEndLoc();

        if (SM.isInMainFile(stloc) == false)
            return true;

        if (isa<VarDecl>(d)) {
            VarDecl *vd = cast<VarDecl>(d);

            SourceRange sr = vd->getSourceRange();

            sr.dump(SM);

            SourceRange sr2 = SourceRange(stloc, edloc);
            sr2.dump(SM);

            llvm::errs() << "\n";
        }

        if (isa<TranslationUnitDecl>(d))
            return true;

        // This flag shows if the mostTopLocation are already set.
        if (isMostTopLocation == false)
            return true;

        // We should now at the first Decl in the main file.
        mostTopLocation = stloc;
        // Set flag to false, no longer reach here.
        isMostTopLocation = false;
        llvm::errs() << "Most Top Location: \n"
                     << "  " << mostTopLocation.printToString(SM) << "\n";
        return true;
    }

    bool VisitStmt(Stmt *s) {
        SourceManager &SM = CTX->getSourceManager();
        auto stloc = s->getBeginLoc();
        auto edloc = s->getEndLoc();

        // Only interest in main file, i.e., excluding header files
        // https://stackoverflow.com/questions/37977758/clang-ast-visitor-avoid-traversing-include-files
        if (SM.isInMainFile(stloc) == false)
            return true;

        if (isa<DeclRefExpr>(s)) {
            DeclRefExpr *dre = cast<DeclRefExpr>(s);

            // xreal::e, xreal::pi always has qualifier "xreal::"
            if (dre->hasQualifier()) {
                // llvm::errs() << "namedDecl (Qualified): " << dre->getFoundDecl()->getQualifiedNameAsString() << "\n";
                if (constantNames.find(
                        dre->getFoundDecl()->getQualifiedNameAsString()) != constantNames.end()) {
                    meetConstant(dre);
                    // llvm::errs() << "[CONSTANT FOUND]\n";
                }
            }
        }
        // End for VisitStmt
        return true;
    }

private:
    bool strReplace(std::string &str, const std::string &from, const std::string &to) {
        size_t start_pos = str.find(from);
        if (start_pos == std::string::npos)
            return false;
        str.replace(start_pos, from.length(), to);
        return true;
    }

    void meetConstant(DeclRefExpr *dre) {
        // Do two things:
        // (1) write constant value at the mostTopLocation
        // (2) rewrite "xreal::e"

        // If target is FP_Real, don't do anything.
        if (TF.getTargetFPType() == FPType::FP_Real)
            return;

        std::string symbolName = dre->getFoundDecl()->getQualifiedNameAsString();
        std::string constDeclareStr = 
            "const $FPTYPE $CONSTNAME = $CONSTVALUE;\n";
        if (symbolName == "xreal::e") {
            std::string constVarName = "CONST_E";
            // First time meet xreal::e, do job (1).
            if (metConstantNames.find(symbolName) == metConstantNames.end()) {
                // insert into meet set.
                metConstantNames.insert(symbolName);
                // construct declaration.
                strReplace(constDeclareStr, "$FPTYPE", TF.getTargetFPTypeStr());
                strReplace(constDeclareStr, "$CONSTNAME", constVarName);
                strReplace(constDeclareStr, "$CONSTVALUE", TF.dumpFPValue(xreal::e));
                PCRewriter.InsertTextBefore(mostTopLocation, constDeclareStr);
            }
            // Always do job (2)
            PCRewriter.ReplaceText(
                SourceRange(dre->getBeginLoc(), dre->getEndLoc()),
                constVarName);
            llvm::errs() << "dre range: \n";
            SourceRange(dre->getBeginLoc(), dre->getEndLoc()).dump(CTX->getSourceManager());
            llvm::errs() << "\n";
        } else if (symbolName == "xreal::pi") {
            std::string constVarName = "CONST_PI";
            // Similar to "xreal::e"
            if (metConstantNames.find(symbolName) == metConstantNames.end()) {
                metConstantNames.insert(symbolName);
                strReplace(constDeclareStr, "$FPTYPE", TF.getTargetFPTypeStr());
                strReplace(constDeclareStr, "$CONSTNAME", constVarName);
                strReplace(constDeclareStr, "$CONSTVALUE", TF.dumpFPValue(xreal::pi));
                PCRewriter.InsertTextBefore(mostTopLocation, constDeclareStr);
            }
            PCRewriter.ReplaceText(
                SourceRange(dre->getBeginLoc(), dre->getEndLoc()),
                constVarName);
        }
        // Add more else if for xreal::log2, log10, etc.
        return;
    }
};

class ConstConverterConsumer : public ASTConsumer {
private:
	ConstConverterVisitor Visitor;
public:
	explicit ConstConverterConsumer(ASTContext *ctx, Rewriter &r, Transformer &t)
        : Visitor(ctx, r, t) {}

	virtual void HandleTranslationUnit(ASTContext &ctx) {
		Visitor.TraverseDecl(ctx.getTranslationUnitDecl());
	}
};

class ConstConverterFrontendAction : public ASTFrontendAction {
private:
    Transformer &TF;
	Rewriter PCRewriter;
public:
    ConstConverterFrontendAction(Transformer &t) : TF(t) { }

	virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(
		CompilerInstance &Compiler, llvm::StringRef InFile)
	{
		// Initialize Rewriter
		PCRewriter.setSourceMgr(Compiler.getSourceManager(), Compiler.getLangOpts());

		// Return ASTConsumer
		return std::make_unique<ConstConverterConsumer>(&Compiler.getASTContext(), PCRewriter, TF);
	}

	virtual void EndSourceFileAction() {
		SourceManager &SM = PCRewriter.getSourceMgr();
		llvm::outs() << "\n** EndSourceFileAction for: "
					 << SM.getFileEntryForID(SM.getMainFileID())->getName()
					 << " **\n\n";
		PCRewriter.getEditBuffer(SM.getMainFileID()).write(llvm::outs());
	}
};

template <typename ActionT>
std::unique_ptr<FrontendActionFactory> TVCFrontendActionFactory(Transformer &t) {
    class SimpleFrontendActionFactory : public FrontendActionFactory {
    private:
        Transformer &TF;

    public:
        SimpleFrontendActionFactory(Transformer &lt) : TF(lt) {}

        std::unique_ptr<FrontendAction> create() override {
            return std::make_unique<ActionT>(TF);
        }
    };

    return std::unique_ptr<FrontendActionFactory>(
        new SimpleFrontendActionFactory(t));
}

int main(int argc, const char **argv) {
    CommonOptionsParser op(argc, argv, ConstConverterToolingCategory);
    ClangTool Tool(op.getCompilations(),
                   op.getSourcePathList());
    // Manually told tool to use c++
    // https://stackoverflow.com/questions/63148816/how-a-h-file-can-parse-as-c-in-clangtool
    Tool.appendArgumentsAdjuster(getInsertArgumentAdjuster("-xc++", ArgumentInsertPosition::BEGIN));
    // Manually assign standard c++17
    Tool.appendArgumentsAdjuster(getInsertArgumentAdjuster("--std=c++17", ArgumentInsertPosition::BEGIN));

    Transformer doubleTarget("inputs/lambert.xreal", "outputs/lambert.double.cpp");
    doubleTarget.setTargetFPType(FPType::FP_Double);


    // int result = Tool.run(newFrontendActionFactory<clang::SyntaxOnlyAction>().get());
    int result = Tool.run(TVCFrontendActionFactory<ConstConverterFrontendAction>(doubleTarget).get());
    llvm::outs() << "\nTool Run Result: " << result << "\n";
    return 0;
}