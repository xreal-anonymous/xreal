#ifndef SOURCELAYER_H
#define SOURCELAYER_H

#include <string>
#include <map>
#include <vector>
#include <cassert>

using std::string;
using std::map;
using std::vector;

namespace sourcelayer {

class Pass;
class PassInfo;
class PassRegistry;

class Pass {
private:
	const void *PassID;
public:
	explicit Pass(char &pid) : PassID(&pid) {}
	Pass(const Pass &) = delete;
	Pass &operator=(const Pass &) = delete;
	// virtual ~Pass();

	virtual bool run() = 0;
};

class PassInfo {
public:
	using NormalCtor_t = Pass* (*)();
private:
	const void *PassID;
	string PassName;
	NormalCtor_t NormalCtor = nullptr;
public:
	PassInfo(const string &name, const void *pi, NormalCtor_t normal)
		: PassName(name), PassID(pi), NormalCtor(normal) { }
	PassInfo(const string &name, const void *pi)
		: PassName(name), PassID(pi) { }
	string getPassName() const { return PassName; }
	const void *getPassID() const { return PassID; }
	NormalCtor_t getNormalCtor() const { return NormalCtor; }
	Pass *createPass() const {
		assert(NormalCtor && "Cannot call createPass on PassInfo without default ctor!");
		return NormalCtor();
	}
};

class PassRegistry {
// Singleton Template: https://stackoverflow.com/questions/1008019/c-singleton-design-pattern/1008289
public:
	static PassRegistry& getInstance() {
		static PassRegistry instance;
		return instance;
	}
private:
	PassRegistry() { }
public:
	PassRegistry(PassRegistry const&)   = delete;
	void operator=(PassRegistry const&) = delete;
// Template Done.

// Maintain Pass Information.
private:
	map<const void *, const PassInfo *> PassInfoMap;
	map<const string, const PassInfo *> PassInfoStringMap;

public:
	void registerPass(const PassInfo &PI) {
		bool inserted_id = PassInfoMap.insert( std::make_pair(PI.getPassID(), &PI) ).second;
		assert(inserted_id && "Pass registered multiple times!");

		bool inserted_name = PassInfoStringMap.insert( std::make_pair(PI.getPassName(), &PI) ).second;
		assert(inserted_name && "Passname registered multiple times!");
		// PassInfoStringMap[PI.getPassName()] = &PI;
	}

	const PassInfo *getPassInfo(const void *TI) const {
		auto it = PassInfoMap.find(TI);
		if (it == PassInfoMap.end())
			return nullptr;
		return it->second;
	}

	const PassInfo *getPassInfo(const string &arg) const {
		auto it = PassInfoStringMap.find(arg);
		if (it == PassInfoStringMap.end())
			return nullptr;
		return it->second;
	}

	vector<string> getAvailablePassNames() const {
		vector<string> nameList;
		for (const auto& entry : PassInfoStringMap) {
			nameList.push_back(entry.first);
		}
		return nameList;
	}
};

template <typename PassName>
Pass *callDefaultCtor() { return new PassName(); }

template <typename passName>
struct RegisterPass : public PassInfo
{
	RegisterPass(const string& name)
		: PassInfo(name, &passName::ID, PassInfo::NormalCtor_t(callDefaultCtor<passName>))
	{
		PassRegistry::getInstance().registerPass(*this);
	}
};

} // end namespace sourcelayer

#endif // SOURCELAYER_H